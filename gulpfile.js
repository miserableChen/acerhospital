// 引入 gulp
'use strict';
var gulp = require('gulp');
var compass = require('gulp-compass');
var htmlreplace = require('gulp-html-replace');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat')
var ngAnnotate = require('gulp-ng-annotate')
var uglify = require('gulp-uglify');
var del = require('del');
var runSequence = require('run-sequence');


var sass_path = ['src/scss/*.scss'];

var css_path = [
    'src/css/welcome.css',
    'src/css/pharmacy.css',
    'src/css/admin.css',
    'src/css/ie.css',
    'src/css/print.css',
    'src/css/global.css',
    'src/css/forgetPW.css',
    'src/css/profile.css'
];

var js_path = ['src/module/module.js', 'src/module/router-config.js', 'src/service/*.js', 'src/controllers/**.js', 'src/controllers/**/**.js', 'src/directives/*.js'];

var asset_path = "src/assets/";

var resource_arr = [asset_path + "resources/**/**"];

var lib_path = [
    asset_path + 'lib/**jquery/dist/jquery.min.js',
    asset_path + 'lib/**bootstrap/dist/js/bootstrap.min.js',
    // asset_path+'lib/**d3/d3.min.js',
    asset_path + 'lib/**angular/angular.min.js',
    asset_path + 'lib/**angular-aria/angular-aria.min.js',
    asset_path + 'lib/**angular-animate/angular-animate.min.js',
    asset_path + 'lib/**angular-cookies/angular-cookies.min.js',
    asset_path + 'lib/**angular-messages/angular-messages.min.js',
    asset_path + 'lib/**angular-ui-router/release/angular-ui-router.min.js',
    asset_path + 'lib/**angular-material/angular-material.min.js',
    asset_path + 'lib/**metisMenu/dist/metisMenu.min.js',
    asset_path + 'lib/**echarts/dist/echarts.common.min.js',
    asset_path + 'lib/**moment/min/moment-with-locales.min.js',
    // asset_path+'lib/**moment/locale/en.js',
    // asset_path+'lib/**moment/locale/zh-tw.js',
    // asset_path+'lib/**moment/locale/zh-cn.js',
    asset_path + 'lib/**datatables/media/js/jquery.dataTables.min.js',
    asset_path + 'lib/**angular-datatables/dist/angular-datatables.min.js',
    asset_path + 'lib/**angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.min.js',
    asset_path + 'lib/**angular-bootstrap/ui-bootstrap-tpls.min.js',
    asset_path + 'lib/**angular-sanitize/angular-sanitize.min.js',
    asset_path + 'lib/**angular-translate/angular-translate.min.js',
    asset_path + 'lib/**angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
    asset_path + 'lib/**ng-csv/build/ng-csv.min.js',

    asset_path + 'lib/**datatables/media/css/dataTables.bootstrap.min.css',
    asset_path + 'lib/**bootstrap/dist/css/bootstrap.min.css',
    asset_path + 'lib/**angular-datatables/dist/css/angular-datatables.min.css',
    asset_path + 'lib/**metisMenu/dist/metisMenu.min.css',
    asset_path + 'lib/**font-awesome/css/font-awesome.min.css',
    asset_path + 'lib/**font-awesome/fonts/**',
    asset_path + 'lib/**angular-material/angular-material.min.css',
    asset_path + 'lib/**third-party-js/**',


];

var dist = "dist/";

gulp.task('parseIndex', function() {
    return gulp.src(['src/index.html'])
        .pipe(htmlreplace({
            'css': 'public/app.css?timestamp=' + new Date().getTime(),
            'js': 'public/app.js?timestamp=' + new Date().getTime()
        }))
        .pipe(gulp.dest(dist));
});

gulp.task('js', function() {
    return gulp.src(js_path)
        .pipe(plumber())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(dist + '/public'))
})

gulp.task('js:watch', ['js'], function() {
    return gulp.watch('src/**/*.js', ['js']);
});

gulp.task('resource', function() {
    return gulp.src(resource_arr)
        .pipe(gulp.dest(dist + 'assets/resources'));

});

gulp.task('lib', function() {
    return gulp.src(lib_path)
        .pipe(gulp.dest(dist + 'assets/lib/'));

});

gulp.task('clean', function() {
    return del([dist]);
});

gulp.task('model', function() {
    return gulp.src(["src/models/*"])
        .pipe(plumber())
        .pipe(gulp.dest(dist + "models"));
});

gulp.task('model:watch', ['model'], function() {
    return gulp.watch('src/models/**.php', ['model']);
})

gulp.task('view', function() {
    return gulp.src(['src/view/**'])
        .pipe(gulp.dest(dist + "view"));
});

gulp.task('view:watch', ["view"], function() {
    return gulp.watch(['src/index.html', 'src/view/**'], ['parseIndex', 'view']);
})

gulp.task('sass', function() {
    return gulp.src(sass_path)
        .pipe(compass({
            sourcemap: true,
            time: true,
            css: 'src/css/',
            sass: 'src/scss/',
            style: 'compact' //nested, expanded, compact, compressed
        }));
});

gulp.task('lib-sass', function() {
    return gulp.src(sass_path)
        .pipe(compass({
            sourcemap: true,
            time: true,
            css: 'src/css/',
            sass: 'src/scss/',
            style: 'compact' //nested, expanded, compact, compressed
        }));
});

gulp.task('css', ['sass'], function() {
    return gulp.src(css_path)
        .pipe(concat('app.css'))
        .pipe(gulp.dest(dist + '/public'));
})

gulp.task('sass:watch', ['css'], function() {
    return gulp.watch('src/scss/*.scss', ['css']);
});


gulp.task('default', function(callback) {
    runSequence('clean', ['parseIndex', 'resource', 'lib', 'view:watch', 'sass:watch', 'js:watch', 'model:watch'],
        callback);
});