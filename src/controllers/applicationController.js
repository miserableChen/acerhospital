angular.module('abeingWellness').controller('ApplicationController', function ($log,$scope,$rootScope, $cookies,
	USER_ROLES,AuthService,currentUserService,$translate) {
	var apCtrl=this;
	$scope.currentUser = null;
	$scope.userRoles = USER_ROLES;
	$scope.isAuthorized = AuthService.isAuthorized;
	apCtrl.lang=$rootScope.language;
	$translate.use($rootScope.language);
	$log.log(apCtrl.lang);
	$scope.setCurrentUser = function (user) {
		$scope.currentUser = user;		
		currentUserService.create(user);
	};

	$scope.changeLanguage = function (langKey,dtOption) {		
		$cookies.put("lang",langKey);
		$rootScope.language=langKey;
		$translate.use(langKey);
		apCtrl.lang=$rootScope.language;
		//window.refresh();
  	};
	if (!$scope.currentUser) {
		$scope.setCurrentUser({
			"id": $cookies.get("userId"),
			"username": $cookies.get("userName"),
			"name": $cookies.get("name"),
			"branchId": $cookies.get("branchId"),
			"branchName": $cookies.get("branchName"),
			"role": $cookies.get("userRole"),
			"title": $cookies.get("title")
		});
	}

})