angular.module('abeingWellness.admin').controller("branchAddController",
	function ($log, $compile,$rootScope, $cookies,$translate, $scope, $mdDialog, branchService, memberData, dialogService) {
		$log.log("sample")
		$scope.branch = {};
		$scope.members = memberData;
		$scope.branch.managerId = "";
		$scope.isLoading=false;
		$scope.submit = function () {
			$scope.isLoading=true;
			branchService.create($scope.branch).then(
				function (response) {
					var result = response.data.resultMsg;
					$translate([result],{"Name":$scope.branch.name}).then(function(trans){
						$rootScope.alertBox("success",trans[result]);
					})
					
					$mdDialog.cancel();
				},
				function (failed) {
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$translate([failed.data.errorCode],{"Name":$scope.branch.name}).then(function(trans){						
						$scope.errorMsg =trans[failed.data.errorCode];
					})
				}

			);
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

