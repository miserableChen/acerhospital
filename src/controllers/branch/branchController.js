angular.module('abeingWellness.admin').controller("branchController",
	function ($log, $compile, $cookies, $scope, $rootScope,$translate, $mdDialog, fhirConfig, dataService, memberData, branchService, dialogService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {

		var branchCtrl = this;
		branchCtrl.data = [];
		branchCtrl.branchData = [];
		branchCtrl.memberData = memberData.result;

		branchCtrl.dtOptions = DTOptionsBuilder.fromFnPromise(function () { return branchService.list(); })
			.withDataProp('result')
			.withOption('paging', true)
			.withOption('headerCallback', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				//datatable如果按鈕或事件不會動，表示有可能需要需要run $compile
				$compile(angular.element(row).contents())($scope);
			})
			.withOption('createdRow', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				$compile(angular.element(row).contents())($scope);
			}).withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
		$scope.$watch('$root.language', function () {
			branchCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
		});

		branchCtrl.dtColumns = [
			DTColumnBuilder.newColumn('name').withTitle('<span translate="S_03115">Branch</span>'),
			DTColumnBuilder.newColumn(null).withTitle('').renderWith(function (data, type, full, meta) {
				branchCtrl.branchData[data.id] = data;
				return '<a ng-click="branchCtrl.editBranch(branchCtrl.branchData[' + data.id + '])"><i class="fa fa-cog" aria-hidden="true" /></a>';
			}).notSortable(),
			DTColumnBuilder.newColumn(null).withTitle('').renderWith(function (data, type, full, meta) {
				return '<a ng-click="branchCtrl.deleteBranch(branchCtrl.branchData[' + data.id + '])"><i class="fa fa-trash" aria-hidden="true" /></a>';
			}).notSortable(),
			DTColumnBuilder.newColumn('id').withTitle('').notVisible(),
			DTColumnBuilder.newColumn('telecom').withTitle('<span translate="S_03121">Phone Number</span>'),
			DTColumnBuilder.newColumn('manager_name').withTitle('<span translate="S_03122">Contact</span>'),
			DTColumnBuilder.newColumn('address').withTitle('<span translate="S_02716">Address</span>')
		];
		branchCtrl.dtInstance = {};
		branchCtrl.addBranch = function () {
			$log.log("add branch");
			$mdDialog.show({
				controller: 'branchAddController',
				templateUrl: 'view/dialog/branch_add.html',
				parent: angular.element(document.body),
				clickOutsideToClose: false,
				fullscreen: false,
				locals: { memberData: branchCtrl.memberData },
				/*info: html_data,*/
				controllerAs: 'branchAddCtrl',
				bindToController: true,
				onRemoving: function () { branchCtrl.refesh() }
			});

		};

		branchCtrl.editBranch = function (data) {
			$log.log("edit branch");
			$mdDialog.show({
				controller: 'branchEditController',
				templateUrl: 'view/dialog/branch_edit.html',
				parent: angular.element(document.body),
				clickOutsideToClose: false,
				fullscreen: false,
				locals: { branchData: data, memberData: branchCtrl.memberData },
				controllerAs: 'branchEditCtrl',
				bindToController: true,
				onRemoving: function () { branchCtrl.refesh() }
			});

		};

		branchCtrl.deleteBranch = function (data) {
			$log.log("delete branch");

			$translate(["S_03247","S_03214","S_03217"], { "Name": data.name }).then(function (trans) {
				var dailog_data = {
					"titile": "Delete branch",
					"message": trans.S_03247,
					"funcOnConfirm": function () {
						branchService.del(data).then(function (response) {
							var result = response.data.resultMsg;
							$rootScope.alertBox("success", trans[result]);
							branchCtrl.refesh();

						}, function (faild) {
							var result = faild.data.resultMsg;
							$rootScope.alertBox("danger", trans[result]);
							branchCtrl.refesh();

						})
					}
				}

				dialogService.confirmBox(dailog_data);
			})


		};

		branchCtrl.refesh = function () {
			branchCtrl.dtInstance.reloadData();
		}



	});

