angular.module('abeingWellness').controller("exportController",
	function ($log, $compile, $cookies, $rootScope, $scope, $mdDialog,dataHeader,exportData,fileName,isData) {
		$scope.title="Export";
		$scope.dataHeader = dataHeader;
		$scope.exportData=exportData;
		$scope.isData=isData;
		var exportName="export.csv";
		if(typeof(fileName)!=="undefined" && fileName.length >0 )
			exportName=fileName.replace(" ","")+".csv";

		$scope.exportFileName=exportName;
		$scope.printData = function () {			
			setTimeout(function(){window.print()},1500);
			$mdDialog.hide();
		};
		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.hide();
		};


	});

