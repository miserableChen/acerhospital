angular.module('abeingWellness.patients').controller("groupAddController",
	function ($log, $compile, $cookies, $rootScope,$filter, $scope, $mdDialog, groupService, selected,branch) {
		var groupAddCtrl=this;
		$scope.patient = {};
		$scope.isLoading=false;
		$scope.submit = function () {
			$scope.isLoading=true;
			
            var response = groupService.addGroup(selected,groupAddCtrl.group.name,branch)
                .then(
                    function(data){
						$rootScope.alertBox("success", $filter("translate")(data));
                        $scope.hide();
                    },
                    function(responseError){
						$scope.isLoading=false;
						$rootScope.alertBox("success", $filter("translate")(responseError));
                        $log.error(responseError);
                    }
                );
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

