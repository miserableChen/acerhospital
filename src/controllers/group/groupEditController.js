angular.module('abeingWellness.patients').controller("groupEditController",
    function ($log, $compile, $cookies, $rootScope, $scope, $mdDialog,$filter, groupService, patientId) {

        var groupEditCtrl = this;
        groupEditCtrl.groupList = {};
        groupEditCtrl.groupIdList = {};
        groupEditCtrl.patientGroup = {};
        $scope.isLoading=false;
        //groupService.updateList();

        groupService.getPatientGroup(patientId).then(
            function (response) {
                groupEditCtrl.groupName = response.groupName;
                groupEditCtrl.groupIdList = response.groupIdList;
                groupEditCtrl.patientGroup = response.patientGroup;
            },
            function (responseError) {
                alert(responseError)
            }
        );
        $scope.removeGroup = function (groupName) {
            groupEditCtrl.patientGroup[groupName] = false;
        }
        $scope.addGroup = function () {
            groupEditCtrl.patientGroup[groupEditCtrl.addGroupName] = true;
        }

        $scope.filterGroup = function (items, state) {
            var result = {};
            angular.forEach(items, function (value, key) {
                if (value == state) {
                    result[key] = value;
                }
            });
            return result;
        }
        $scope.submit = function () {
            var patientGroupId = {};
            $scope.isLoading=true;
            angular.forEach(groupEditCtrl.patientGroup, function (value, key) {
                patientGroupId[groupEditCtrl.groupIdList[key]] = value;
            });
            groupService.setPatientGroup(patientId, patientGroupId).then(
                function () {
                    $rootScope.alertBox("success", $filter("translate")("S_03187"));                    
                    $mdDialog.hide();
                },
                function (responseError) {
                    $scope.isLoading=false;
                    $log.error(responseError);
                }
            )
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

    });

