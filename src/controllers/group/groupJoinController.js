angular.module('abeingWellness.patients').controller("groupJoinController",
	function ($log, $compile, $cookies, $rootScope, $scope, $mdDialog, groupService, selected) {
		$scope.patient = {};
        $scope.isLoading=false;
        var groupJoinCtrl = this;
        groupJoinCtrl.groupIdList = {};
    
        groupService.updateList().then(
            function(response){
                groupJoinCtrl.groupIdList = response;
            },
            function(responseError){
                alert(responseError)
            }
        );
    
		$scope.submit = function () {
            $scope.isLoading=true;
            groupService.joinGroup(selected, groupJoinCtrl.groupId).then(
                function(data){
                    $rootScope.alertBox("success",data);
                    $log.info('Join Group Success');
                },
                function(responseError){
                    $scope.isLoading=false;
                    $rootScope.alertBox("danger",data);
                    $log.error(responseError);
                }
            );
            $scope.hide();
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

