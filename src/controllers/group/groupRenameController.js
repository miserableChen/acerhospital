angular.module('abeingWellness.patients').controller("groupRenameController",
    function($log, $compile, $cookies, $rootScope, $translate, $scope, $mdDialog, groupService, gName, gId) {
        $scope.patient = {};
        $scope.isLoading = false;
        $log.log(gId)
        var gCtrl = this;
        gCtrl.groupName = gName;
        $scope.submit = function() {
            $scope.isLoading = true;
            $log.log(gCtrl.groupName);
            var response = groupService.renameGroup(gCtrl.groupName, gId)
                .then(
                    function(data) {
                        $translate([data], { "Name": gCtrl.groupName }).then(function(trans) {
                            $rootScope.alertBox("success", trans[data]);
                        });
                        $mdDialog.cancel();
                    },
                    function(responseError) {
                        $scope.isLoading = false;
                        $translate([responseError], { "Name": gCtrl.groupName }).then(function(trans) {
                            $rootScope.alertBox("danger", trans[responseError]);
                        });
                        $log.error(responseError);
                    }
                );
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };


    });