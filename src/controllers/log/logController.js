angular.module('abeingWellness.admin').controller("logController",
	function ($log, $compile, $cookies, $rootScope, $scope, $mdDialog, fhirConfig, logService, dialogService, dataService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder,$translate) {

		var logCtrl = this;
		logCtrl.data = [];
		logCtrl.logData = [];

		logCtrl.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
			return logCtrl.logData = logService.get_log();
		})
			.withDataProp('result')
			.withOption('paging', true)
			.withOption('createdRow', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				$compile(angular.element(row).contents())($scope);
			}).withOption('headerCallback', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				//datatable如果按鈕或事件不會動，表示有可能需要需要run $compile
				$compile(angular.element(row).contents())($scope);
			}).withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
		$scope.$watch('$root.language', function () {
			logCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
		});

		logCtrl.dtColumns = [
			DTColumnBuilder.newColumn('create_time').withTitle('<span translate="S_00299">Time</span>'),
			DTColumnBuilder.newColumn('username').withTitle('<span translate="S_01404">Account</span>'),
			DTColumnBuilder.newColumn('message').withTitle('<span translate="S_02648">Message</span>')
		];
		logCtrl.dtInstance = {};

		logCtrl.refesh = function () {
			logCtrl.dtInstance.reloadData();
		}

		logCtrl.exportData = function () {
			$log.log(logCtrl.logData.then(function (data) {
				$mdDialog.show({
					controller: 'exportController',
					templateUrl: 'view/dialog/export.html',
					parent: angular.element(document.body),
					clickOutsideToClose: false,
					fullscreen: false,
					locals: { exportData: data.result, fileName: $translate("S_03171"), dataHeader: ["Time", "Account", "Message"], isData: true },
					controllerAs: 'expCtrl',
					bindToController: true
				});
			}));


		};

	});

