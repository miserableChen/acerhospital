angular.module('abeingWellness.login').controller("LoginController",
    function($log, $cookies, $state, $rootScope, $scope, $filter, AuthService, AUTH_EVENTS, USER_ROLES, dialogService, purchaseService) {
        $scope.credentials = {
            username: '',
            password: ''
        };

        if (typeof($cookies.get("remember")) !== 'undefined') {
            $scope.credentials.remember = eval($cookies.get("remember"));
        }

        if (eval($scope.credentials.remember) && typeof($cookies.get("userName")) !== 'undefined') {
            $scope.credentials.username = $cookies.get("userName");
        }

        if (typeof($cookies.get("authToken")) !== 'undefined') {
            AuthService.login_by_token($cookies.get("authToken")).then(function(user) {
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                $scope.setCurrentUser(user);
                $cookies.put("userId", user.id);
                $cookies.put("userRole", user.role);
                $cookies.put("userName", user.username);
                $cookies.put("name", user.name);
                $cookies.put("branchId", user.branchId);
                $cookies.put("branchName", user.branchName);
                $cookies.put("title", user.title);
                purchaseService.refresh();
                if (user.role == USER_ROLES.admin) {
                    $state.go("main.menu");
                } else if (user.role == USER_ROLES.patient) {
                    $log.log("test");
                    $state.go("main.patient", { id: 1 });
                } else {
                    $state.go("main.dashboard");
                }
            })
        }


        $scope.errorMessage = "";


        $scope.login = function(credentials) {

            AuthService.login(credentials).then(function(user) {
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                $scope.setCurrentUser(user);
                $cookies.put("userId", user.id);
                $cookies.put("userRole", user.role);
                $cookies.put("userName", user.username);
                $cookies.put("name", user.name);
                $cookies.put("branchId", user.branchId);
                $cookies.put("branchName", user.branchName);
                $cookies.put("title", user.title);
                purchaseService.refresh();
                if (user.role == USER_ROLES.admin) {
                    $state.go("main.menu");
                } else if (user.role == USER_ROLES.patient) {
                    $log.log("test");
                    $state.go("main.patient", { id: 1 });
                } else {
                    $state.go("main.dashboard");
                }

            }, function(failed) {
                $log.log(failed);
                if (failed.status == -1) {
                    $scope.errorMessage = $filter("translate")("S_00202");
                } else {
                    $scope.errorMessage = failed.data.errorCode;
                }

            });

        };


        $scope.rememberMe = function() {

            $cookies.put("remember", $scope.credentials.remember);
        }
    });