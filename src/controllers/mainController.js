angular.module("abeingWellness").controller('MainController',
    function($scope, $interval, $rootScope, $log, $cookies, $state, $location, Session, USER_ROLES, DATA_TYPE, patientService, AuthService, emergencyService, fhirConfig, groupService, logService, currentUserService, NoteService) {
        var mainCtrl = this;
        var timer;
        mainCtrl.shopName = fhirConfig.shopName;
        mainCtrl.patients = 0;
        mainCtrl.alerts = [];
        mainCtrl.notificationes = [];
        mainCtrl.currentGroup = "";
        mainCtrl.groups = [];
        $rootScope.timerArr = {};
        mainCtrl.currentUser = currentUserService.currentUser;
        NoteService.init();
        //$state.go("dashboard.patient")
        mainCtrl.loginRouting = function() {
            if ($location.path() == "/login") {
                return true;
            } else {
                return false;
            }
        };


        mainCtrl.setCurrentGroup = function(groupName) {
            mainCtrl.currentGroup = groupName;
        }

        mainCtrl.isDashboard = function() {
            return $state.current.name == 'main.dashboard' || $state.current.name == 'main.groups';
        };

        //redirect to home or login page.
        mainCtrl.hrefHome = function() {

            if (typeof(Session.id) === 'undefined')
                $state.go("login");
            else if (Session.userRole == USER_ROLES.admin) {
                $state.go("main.menu");
            } else {
                $state.go("main.dashboard");
                mainCtrl.setCurrentGroup('');
            }
        };

        // logout function
        mainCtrl.logout = function() {

            AuthService.logout($rootScope.timerArr);
        };

        mainCtrl.readEmergencyData = function(patientId, createTime) {

            emergencyService.readAlertData(patientId, createTime).then(function() {
                $log.log("read success");
            }, function() {
                $log.log("read fail");
            })
            $state.go("main.patient", { id: patientId });
        }

        mainCtrl.getEmergencyData = function() {

            emergencyService.getEmergencyPatientList(currentUserService.currentUser.branchId).then(function(response) {
                var notification_arr = [];
                var tmpObj = {};
                for (var i = 0; i < response.length; i++) {
                    //parse response data to notification with DATA_TYPE keyword
                    // var timestamp = response[i]["createTime"];

                    // if (typeof (tmpObj[timestamp]) == "undefined") tmpObj[timestamp] = [];
                    var emg_patient_obj = {
                        id: response[i].patientId,
                        dataId: response[i].id,
                        name: response[i].name,
                        createTime: response[i].createTime,
                        fromNow: (moment.unix(response[i].createTime).fromNow())
                    }
                    notification_arr.push(emg_patient_obj);
                }


                // for (var j in tmpObj) {					
                // 	for (var k in tmpObj[j]) {
                // 		notification_arr.push(tmpObj[j][k]);
                // 	}					
                // }
                // $log.log(notification_arr)
                mainCtrl.notificationes = notification_arr;
                //$log.log(mainCtrl.notificationes.length)
            })

        }

        mainCtrl.watchNotification = function() {
            setTimeout(function() { mainCtrl.getEmergencyData(); }, 100);
            if (typeof($rootScope.timerArr.emergencyQuery) != undefined) {
                $interval.cancel($rootScope.timerArr.emergencyQuery);
            }
            var timer = $interval(function() {
                mainCtrl.getEmergencyData();
            }, 9000);

            $rootScope.timerArr.emergencyQuery = timer;
        }
        if (mainCtrl.isDashboard()) {
            mainCtrl.watchNotification();
        }

    });