angular.module('abeingWellness.admin').controller("memberAddController",
	function ($log, $compile,$rootScope, $cookies, $scope, $mdDialog,$translate, memberService, branchData,dialogService) {
		$scope.member = {};
		$scope.member.role = "1";
		$scope.usernamePatten=/^[a-zA-Z0-9]{1,25}$/;
		$scope.branches = branchData;
		$scope.isLoading=false;
		$scope.submitted=false;
		$scope.submit = function () {
			
			$scope.submitted=true;
			$scope.isLoading=true;
			$scope.member.name=$scope.member.firstName+" "+$scope.member.lastName;
			memberService.create($scope.member).then(
				function (response) {
					var result = response.data.resultMsg;
					$translate([result],{"Name":$scope.member.name}).then(function(trans){
						$rootScope.alertBox("success",trans[result]);
					})
					
					
					$mdDialog.cancel();
				},
				function (failed) {
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$translate([failed.data.errorCode],{"Name":$scope.member.name}).then(function(trans){						
						$scope.errorMsg =trans[failed.data.errorCode];
					})
				}

			)

		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

