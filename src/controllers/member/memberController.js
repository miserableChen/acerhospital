angular.module('abeingWellness.admin').controller("memberController",
    function($log, $compile, $cookies, $rootScope, $scope, $translate, $mdDialog, fhirConfig, dataService, memberService, dialogService, branchService, branchData, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {

        var memCtrl = this;
        memCtrl.data = [];
        memCtrl.memberData = [];
        memCtrl.currentId = $cookies.get("userId");
        memCtrl.branchData = branchData.result;
        $log.log($rootScope.language);
        memCtrl.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return memberService.list() })
            .withDataProp('result')
            .withOption('paging', true)
            .withOption('createdRow', function(row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                //datatable如果按鈕或事件不會動，表示有可能需要需要run $compile
                $compile(angular.element(row).contents())($scope);
            })
            .withOption('headerCallback', function(row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                //datatable如果按鈕或事件不會動，表示有可能需要需要run $compile
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
        $scope.$watch('$root.language', function() {
            memCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
        });
        dtColumnsDef = [
            DTColumnDefBuilder.newColumnDef(0).notSortable()
        ];



        memCtrl.dtColumns = [
            DTColumnBuilder.newColumn('name').withTitle('<span translate="S_00296">Name</span>'),
            DTColumnBuilder.newColumn(null).withTitle('').renderWith(function(data, type, full, meta) {
                memCtrl.memberData[data.id] = data;
                return '<a ng-click="memCtrl.editMember(memCtrl.memberData[' + data.id + '])"><i class="fa fa-cog" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('').renderWith(function(data, type, full, meta) {
                if (data.id == memCtrl.currentId) {
                    return '';
                }
                return '<a ng-click="memCtrl.deleteMember(memCtrl.memberData[' + data.id + '])"><i class="fa fa-trash" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn('branchId').withTitle('').notVisible(),
            DTColumnBuilder.newColumn('branch_name').withTitle('<span translate="S_03115">Branch</span>').notSortable(), ,
            DTColumnBuilder.newColumn('title').withTitle('<span translate="S_00408">Title</span>').notSortable(), ,
            DTColumnBuilder.newColumn('username').withTitle('<span translate="S_01404">Account</span>').notSortable(),
        ];
        memCtrl.dtInstance = {};
        memCtrl.addMember = function() {
            $log.log("add member")
            $mdDialog.show({
                controller: 'memberAddController',
                templateUrl: 'view/dialog/member_add.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { branchData: memCtrl.branchData },
                /*info: html_data,*/
                controllerAs: 'memAddCtrl',
                bindToController: true,
                onRemoving: function() { memCtrl.refesh() }
            });

        };

        memCtrl.editMember = function(data) {
            $log.log("edit member");
            $mdDialog.show({
                controller: 'memberEditController',
                templateUrl: 'view/dialog/member_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { memberData: data, branchData: memCtrl.branchData },
                controllerAs: 'memEditCtrl',
                bindToController: true,
                onRemoving: function() { memCtrl.refesh() }
            });

        };

        memCtrl.deleteMember = function(data) {

            $translate(["S_03228", "S_03214", "S_03217"], { "Name": data.name }).then(function(trans) {
                var dailog_data = {
                    "titile": "Delete member",
                    "message": trans.S_03228,
                    "funcOnConfirm": function() {
                        memberService.del(data).then(function(response) {
                            var result = response.data.resultMsg;
                            $rootScope.alertBox("success", trans[result]);
                            memCtrl.refesh();
                        }, function(faild) {
                            var result = faild.data.resultMsg;
                            $rootScope.alertBox("danger", trans[result]);
                            memCtrl.refesh();

                        });
                    }
                };

                dialogService.confirmBox(dailog_data);
            });

        };

        memCtrl.refesh = function() {
            memCtrl.dtInstance.reloadData();
        }



    });