angular.module('abeingWellness.admin').controller("memberEditController",
	function ($log, $compile, $cookies,$rootScope, $scope,$translate, $mdDialog, memberData,branchData,memberService,dialogService) {
		var memCtrl=this;
		$scope.currentId=$cookies.get("userId");
		var name=memberData.name.split(" ");
		memberData.firstName=name[0];
		memberData.lastName=name[1];
		$scope.branches =branchData;
		$scope.member=memberData;
		$scope.isLoading=false;

		$scope.$watch('member.password',function(){
			 $scope.memberForm.$setValidity('pwmatch', $scope.member.password==$scope.member.confrimPassword);
		})

		$scope.$watch('member.confrimPassword',function(){
			 $scope.memberForm.$setValidity('pwmatch', $scope.member.password==$scope.member.confrimPassword);
		})

		$scope.submit = function () {			
			$scope.isLoading=true;
			$scope.member.name=$scope.member.firstName+" "+$scope.member.lastName;
			memberService.update($scope.member).then(
				function (response) {
					var result = response.data.resultMsg;
					$translate([result],{"Name":$scope.member.name}).then(function(trans){
						$rootScope.alertBox("success",trans[result]);
					})
					$mdDialog.cancel();
				},
				function (failed) {
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$translate([failed.data.errorCode],{"Name":$scope.member.name}).then(function(trans){						
						$scope.errorMsg =trans[failed.data.errorCode];
					})
					//$scope.errorMsg =$filter("translate")( failed.data.errorCode);
				}
			);
			
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

