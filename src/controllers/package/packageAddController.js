angular.module('abeingWellness.admin').controller("packageAddController",
	function ($log, $compile, $cookies,$rootScope,$translate, $scope, $mdDialog, packageService, dialogService) {		
		$scope.package = {};
		$scope.isLoading=false;
		$scope.submit = function () {
			$scope.isLoading=true;
			packageService.create($scope.package).then(
				function (response) {
					var result = response.data.resultMsg;
					$translate([result],{"Name":$scope.package.title}).then(function(trans){
						$rootScope.alertBox("success",trans[result]);
					});
					$mdDialog.cancel();
				},
				function (failed) {
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$translate([failed.data.errorCode],{"Name":$scope.package.title}).then(function(trans){						
						$scope.errorMsg =trans[failed.data.errorCode];
					});
				}
			);
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

