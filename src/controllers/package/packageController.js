angular.module('abeingWellness.admin').controller("packageController",
	function ($log, $compile, $cookies, $rootScope, $scope, $translate, $mdDialog, fhirConfig, packageService, dialogService, dataService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {

		var pkgCtrl = this;
		pkgCtrl.data = [];
		pkgCtrl.packageData = [];

		pkgCtrl.dtOptions = DTOptionsBuilder.fromFnPromise(function () { return packageService.list(); }).withDataProp('result')
			.withOption('paging', true)
			.withOption('createdRow', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				$compile(angular.element(row).contents())($scope);
			}).withOption('headerCallback', function (row, data, dataIndex) {
				// Recompiling so we can bind Angular directive to the DT
				//datatable如果按鈕或事件不會動，表示有可能需要需要run $compile
				$compile(angular.element(row).contents())($scope);
			})
			.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

		$scope.$watch('$root.language', function () {
			pkgCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
		});

		pkgCtrl.dtColumns = [
			DTColumnBuilder.newColumn('title').withTitle('<span translate="S_03059">Package</span>'),
			DTColumnBuilder.newColumn(null).withTitle('').renderWith(function (data, type, full, meta) {
				pkgCtrl.packageData[data.id] = data;
				return '<a ng-click="pkgCtrl.editService(pkgCtrl.packageData[' + data.id + '])"><i class="fa fa-cog" aria-hidden="true" /></a>';
			}).notSortable(),
			DTColumnBuilder.newColumn(null).withTitle('').renderWith(function (data, type, full, meta) {
				return '<a ng-click="pkgCtrl.deleteService(pkgCtrl.packageData[' + data.id + '])"><i class="fa fa-trash" aria-hidden="true" /></a>';
			}).notSortable(),
			DTColumnBuilder.newColumn('id').withTitle('').notVisible(),
			DTColumnBuilder.newColumn('monthValue').withTitle('<span translate="S_03254">Month</span>'),
			DTColumnBuilder.newColumn('price').withTitle('<span translate="S_03124">Price</span>'),
			DTColumnBuilder.newColumn(null).withTitle('<span translate="S_03188">Note</span>').renderWith(function (data, type, full, meta) {
				return dataService.br2n(data.note, false);
			})
		];
		pkgCtrl.dtInstance = {};
		pkgCtrl.addService = function () {
			$log.log("add Service");
			$mdDialog.show({
				controller: 'packageAddController',
				templateUrl: 'view/dialog/package_add.html',
				parent: angular.element(document.body),
				clickOutsideToClose: false,
				fullscreen: false,
				/*info: html_data,*/
				controllerAs: 'pkgAddCtrl',
				bindToController: true,
				onRemoving: function () { pkgCtrl.refesh() }
			});

		};

		pkgCtrl.editService = function (data) {
			$log.log("edit Service");
			$mdDialog.show({
				controller: 'packageEditController',
				templateUrl: 'view/dialog/package_edit.html',
				parent: angular.element(document.body),
				clickOutsideToClose: false,
				fullscreen: false,
				locals: { packageData: data },
				controllerAs: 'pkgEditCtrl',
				bindToController: true,
				onRemoving: function () { pkgCtrl.refesh() }
			});

		};

		pkgCtrl.deleteService = function (data) {

			$translate(["S_03248", "S_03214", "S_03217"], { "Name": data.title }).then(function (trans) {
				var dailog_data = {
					"titile": "Delete package",
					"message": trans.S_03248,
					"funcOnConfirm": function () {
						packageService.del(data).then(function (response) {
							var result = response.data.resultMsg;
							$rootScope.alertBox("success",trans[result]);
							pkgCtrl.refesh();

						}, function (faild) {
							var result = faild.data.resultMsg;
							$rootScope.alertBox("danger", trans[result]);
							pkgCtrl.refesh();

						});
					}
				};

				dialogService.confirmBox(dailog_data);
			});

		};

		pkgCtrl.refesh = function () {
			pkgCtrl.dtInstance.reloadData();
		}



	});

