angular.module('abeingWellness.admin').controller("packageEditController",
	function ($log, $compile, $cookies,$rootScope, $scope,$translate, $mdDialog, packageData,packageService,dialogService) {
		packageData.monthValue=parseInt(packageData.monthValue);
		packageData.price=parseInt(packageData.price);
		$scope.isLoading=false;
		$scope.package=packageData;

		$scope.submit = function () {
			$scope.isLoading=true;
			packageService.update($scope.package).then(
				function (response) {
					var result = response.data.resultMsg;
					$translate([result],{"Name":$scope.package.title}).then(function(trans){
						$rootScope.alertBox("success",trans[result]);
					});
					$mdDialog.cancel();
				},
				function (failed) {
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$translate([failed.data.errorCode],{"Name":$scope.package.title}).then(function(trans){						
						$scope.errorMsg =trans[failed.data.errorCode];
					});
				}
			);
			
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

