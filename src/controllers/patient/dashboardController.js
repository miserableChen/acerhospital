angular.module('abeingWellness.patients').controller("dashboardController",
    function($log, $compile, $cookies, $window, $state, $rootScope, $scope, $interval, $timeout, $mdDialog, $stateParams, $filter, $translate, DATA_UNIT,
        patientService, fhirConfig, dataService, DTDefaultOptions, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, groupService, dialogService, groupData, currentUserService) {

        var patCtrl = this;
        var mainCtrl = $scope.$parent.mainCtrl;
        var currentBranch = currentUserService.currentUser.branchId;
        $log.log(currentUserService.currentUser.branchId);
        patCtrl.userId = {};
        patCtrl.selected = [];
        patCtrl.current_rows = [];
        patCtrl.details = {};
        patCtrl.onloading = true;
        patCtrl.reverse = true;
        patCtrl.selectedPat = false;
        patCtrl.getPatientType = 'RAL';
        patCtrl.isGroup = false;
        patCtrl.groupId = ($stateParams.groupId) || '';
        patCtrl.dtInstance = {};
        //var titleHtml = '<label><input type="checkbox" id="selectAll" ng-click="patCtrl.toggleAll(patCtrl.selected)"></label>';
        var titleHtml = '<label><input type="checkbox" id="selectAll" ng-model="patCtrl.selectAll" ng-click="patCtrl.toggleAll()"></label>';

        if (patCtrl.groupId != '') {
            patCtrl.getPatientType = 'R_GROUP';
            patCtrl.isGroup = true;

            var currentGroup = groupData.filter(function(element, index, array) {
                if (element.id == patCtrl.groupId)
                    return true;
                else
                    return false;
            });

            mainCtrl.setCurrentGroup(currentGroup[0].name);
            patCtrl.currentGroup = currentGroup[0].name;
        }

        patCtrl.dtOptions = DTOptionsBuilder
            .fromFnPromise(function() {
                if (patCtrl.groupId != '') {
                    return patientService.getPatientListWithGroup(currentBranch, patCtrl.groupId);
                } else {
                    return patientService.getPatientList(currentBranch);
                }

            })
            //.fromSource("src/models/patient.php").withDataProp('result')
            .withOption('paging', true)
            .withOption('order', [
                [11, 'desc'],
                [10, 'desc'],
                [9, 'desc']
            ])
            //.withOption("pagingType", "simple_numbers")
            .withOption("dom", "lftrip")
            .withOption('headerCallback', function(header) {
                //if (!patCtrl.headerCompiled) {
                // Use this headerCompiled field to only compile header once					
                //patCtrl.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
                //}
            })
            .withOption('createdRow', function(row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        $scope.$watch('$root.language', function() {
            //DTDefaultOptions.setLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            //patCtrl.headerCompiled = false;
            patCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            if (patCtrl.dtInstance.rerender)
                patCtrl.dtInstance.rerender();
        });

        patCtrl.dtColumnsDef = [
            DTColumnDefBuilder.newColumnDef(0).notSortable()
        ];

        patCtrl.dtColumns = [
            DTColumnBuilder.newColumn(null)
            .withTitle(titleHtml).notSortable()
            .renderWith(function(data, type, full, meta) {
                //$log.log(data)
                if (typeof(patCtrl.selected[full.id]) === "undefined") {
                    patCtrl.selected[full.id] = false;
                }
                patCtrl.userId[data.id] = data.userId;
                //return '<label><input type="checkbox" ng-model="patCtrl.selected[' + data.id + ']" ng-checked="patCtrl.selected[' + data.id + ']" ng-click="patCtrl.toggleOne(patCtrl.selected)" ></label>';
                return '<label><input type="checkbox" ng-model="patCtrl.selected[' + data.id + ']" ng-checked="patCtrl.selected[' + data.id + ']" ng-click="patCtrl.toggleOne(data.id)" ></label>';
            }),

            DTColumnBuilder.newColumn('name').withTitle('<span translate="S_03128">Patient Name</span>')
            .renderWith(function(data, type, full, meta) {
                return '<a ui-sref="main.patient({\'id\':\'' + full.id + '\'})">' + data + ' </a>';
            }),
            DTColumnBuilder.newColumn('hr').withTitle('<span translate="S_03129">PLUSE</span><p>' + DATA_UNIT['hr'] + '</p>')
            .renderWith(function(data, type, full, meta) {
                if (data >= parseInt(full.hr_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('rhr').withTitle('<span translate="S_03132">RHR</span><p>' + DATA_UNIT['hr'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(full.rhr_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('systolic').withTitle('<span translate="S_03134">SYS</span><p>' + DATA_UNIT['bp'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(full.sys_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('diastolic').withTitle('<span translate="S_03135">DIA</span><p>' + DATA_UNIT['bp'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data > parseInt(full.dia_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            // DTColumnBuilder.newColumn('co2').withTitle('CO2').renderWith(function (data, type, full, meta) {
            // 	if (data > 5000) {
            // 		return '<span class="danger">' + parseInt(data) + '</span>';
            // 	}
            // 	return (data) ? parseInt(data) : null;
            // }),
            // DTColumnBuilder.newColumn('tvocs').withTitle('TVOCS').renderWith(function (data, type, full, meta) { return (data) ? parseInt(data) : null; }),
            // DTColumnBuilder.newColumn('pm2_5').withTitle('PM2.5').renderWith(function (data, type, full, meta) { return (data) ? parseInt(data) : null; }),
            // DTColumnBuilder.newColumn('steps').withTitle('STEPS').renderWith(function (data, type, full, meta) { return (data) ? parseInt(data) : null; }),
            DTColumnBuilder.newColumn('sleep').withTitle('<span translate="S_03174">Sleep</span><p>' + DATA_UNIT['sleep'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? parseInt(data) : null; }),
            DTColumnBuilder.newColumn('bmi').withTitle('<span>BMI</span><p>' + DATA_UNIT['bmi'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? parseInt(data) : null; }),
            DTColumnBuilder.newColumn('bg').withTitle('<span translate="S_03278">blood glucose </span><p>' + DATA_UNIT['bg'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data > parseInt(full.bg_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('flag').withTitle('')
            .renderWith(function(data, type, full, meta) {

                if (data == 0 || data == null) {
                    data = 0;
                    return '<button class="btn btn-default" ng-click="patCtrl.toggleFlag(' + full.id + ',' + data + ',\'' + full.name + '\')" ><i class="fa fa-flag-o" aria-hidden="true"></i></button><span style="display:none">' + data + '</span>';
                } else {
                    return '<button class="btn btn-default"  ng-click="patCtrl.toggleFlag(' + full.id + ',' + data + ',\'' + full.name + '\')"><i class="fa fa-flag" aria-hidden="true" ></i></button><span style="display:none">' + data + '</span>';
                }

            }),
            DTColumnBuilder.newColumn('afib').withTitle('')
            .renderWith(function(data, type, full, meta) {

                if (data == 0 || data == null) {
                    data = 0;
                    return '';
                } else {
                    return '<span><img class="afib-icon" src="assets/resources/images/image_afib_h.png"></span>';
                }

            }),
            DTColumnBuilder.newColumn('laststatus').withTitle('').renderWith(function(data, type, full, meta) {
                if (data == 0 || data == null) {
                    data = 0;
                    return '<span style="display:none">' + data + '</span>';
                } else {
                    return '<button class="btn btn-default" ng-click="patCtrl.pushNotification(' + full.id + ',' + full.userId + ')"><i class="fa fa-comment" aria-hidden="true"></i></button><span style="display:none">' + data + '</span>';
                }

            })
        ];



        if (typeof($rootScope.patient) !== 'undefined') {
            $rootScope.patient = {};
        }


        patCtrl.toggleAll = function() {
            $log.log(patCtrl.selectAll);
            patCtrl.selected.map(function(x, i, ar) {
                ar[i] = patCtrl.selectAll;
            });
            $log.log(patCtrl.selected);
            patCtrl.checkSelectAll();
        }

        patCtrl.toggleOne = function(itemId) {

            if (patCtrl.selected[itemId]) {
                var arr = patCtrl.selected.fliter(function(elemt) {
                    return false;
                })
                if (arr.length == 0) {
                    patCtrl.selectAll = true;
                }
            } else {
                patCtrl.selectAll = false;
            }
            patCtrl.checkSelectAll();

        }

        patCtrl.toggleFlag = function(id, flag, name) {
            $log.log(id + " " + flag);

            patientService.setFlag(id, flag).then(function(response) {
                patCtrl.dtInstance.reloadData();
                $log.log(name + " " + response.data.resultMsg);
                var result = response.data.resultMsg;
                $translate([result], { "Name": name }).then(function(trans) {
                    $rootScope.alertBox("success", trans[result]);
                })


            }, function(failed) {
                var result = failed.data.errorCode;
                $translate([result], { "Name": name }).then(function(trans) {
                    $rootScope.alertBox("danger", trans[result]);
                })
            })

        };


        patCtrl.checkSelectAll = function() {

            var patientSelected = Object.filter(patCtrl.selected,
                function(el) { return el == false; }
            );

            if (Object.keys(patientSelected).length === 0) {
                patCtrl.selectedPat = false;
            } else {
                patCtrl.selectedPat = true;
            }
        }


        patCtrl.goDeatil = function(id, name) {
            $state.go("main.patient");
        };



        patCtrl.getPatients = function(url) {
            $log.log(patCtrl.dtInstance)
            var tmp = patCtrl.selected;
            if (patCtrl.dtInstance.reloadData)
                patCtrl.dtInstance.reloadData(null, false);
        };


        patCtrl.createPatients = function() {

            $log.log("add patient")
            $mdDialog.show({
                controller: 'patientAddController',
                templateUrl: 'view/dialog/patient_add.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                branch: currentBranch,
                /*info: html_data,*/
                controllerAs: 'patientAddCtrl',
                bindToController: true,
                onRemoving: function() { patCtrl.getPatients(); }
            });


        };

        patCtrl.pushNotification = function(patientId, userId) {
            var patientSelected = [];
            var pushList = [];
            var isAll = false;
            var isMessage = false;

            if (typeof(patientId) === "undefined") {
                patientSelected = Object.keys(Object.filter(patCtrl.selected,
                    function(el) { return el == false; }
                ));
                if (patientSelected.length == 0) {
                    patientSelected = Object.keys(patCtrl.selected);
                    isAll = true;
                }
                for (var i in patientSelected) {
                    pushList.push({ "patientId": patientSelected[i], "userId": patCtrl.userId[patientSelected[i]] });

                }
            } else {
                pushList.push({ "patientId": patientId, "userId": userId });
                isMessage = true;
            }



            $mdDialog.show({
                controller: 'messageController',
                templateUrl: 'view/dialog/push_notification.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientList: pushList, isAll: isAll, isMessage: isMessage },
                controllerAs: 'messageCtrl',
                bindToController: true

            });


        };

        //---------------- start to object filter -------------------------------
        Object.filter = function(obj, predicate) {
            var result = {},
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key) && !predicate(obj[key])) {
                    result[key] = obj[key];
                }
            }

            return result;
        };
        //---------------- end of object filter -------------------------------*/

        patCtrl.addGroup = function() {

            var patientSelected = Object.filter(patCtrl.selected,
                function(el) { return el == false; }
            );

            if (Object.keys(patientSelected).length === 0) {
                return alert('Please Select patient!');
            }

            $mdDialog.show({
                controller: 'groupAddController',
                templateUrl: 'view/dialog/group_add.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { selected: patientSelected, branch: currentBranch },
                controllerAs: 'groupAddCtrl',
                bindToController: true,
                onRemoving: function() {
                    groupService.updateList().then(function(data) {
                        mainCtrl.groups = data;
                    })
                }
            });

        };

        patCtrl.renameGroup = function() {

            var patientSelected = Object.filter(patCtrl.selected,
                function(el) { return el == false; }
            );

            //            if (Object.keys(patientSelected).length === 0) {
            //                return alert('Please Select patient!');
            //            }

            $mdDialog.show({
                controller: 'groupRenameController',
                templateUrl: 'view/dialog/group_rename.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { gName: patCtrl.currentGroup, gId: patCtrl.groupId },
                controllerAs: 'groupRenameCtrl',
                bindToController: true,
                onRemoving: function() {
                    groupService.updateList().then(function(data) {
                        mainCtrl.groups = data;
                    })
                }
            });

        };
        //no use now...maybe need add join group feature in future...
        // patCtrl.joinGroup = function () {

        //     var patientSelected = Object.filter(patCtrl.selected,
        // 		function (el) { return el == false; }
        // 	);

        //     if (Object.keys(patientSelected).length === 0) {
        //         return alert('Please Select patient!');
        //     }

        //     $mdDialog.show({
        //         controller: 'groupJoinController',
        //         templateUrl: 'view/dialog/group_join.html',
        //         parent: angular.element(document.body),
        //         clickOutsideToClose: false,
        //         fullscreen: false,
        //         selected: patientSelected,
        //         controllerAs: 'groupJoinCtrl',
        //         bindToController: true,
        //         onRemoving: function () {
        //             groupService.updateList().then(function (data) {
        // 				mainCtrl.groups = data;
        // 			})
        //         }
        //     });

        // };

        patCtrl.delGroup = function() {

            $translate(["S_03226", "S_03231", "S_03232"], { "Name": "[" + patCtrl.currentGroup + "]" }).then(function(trans) {
                var dailog_data = {
                    "message": trans.S_03226,
                    "funcOnConfirm": function() {
                        groupService.delGroup(patCtrl.groupId)
                            .then(function() {
                                groupService.updateList().then(function(data) {
                                    mainCtrl.groups = data;
                                })
                                $rootScope.alertBox("success", trans.S_03231);
                                // $state.go('main.dashboard');
                                mainCtrl.hrefHome();

                            }, function(faild) {
                                $rootScope.alertBox("danger", trans.S_03232);
                                $log.error(faild);
                            });
                    }
                }

                dialogService.confirmBox(dailog_data);


            });


        };

        patCtrl.leaveGroup = function() {

            var patientSelected = Object.filter(patCtrl.selected,
                function(el) { return el == false; }
            );

            if (Object.keys(patientSelected).length === 0) {
                return alert('Please Select patient!');
            }

            $translate(["S_03227", "S_03233", "S_03234"], { "Name": "[" + patCtrl.currentGroup + "]" }).then(function(trans) {
                var dailog_data = {
                    "message": trans.S_03227,
                    // "message": "do you want leave group [" + patCtrl.currentGroup + "]?",				
                    "funcOnConfirm": function() {
                        groupService.leaveGroup(patientSelected, patCtrl.groupId)
                            .then(function() {
                                $rootScope.alertBox("success", trans.S_03233);
                                patCtrl.dtInstance.reloadData();
                            }, function(faild) {
                                $rootScope.alertBox("danger", trans.S_03234);
                                $log.error(faild);
                            });
                    }
                };
                dialogService.confirmBox(dailog_data);
            });


        };


        patCtrl.makeCall = function() {

            //webPhoneService.makeCall("9996", document.getElementById('audio_remote'));
        }

        $timeout(function() {
            if (typeof($rootScope.timerArr.dashboardQuery) != undefined) {
                $interval.cancel($rootScope.timerArr.dashboardQuery);
            }
            patCtrl.onloading = true;
            patCtrl.getPatients();
            var timer = $interval(function() {
                patCtrl.getPatients();
            }, 11000);

            $rootScope.timerArr.dashboardQuery = timer;


        });


    });