angular.module('abeingWellness.patientsDetail').controller("deviceEditController",
	function ($log, $compile, $cookies,$filter, $rootScope, $scope, $mdDialog, deviceData, patientService) {
		$log.log("sample")
		var dateFormat = "YYYY/MM/DD";
		$scope.checkResult = true;
		$scope.errorMsg = "";
		deviceData.pickpurchaseDate = new Date(parseInt(deviceData.purchase_date) * 1000);
		$scope.device = deviceData;
		$scope.isLoading=false;
		$scope.submit = function () {
			$scope.device.purchaseDate = moment($scope.device.pickpurchaseDate).unix();
			$scope.isLoading=true;
			patientService.setDeivicePurchaseDate($scope.device).then(
				function (response) {

					var result = response.data.resultMsg;
					$rootScope.alertBox("success",$filter("translate")(result));					
					$mdDialog.cancel();
				}, function (failed) {
					$scope.isLoading=false;
					$log.log("edit Date of Purchase fail");
					$scope.checkResult = failed.data.result;
					$scope.errorMsg =$filter("translate")( failed.data.errorCode);
					return failed;
				});

		};


		$scope.hide = function () {
			$mdDialog.cancel();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

