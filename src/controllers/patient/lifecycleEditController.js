angular.module('abeingWellness.patients').controller("lifecycleEditController",
    function($log, $compile, $cookies, $filter, $rootScope, $scope, $mdDialog, $sce, patientData, patientService) {
        $scope.isLoading = false;
        patientService.getPatientData("R_ALLRULE", patientData.id).then(function (rule) {
            var rule = rule[0];
            _.each(rule, function (n, key) {
                if (key.match("time")){
                    rule[key] = moment(rule[key], "HH:mm:ss").format("HH:mm");
                } else {
                    rule[key] = (+rule[key]);
                }
            });
            $scope.rule = rule;
        });
        $scope.errorArr = [];
        $scope.checkBgData = function () {
            // bg data
            if ( $scope.rule.bg_morning_l > $scope.rule.bg_morning_h ) {
                $scope.errorArr = $scope.errorArr.concat(['bg_morning_l', 'bg_morning_h']);
            }
            if ( $scope.rule.bg_before_meal_l > $scope.rule.bg_before_meal_h ) {
                $scope.errorArr = $scope.errorArr.concat(['bg_before_meal_l', 'bg_before_meal_h']);
            }
            if ( $scope.rule.bg_after_meal_l > $scope.rule.bg_after_meal_h ) {
                $scope.errorArr = $scope.errorArr.concat(['bg_after_meal_l', 'bg_after_meal_h']);
            }
            if ( $scope.rule.bg_before_sleep_l > $scope.rule.bg_before_sleep_h ) {
                $scope.errorArr = $scope.errorArr.concat(['bg_before_sleep_l', 'bg_before_sleep_h']);
            }
            if ( $scope.rule.bg_daybreak_l > $scope.rule.bg_daybreak_h ) {
                $scope.errorArr = $scope.errorArr.concat(['bg_daybreak_l', 'bg_daybreak_h']);
            }

            // bg date range
            var time_bg_wakeup_b = moment($scope.rule.time_bg_wakeup_b, "HH:mm");
            var time_before_breakfast_b = moment($scope.rule.time_before_breakfast_b, "HH:mm");
            var time_after_breakfast_b = moment($scope.rule.time_after_breakfast_b, "HH:mm");
            var time_before_lunch_b = moment($scope.rule.time_before_lunch_b, "HH:mm");
            var time_after_lunch_b = moment($scope.rule.time_after_lunch_b, "HH:mm");
            var time_before_dinner_b = moment($scope.rule.time_before_dinner_b, "HH:mm");
            var time_after_dinner_b = moment($scope.rule.time_after_dinner_b, "HH:mm");
            var time_sleep_b = moment($scope.rule.time_sleep_b, "HH:mm");
            var time_bg_night_b = moment($scope.rule.time_bg_night_b, "HH:mm");
            var time_bg_wakeup_e = moment($scope.rule.time_bg_wakeup_e, "HH:mm");
            var time_before_breakfast_e = moment($scope.rule.time_before_breakfast_e, "HH:mm");
            var time_after_breakfast_e = moment($scope.rule.time_after_breakfast_e, "HH:mm");
            var time_before_lunch_e = moment($scope.rule.time_before_lunch_e, "HH:mm");
            var time_after_lunch_e = moment($scope.rule.time_after_lunch_e, "HH:mm");
            var time_before_dinner_e = moment($scope.rule.time_before_dinner_e, "HH:mm");
            var time_after_dinner_e = moment($scope.rule.time_after_dinner_e, "HH:mm");
            var time_sleep_e = moment($scope.rule.time_sleep_e, "HH:mm");
            var time_bg_night_e = moment($scope.rule.time_bg_night_e, "HH:mm");

            var time_bg_wakeup_ok = time_bg_wakeup_b.isAfter(time_bg_night_e) && time_bg_wakeup_e.isBefore(time_before_breakfast_b);
            var time_before_breakfast_ok = time_before_breakfast_b.isAfter(time_bg_wakeup_e) && time_before_breakfast_e.isBefore(time_after_breakfast_b);
            var time_after_breakfast_ok = time_after_breakfast_b.isAfter(time_before_breakfast_e) && time_after_breakfast_e.isBefore(time_before_lunch_b);
            var time_before_lunch_ok = time_before_lunch_b.isAfter(time_after_breakfast_e) && time_before_lunch_e.isBefore(time_after_lunch_b);
            var time_after_lunch_ok = time_after_lunch_b.isAfter(time_before_lunch_e) && time_after_lunch_e.isBefore(time_before_dinner_b);
            var time_before_dinner_ok = time_before_dinner_b.isAfter(time_after_lunch_e) && time_before_dinner_e.isBefore(time_after_dinner_b);
            var time_after_dinner_ok = time_after_dinner_b.isAfter(time_before_dinner_e) && time_after_dinner_e.isBefore(time_sleep_b);
            var time_sleep_ok = time_sleep_b.isAfter(time_after_dinner_e) && time_sleep_e.isBefore(time_bg_night_b);
            var time_bg_night_ok = time_bg_night_b.isAfter(time_sleep_e) && time_bg_night_e.isBefore(time_bg_wakeup_b);

            if ( !time_bg_wakeup_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_bg_wakeup_b', 'time_bg_wakeup_e']);
            }
            if ( !time_before_breakfast_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_before_breakfast_b', 'time_before_breakfast_e']);
            }
            if ( !time_after_breakfast_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_after_breakfast_b', 'time_after_breakfast_e']);
            }
            if ( !time_before_lunch_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_before_lunch_b', 'time_before_lunch_e']);
            }
            if ( !time_after_lunch_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_after_lunch_b', 'time_after_lunch_e']);
            }
            if ( !time_before_dinner_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_before_dinner_b', 'time_before_dinner_e']);
            }
            if ( !time_after_dinner_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_after_dinner_b', 'time_after_dinner_e']);
            }
            if ( !time_sleep_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_sleep_b', 'time_sleep_e']);
            }
            if ( !time_bg_night_ok ) {
                $scope.errorArr = $scope.errorArr.concat(['time_bg_night_b', 'time_bg_night_e']);
            }
        };
        $scope.checkBpData = function () {
            // bp data
            if ($scope.rule.sys_wakeup_l > $scope.rule.sys_wakeup_h) {
                $scope.errorArr = $scope.errorArr.concat(['sys_wakeup_l', 'sys_wakeup_h']);
            }
            if ($scope.rule.sys_morning_l > $scope.rule.sys_morning_h) {
                $scope.errorArr = $scope.errorArr.concat(['sys_morning_l', 'sys_morning_h']);
            }
            if ($scope.rule.sys_noon_l > $scope.rule.sys_noon_h) {
                $scope.errorArr = $scope.errorArr.concat(['sys_noon_l', 'sys_noon_h']);
            }
            if ($scope.rule.sys_afternoon_l > $scope.rule.sys_afternoon_h) {
                $scope.errorArr = $scope.errorArr.concat(['sys_afternoon_l', 'sys_afternoon_h']);
            }
            if ($scope.rule.sys_night_l > $scope.rule.sys_night_h) {
                $scope.errorArr = $scope.errorArr.concat(['sys_night_l', 'sys_night_h']);
            }
            if ($scope.rule.dia_wakeup_l > $scope.rule.dia_wakeup_h) {
                $scope.errorArr = $scope.errorArr.concat(['dia_wakeup_l', 'dia_wakeup_h']);
            }
            if ($scope.rule.dia_morning_l > $scope.rule.dia_morning_h) {
                $scope.errorArr = $scope.errorArr.concat(['dia_morning_l', 'dia_morning_h']);
            }
            if ($scope.rule.dia_noon_l > $scope.rule.dia_noon_h) {
                $scope.errorArr = $scope.errorArr.concat(['dia_noon_l', 'dia_noon_h']);
            }
            if ($scope.rule.dia_afternoon_l > $scope.rule.dia_afternoon_h) {
                $scope.errorArr = $scope.errorArr.concat(['dia_afternoon_l', 'dia_afternoon_h']);
            }
            if ($scope.rule.dia_night_l > $scope.rule.dia_night_h) {
                $scope.errorArr = $scope.errorArr.concat(['dia_night_l', 'dia_night_h']);
            }

            // bp time range
            var time_bp_wake_b = moment($scope.rule.time_bp_wake_b, "HH:mm");
            var time_morning_b = moment($scope.rule.time_morning_b, "HH:mm");
            var time_noon_b = moment($scope.rule.time_noon_b, "HH:mm");
            var time_afternoon_b = moment($scope.rule.time_afternoon_b, "HH:mm");
            var time_bp_night_b = moment($scope.rule.time_bp_night_b, "HH:mm");
            var time_bp_wake_e = moment($scope.rule.time_bp_wake_e, "HH:mm");
            var time_morning_e = moment($scope.rule.time_morning_e, "HH:mm");
            var time_noon_e = moment($scope.rule.time_noon_e, "HH:mm");
            var time_afternoon_e = moment($scope.rule.time_afternoon_e, "HH:mm");
            var time_bp_night_e = moment($scope.rule.time_bp_night_e, "HH:mm");

            var wakeOk = time_bp_wake_b.isAfter(time_bp_night_e) && time_bp_wake_e.isBefore(time_morning_b);
            var morningOk = time_morning_b.isAfter(time_bp_wake_e) && time_morning_e.isBefore(time_noon_b);
            var noonOk = time_noon_b.isAfter(time_morning_e) && time_noon_e.isBefore(time_afternoon_b);
            var afternoonOk = time_afternoon_b.isAfter(time_noon_e) && time_afternoon_e.isBefore(time_bp_night_b);
            var nightOk = time_bp_night_b.isAfter(time_afternoon_e) && time_bp_night_e.isBefore(time_bp_wake_b);
            if (!wakeOk) {
                $scope.errorArr = $scope.errorArr.concat(['time_bp_wake_b', 'time_bp_wake_e']);
            }
            if (!morningOk) {
                $scope.errorArr = $scope.errorArr.concat(['time_morning_b', 'time_morning_e']);
            }
            if (!noonOk) {
                $scope.errorArr = $scope.errorArr.concat(['time_noon_b', 'time_noon_e']);
            }
            if (!afternoonOk) {
                $scope.errorArr = $scope.errorArr.concat(['time_afternoon_b', 'time_afternoon_e']);
            }
            if (!nightOk) {
                $scope.errorArr = $scope.errorArr.concat(['time_bp_night_b', 'time_bp_night_e']);
            }
        };
        $scope.lifecycleActive = 0;
        $scope.tabItems = [
            {
                title: "血糖目標值與生活習慣時間",
                templateUrl: ''
            },
            {
                title: "血壓目標值與生活習慣時間",
                templateUrl: ''
            }
        ];
        $scope.trustAsHtml = $sce.trustAsHtml;


        $scope.indexOfTab = 0;
        $scope.rerender = function(index) {
            $scope.indexOfTab = index;
            if (index > 0) return;
            angular.element($mdDialog).triggerHandler('rerender');

        };

        $scope.requestEditBpData = function () {
            var data = {
                sys_wakeup_l : +$scope.rule.sys_wakeup_l,
                sys_morning_l : +$scope.rule.sys_morning_l,
                sys_noon_l : +$scope.rule.sys_noon_l,
                sys_afternoon_l : +$scope.rule.sys_afternoon_l,
                sys_night_l : +$scope.rule.sys_night_l,
                sys_wakeup_h : +$scope.rule.sys_wakeup_h,
                sys_morning_h : +$scope.rule.sys_morning_h,
                sys_noon_h : +$scope.rule.sys_noon_h,
                sys_afternoon_h : +$scope.rule.sys_afternoon_h,
                sys_night_h : +$scope.rule.sys_night_h,
                dia_wakeup_l : +$scope.rule.dia_wakeup_l,
                dia_morning_l : +$scope.rule.dia_morning_l,
                dia_noon_l : +$scope.rule.dia_noon_l,
                dia_afternoon_l : +$scope.rule.dia_afternoon_l,
                dia_night_l : +$scope.rule.dia_night_l,
                dia_wakeup_h : +$scope.rule.dia_wakeup_h,
                dia_morning_h : +$scope.rule.dia_morning_h,
                dia_noon_h : +$scope.rule.dia_noon_h,
                dia_afternoon_h : +$scope.rule.dia_afternoon_h,
                dia_night_h : +$scope.rule.dia_night_h,
                time_bp_wake_b : $scope.rule.time_bp_wake_b,
                time_morning_b : $scope.rule.time_morning_b,
                time_noon_b : $scope.rule.time_noon_b,
                time_afternoon_b : $scope.rule.time_afternoon_b,
                time_bp_night_b : $scope.rule.time_bp_night_b,
                time_bp_wake_e : $scope.rule.time_bp_wake_e,
                time_morning_e : $scope.rule.time_morning_e,
                time_noon_e : $scope.rule.time_noon_e,
                time_afternoon_e : $scope.rule.time_afternoon_e,
                time_bp_night_e : $scope.rule.time_bp_night_e
            };
            patientService.editBp(patientData.id, data).then(function() {
                $rootScope.alertBox("success", "修改血壓目標值與生活習慣成功。");
                $scope.isLoading = false;
            }, function() {
                $rootScope.alertBox("error", "修改血壓目標值與生活習慣失敗。");
                $scope.isLoading = false;
            })
        };

        $scope.requestEditBgData = function () {
            var data = {
                bg_morning_l : +$scope.rule.bg_morning_l,
                bg_before_meal_l : +$scope.rule.bg_before_meal_l,
                bg_after_meal_l : +$scope.rule.bg_after_meal_l,
                bg_before_sleep_l : +$scope.rule.bg_before_sleep_l,
                bg_daybreak_l : +$scope.rule.bg_daybreak_l,
                bg_morning_h : +$scope.rule.bg_morning_h,
                bg_before_meal_h : +$scope.rule.bg_before_meal_h,
                bg_after_meal_h : +$scope.rule.bg_after_meal_h,
                bg_before_sleep_h : +$scope.rule.bg_before_sleep_h,
                bg_daybreak_h : +$scope.rule.bg_daybreak_h,
                time_bg_wakeup_b : $scope.rule.time_bg_wakeup_b,
                time_before_breakfast_b : $scope.rule.time_before_breakfast_b,
                time_after_breakfast_b : $scope.rule.time_after_breakfast_b,
                time_before_lunch_b : $scope.rule.time_before_lunch_b,
                time_after_lunch_b : $scope.rule.time_after_lunch_b,
                time_before_dinner_b : $scope.rule.time_before_dinner_b,
                time_after_dinner_b : $scope.rule.time_after_dinner_b,
                time_sleep_b : $scope.rule.time_sleep_b,
                time_bg_night_b : $scope.rule.time_bg_night_b,
                time_bg_wakeup_e : $scope.rule.time_bg_wakeup_e,
                time_before_breakfast_e : $scope.rule.time_before_breakfast_e,
                time_after_breakfast_e : $scope.rule.time_after_breakfast_e,
                time_before_lunch_e : $scope.rule.time_before_lunch_e,
                time_after_lunch_e : $scope.rule.time_after_lunch_e,
                time_before_dinner_e : $scope.rule.time_before_dinner_e,
                time_after_dinner_e : $scope.rule.time_after_dinner_e,
                time_sleep_e : $scope.rule.time_sleep_e,
                time_bg_night_e : $scope.rule.time_bg_night_e
            };
            patientService.editBg(patientData.id, data).then(function () {
                $rootScope.alertBox("success", "修改血糖目標值與生活習慣成功。");
                $scope.isLoading = false;
            }, function () {
                $rootScope.alertBox("error", "修改血糖目標值與生活習慣失敗。");
                $scope.isLoading = false;
            });
        };

        $scope.submit = function() {
            $scope.isLoading = true;
            $scope.errorArr = [];
            $scope.checkBgData();
            $scope.checkBpData();
            if ($scope.errorArr.length === 0) {
                $scope.requestEditBgData();
                $scope.requestEditBpData();
                $mdDialog.cancel();
            }
        };


        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };


    });
