angular.module('abeingWellness.patients').controller("messageController",
	function ($log, $compile, $cookies, $rootScope, $scope,$filter, $mdDialog,patientService,patientList,isAll,isMessage,Session) {
		$scope.pushList={};
		
		$scope.isAll=isAll;
		$scope.pushNumber=patientList.length;
		$scope.pushList.sender=Session.userId;
		$scope.pushList.receiver=patientList;
		$scope.title=(isMessage)?"Leave Message":"Push Notification";
		$scope.isMessage=isMessage;
		$scope.isLoading=false;
		$scope.submit = function () {
			$scope.isLoading=true;		
			patientService.pushNotification($scope.pushList).then(
				function(response){
					
					$rootScope.alertBox("success",$filter("translate")("S_03282"));
					$mdDialog.cancel();
				},
				function(failed){
					$scope.isLoading=false;
					$rootScope.alertBox("danger",$filter("translate")("S_03283"));
					$mdDialog.cancel();
				}

			)
		};

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

