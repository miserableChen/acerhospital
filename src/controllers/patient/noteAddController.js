angular.module('abeingWellness.patients').controller("noteAddController",
    function($log, $compile, $cookies, $filter, $rootScope, $scope, $mdDialog, $sce, NoteAddService, patientData, patientService) {
        $scope.isLoading = false;
        $scope.classItems = [
            { label: "來電處理", value: "來電處理" },
            { label: "去電訪", value: "去電訪" }
        ]
        $log.log(patientData);
        NoteAddService.init();
        $scope.selectClass = "來電處理";
        $scope.selectItems = [];
        $scope.trustAsHtml = $sce.trustAsHtml;
        $scope.selectItems = NoteAddService.selectItems;
        $scope.selectTemplate = NoteAddService.selectTemplate;
        $scope.categoryItems = NoteAddService.categoryItems;

        // $scope.categoryItems = [
        //     { category: "例行性關懷", subcategory: [{ id: 0, description: "例行性電訪", checked: false }] },
        //     { category: "住院或急診訪視", subcategory: [{ id: 1, description: "急診關懷", checked: false }, { id: 2, description: "住院訪視", checked: false }, { id: 3, description: "緊急處理", checked: false }] },
        //     { category: "量測值異常", subcategory: [{ id: 4, description: "低血糖", checked: false }, { id: 5, description: "高血糖", checked: false }, { id: 6, description: "血壓異常", checked: false }] }
        // ]

        $scope.submit = function() {
            $scope.isLoading = true;
            $scope.$broadcast('setValue'); // going down!

            var data = {
                service_type: $scope.selectClass,
                note_content: []
            }

            for (var i in $scope.selectTemplate) {
                data.note_content.push({
                    cause: $scope.selectTemplate[i].label,
                    description: $scope.selectTemplate[i].post.data,
                    comment: $scope.selectTemplate[i].post.comment,
                    label_id: $scope.selectTemplate[i].id

                })

            }
            patientService.setNote(patientData.id, data).then(function() {
                $rootScope.alertBox("success", "Add note success")
                $mdDialog.cancel();
            }, function() {
                $rootScope.alertBox("error", "Add note error")
                NoteAddService.cleanSelect();
            })

        };

        $scope.removeSelectItem = function(id) {
            NoteAddService.removeSelectItem(id);
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            NoteAddService.cleanSelect();
            $mdDialog.cancel();
        };


    });