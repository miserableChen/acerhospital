angular.module('abeingWellness.patients').controller("patientAddController",
    function($log, $compile, $cookies, $filter, $rootScope, $scope, $mdDialog, patientService, branch) {

        $scope.patient = {};
        $scope.patient.pickbirthday = new Date();
        $scope.isLoading = false;
        $scope.patient.gender = "male";
        $scope.patient.branchId = branch;
        $scope.submit = function() {
            $scope.isLoading = true;
            $scope.patient.name = $scope.patient.firstName + " " + $scope.patient.lastName;
            $scope.patient.birthday = moment($scope.patient.pickbirthday).format('YYYY-MM-DD');
            $scope.patient.email = $scope.patient.username;
            patientService.create($scope.patient).then(
                function(response) {

                    // var patientId=response.data.patientId;
                    // var emgName=$scope.patient.emgName;
                    // var emgPhone=$scope.patient.emgPhone;
                    // patientService.setEmergencyContact(patientId,emgName,emgPhone).then(function(response){
                    $rootScope.alertBox("success", $filter("translate")("S_03220"));
                    $log.log(response.resultMsg);
                    $mdDialog.cancel();
                    // },function(failded){
                    // 	$scope.isLoading=false;
                    // 	$rootScope.alertBox("danger", $filter("translate")("S_03222"));
                    // 	$log.log("set contact fail");
                    // })

                },
                function(failed) {
                    $scope.isLoading = false;
                    $rootScope.alertBox("danger", $filter("translate")("S_03222"));
                    $log.log("API fail")
                        //$mdDialog.cancel();
                })

        };

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };


    });