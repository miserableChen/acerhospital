angular.module('abeingWellness.patientsDetail').controller("patientDetailController",
    function($scope, $compile, $rootScope, $log, $cookies, $window, $state, $filter, $translate, Session, patientInfo, packageList, $mdDialog,
        AUTH_EVENTS, fhirConfig, dialogService, patientService, dataService, branchData, eChartService, purchaseService, DATA_UNIT, DATA_TYPE,
        DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {
        var patDCtrl = this;
        var branchList = branchData.result;
        var timeArr = [];
        patDCtrl.maxDate = new Date(moment().day(0).format("YYYY/MM/DD"));
        patDCtrl.dataList = [];
        patDCtrl.purchaseData = [];
        patDCtrl.deviceData = [];
        patDCtrl.noteData = [];
        patDCtrl.prescriptionData = {};
        patDCtrl.exportData = [];
        patDCtrl.dataHeader = ["Time", "Pulse", "RHR", "SYS", "DIA", "Sleep", "BMI", "BMR", "Weight", "Body Fat", "Steps", "CO2", "PM2.5", "TVOCs", "Blood Glucose"];
        patDCtrl.loaded = false;
        patDCtrl.showChart = false;
        patDCtrl.showPeriodTimeBar = true;
        patDCtrl.active = 0;
        patDCtrl.overviewActive = 0;
        patDCtrl.id = patientInfo.id;
        patDCtrl.userId = patientInfo.userId;
        patDCtrl.name = patientInfo.name;
        patDCtrl.chartOption = {lineChart:{}};
        patDCtrl.chartConfig = {};
        patDCtrl.title = "";
        patDCtrl.infoList = patientInfo;
        patDCtrl.typeList = {};
        patDCtrl.queryStartDate = new Date(moment().day(-90).format("YYYY/MM/DD"));
        patDCtrl.queryEndDate = new Date(moment().format("YYYY/MM/DD"));
        patDCtrl.searchMethod = 'timePeriod';
        patDCtrl.BGDataObject = {
            allRuleData: []
        };
        patDCtrl.chartOption.boxplotData = [];
        patDCtrl.BPDataObject = {};
        patDCtrl.tabList = [{
                title: $filter('translate')("S_03153"),
                templateUrl: 'view/overview.html'
            },
            {
                title: $filter('translate')("S_02779"),
                templateUrl: 'view/data.html'
            },
            {
                title: $filter('translate')("S_03157"),
                templateUrl: 'view/communication.html'
            },
            {
                title: $filter('translate')("S_03052"),
                templateUrl: 'view/prescription.html'
            },
            {
                title: $filter('translate')("S_03158"),
                templateUrl: 'view/device.html'
            },

            {
                title: $filter('translate')("S_03159"),
                templateUrl: 'view/purchase.html'
            },
            {
                title: $filter('translate')("Threshold"),
                templateUrl: 'view/threshold.html'
            },
            {
                title: $filter('translate')("Note"),
                templateUrl: 'view/note.html'
            }
        ];
        patDCtrl.overviewTabList = [{
                title: "圖表",
                templateUrl: 'view/overview/chartDetail.html'
            },
            {
                title: "原始數據",
                templateUrl: 'view/overview/tableDetail.html'
            }
        ];
        patDCtrl.placement = {
            options: [
                {id:1, start: moment().add(-6, "day").format("YYYY/MM/DD"), end: moment().format("YYYY/MM/DD"), text: '最近一周'},
                {id:2, start: moment().add(-27, "day").format("YYYY/MM/DD"), end: moment().format("YYYY/MM/DD"), text: '最近一個月'},
                {id:3, start: moment().add(-83, "day").format("YYYY/MM/DD"), end: moment().format("YYYY/MM/DD"), text: '最近三個月'}
            ],
            selected: {}
        };
        patDCtrl.placement.selected = patDCtrl.placement.options[0];
        patDCtrl.periodTime=[];
        patDCtrl.ruleMap = {
            // for bg
            time_bg_wakeup : {prefix:"time_bg_wakeup", b:"time_bg_wakeup_b", e:"time_bg_wakeup_e", low:"bg_morning_l", high:"bg_morning_h"},
            time_before_breakfast : {prefix:"time_before_breakfast", b:"time_before_breakfast_b", e:"time_before_breakfast_e", low:"bg_before_meal_l", high:"bg_before_meal_h"},
            time_after_breakfast : {prefix:"time_after_breakfast", b:"time_after_breakfast_b", e:"time_after_breakfast_e", low:"bg_after_meal_l", high:"bg_before_meal_h"},
            time_before_lunch : {prefix:"time_before_lunch", b:"time_before_lunch_b", e:"time_before_lunch_e", low:"bg_before_meal_l", high:"bg_before_meal_h"},
            time_after_lunch : {prefix:"time_after_lunch", b:"time_after_lunch_b", e:"time_after_lunch_e", low:"bg_after_meal_l", high:"bg_before_meal_h"},
            time_before_dinner : {prefix:"time_before_dinner", b:"time_before_dinner_b", e:"time_before_dinner_e", low:"bg_before_meal_l", high:"bg_before_meal_h"},
            time_after_dinner : {prefix:"time_after_dinner", b:"time_after_dinner_b", e:"time_after_dinner_e", low:"bg_after_meal_l", high:"bg_before_meal_h"},
            time_sleep : {prefix:"time_sleep", b:"time_sleep_b", e:"time_sleep_e", low:"bg_before_sleep_l", high:"bg_before_sleep_h"},
            time_bg_night : {prefix:"time_bg_night", b:"time_bg_night_b", e:"time_bg_night_e", low:"bg_daybreak_l", high:"bg_daybreak_h"},

            // f
            time_bp_wakeup: {postfix:"wakeup", b:"time_bp_wakeup_b", e:"time_bp_wake_e", sys_low:"sys_wakeup_l", sys_high:"sys_wakeup_h", dia_low:"dia_wakeup_l", dia_high:"dia_wakeup_h"},
            time_morning: {postfix:"morning", b:"time_morning_b", e:"time_morning_e", sys_low:"sys_morning_l", sys_high:"sys_morning_h", dia_low:"dia_morning_l", dia_high:"dia_morning_h"},
            time_noon: {postfix:"noon", b:"time_noon_b", e:"time_noon_e", sys_low:"sys_noon_l", sys_high:"sys_noon_h", dia_low:"dia_noon_l", dia_high:"dia_noon_h"},
            time_afternoon: {postfix:"afternoon", b:"time_afternoon_b", e:"time_afternoon_e", sys_low:"sys_afternoon_l", sys_high:"sys_afternoon_h", dia_low:"dia_afternoon_l", dia_high:"dia_afternoon_h"},
            time_bp_night: {postfix:"night", b:"time_bp_night_b", e:"time_bp_night_e", sys_low:"sys_night_l", sys_high:"sys_night_h", dia_low:"dia_night_l", dia_high:"dia_night_h"}
        };
        patDCtrl.keyName = {
            avg: '平均',
            max: '最大值',
            min: '最小值',
            num: '資料筆數',
            goal_low: '低於目標值筆數',
            goal_in: '正常值筆數',
            goal_high: '高於目標值筆數'
        };
        var dateFormat = "MM/DD/YYYY";

        var typeTitle = {
            "BP": "S_03048",
            "SYS": "S_03049",
            "DIA": "S_03050",
            "HR": "S_03129",
            "RHR": "S_03132",
            "BG": "S_03278",
            "WEIGHT": "S_03056",
            "BFP": "S_03175",
            "BMI": "BMI",
            "BMR": "S_03176",
            "STEPS": "S_03257",
            "SLEEPTIME": "S_03174",
            "TVOCS": "S_03139",
            "CO2": "S_03137",
            "PM2_5": "S_03141"
        }

        var typeKeyword = {
            "BP": "bp",
            "SYS": "bp",
            "DIA": "bp",
            "HR": "hr",
            "RHR": "rhr",
            "BG": "bg",
            "WEIGHT": "weight",
            "BFP": "bfp",
            "BMI": "bmi",
            "BMR": "bmr",
            "STEPS": "step",
            "SLEEPTIME": "sleep",
            "TVOCS": "tvocs",
            "CO2": "co2",
            "PM2_5": "pm2_5"
        }



        patDCtrl.threshold = {
            "id": patientInfo.id,
            "sys_rule": parseInt(patDCtrl.infoList.sys_rule),
            "dia_rule": parseInt(patDCtrl.infoList.dia_rule),
            "bg_rule": parseInt(patDCtrl.infoList.bg_rule),
            "hr_rule": parseInt(patDCtrl.infoList.hr_rule),
            "rhr_rule": parseInt(patDCtrl.infoList.rhr_rule),
            "co2_rule": parseInt(patDCtrl.infoList.co2_rule)
        };
        patDCtrl.origThreshold = {
            "id": patientInfo.id,
            "sys_rule": parseInt(patDCtrl.infoList.sys_rule),
            "dia_rule": parseInt(patDCtrl.infoList.dia_rule),
            "bg_rule": parseInt(patDCtrl.infoList.bg_rule),
            "hr_rule": parseInt(patDCtrl.infoList.hr_rule),
            "rhr_rule": parseInt(patDCtrl.infoList.rhr_rule),
            "co2_rule": parseInt(patDCtrl.infoList.co2_rule)
        };

        //=============================patient Data =============================================
        patDCtrl.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return patientService.getPatientData("R_DATA", patDCtrl.id) })
            .withOption('paging', true)
            .withOption('order', [0, 'desc'])
            .withOption("dom", "pltrip")
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            }).withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        $scope.bgData = [];

        $scope.bgDataGroups = [];

        var getMinuteOfTime = function (timeStr) {
            var time = moment(timeStr, "hhmmss");
            return time.hour() * 60 + time.minute();
        };

        var inTimeRange = function (minuteOfToday, fromStr, toStr) {
            var oneDayMin = 60 * 24;
            var fromMin = getMinuteOfTime(fromStr);
            var toMin = getMinuteOfTime(toStr);
            var isCrossDay = toMin < fromMin;
            if (minuteOfToday == fromMin || minuteOfToday ==toMin) {
                return true;
            }
            return isCrossDay
                ? _.inRange(minuteOfToday, fromMin, oneDayMin) || _.inRange(minuteOfToday, 0, toMin)
                : _.inRange(minuteOfToday, fromMin, toMin);
        };

        /**
         * value=BG值
         * goal=(-1:低於目標、0:介於目標、1:高於目標)
         */
        patDCtrl.getBgDataInterval = function (dataArr, rule) {
            // default value
            var bgDataResult = {
                time_bg_wakeup: {value: null, goal: -1},
                time_before_breakfast: {value: null, goal: -1},
                time_after_breakfast: {value: null, goal: -1},
                time_before_lunch: {value: null, goal: -1},
                time_after_lunch: {value: null, goal: -1},
                time_before_dinner: {value: null, goal: -1},
                time_after_dinner: {value: null, goal: -1},
                time_sleep: {value: null, goal: -1},
                time_bg_night: {value: null, goal: -1}
            };
            _.each(dataArr, function (bgData) {
                var bgMoment = moment(new Date(bgData.timestamp * 1000));
                var minuteOfToday = bgMoment.hour() * 60 + bgMoment.minute();
                var value = +bgData.bg;
                var is_time_bg_wakeup = inTimeRange(minuteOfToday, rule['time_bg_wakeup_b'], rule['time_bg_wakeup_e']);
                var is_time_before_breakfast = inTimeRange(minuteOfToday, rule['time_before_breakfast_b'], rule['time_before_breakfast_e']);
                var is_time_after_breakfast = inTimeRange(minuteOfToday, rule['time_after_breakfast_b'], rule['time_after_breakfast_e']);
                var is_time_before_lunch = inTimeRange(minuteOfToday, rule['time_before_lunch_b'], rule['time_before_lunch_e']);
                var is_time_after_lunch = inTimeRange(minuteOfToday, rule['time_after_lunch_b'], rule['time_after_lunch_e']);
                var is_time_before_dinner = inTimeRange(minuteOfToday, rule['time_before_dinner_b'], rule['time_before_dinner_e']);
                var is_time_after_dinner = inTimeRange(minuteOfToday, rule['time_after_dinner_b'], rule['time_after_dinner_e']);
                var is_time_sleep = inTimeRange(minuteOfToday, rule['time_sleep_b'], rule['time_sleep_e']);
                var is_time_bg_night = inTimeRange(minuteOfToday, rule['time_bg_night_b'], rule['time_bg_night_e']);
                var ruleKeys = is_time_bg_wakeup ?
                    patDCtrl.ruleMap.time_bg_wakeup : is_time_before_breakfast ?
                        patDCtrl.ruleMap.time_before_breakfast : is_time_after_breakfast ?
                            patDCtrl.ruleMap.time_after_breakfast : is_time_before_lunch ?
                                patDCtrl.ruleMap.time_before_lunch : is_time_after_lunch ?
                                    patDCtrl.ruleMap.time_after_lunch : is_time_before_dinner ?
                                        patDCtrl.ruleMap.time_before_dinner : is_time_after_dinner ?
                                            patDCtrl.ruleMap.time_after_dinner : is_time_sleep ?
                                                patDCtrl.ruleMap.time_sleep : is_time_bg_night ?
                                                    patDCtrl.ruleMap.time_bg_night : null;
                if (ruleKeys) {
                    bgDataResult[ruleKeys.prefix].value = value;
                    bgDataResult[ruleKeys.prefix].goal = value < +rule[ruleKeys.low] ? -1 : +value > rule[ruleKeys.high] ? 1 : 0;
                }
            });

            return bgDataResult;
        };

        patDCtrl.getBpDataInterval = function (dataArr, rule) {
            // default value
            var bpDataResult = {
                // 收縮壓
                sys_wakeup: {value: null, goal: -1},
                sys_morning: {value: null, goal: -1},
                sys_noon: {value: null, goal: -1},
                sys_afternoon: {value: null, goal: -1},
                sys_night: {value: null, goal: -1},
                // 舒張壓
                dia_wakeup: {value: null, goal: -1},
                dia_morning: {value: null, goal: -1},
                dia_noon: {value: null, goal: -1},
                dia_afternoon: {value: null, goal: -1},
                dia_night: {value: null, goal: -1},
                // 心跳
                pulse_wakeup: {value: null, goal: -1},
                pulse_morning: {value: null, goal: -1},
                pulse_noon: {value: null, goal: -1},
                pulse_afternoon: {value: null, goal: -1},
                pulse_night: {value: null, goal: -1},
            };
            _.each(dataArr, function (data) {
                var dataMoment = moment(new Date(data.timestamp * 1000));
                var minuteOfToday = dataMoment.hour() * 60 + dataMoment.minute();
                var sys_value = +data.systolic;
                var dia_value = +data.diastolic;
                var pulse_value = +data.hr;
                var is_time_bp_wakeup = inTimeRange(minuteOfToday, rule['time_bp_wake_b'], rule['time_bp_wake_e']);
                var is_time_morning = inTimeRange(minuteOfToday, rule['time_morning_b'], rule['time_morning_e']);
                var is_time_noon = inTimeRange(minuteOfToday, rule['time_noon_b'], rule['time_noon_e']);
                var is_time_afternoon = inTimeRange(minuteOfToday, rule['time_afternoon_b'], rule['time_afternoon_e']);
                var is_time_bp_night = inTimeRange(minuteOfToday, rule['time_bp_night_b'], rule['time_bp_night_e']);
                var ruleKeys = is_time_bp_wakeup ?
                    patDCtrl.ruleMap.time_bp_wakeup : is_time_morning ?
                        patDCtrl.ruleMap.time_morning : is_time_noon ?
                            patDCtrl.ruleMap.time_noon : is_time_afternoon ?
                                patDCtrl.ruleMap.time_afternoon : is_time_bp_night ?
                                    patDCtrl.ruleMap.time_bp_night : null;
                if (ruleKeys) {
                    bpDataResult["sys_" + ruleKeys.postfix].value = sys_value;
                    bpDataResult["sys_" + ruleKeys.postfix].goal = sys_value < +rule[ruleKeys.sys_low] ? -1 : +sys_value > rule[ruleKeys.sys_high] ? 1 : 0;
                    bpDataResult["dia_" + ruleKeys.postfix].value = dia_value;
                    bpDataResult["dia_" + ruleKeys.postfix].goal = dia_value < +rule[ruleKeys.dia_low] ? -1 : +dia_value > rule[ruleKeys.dia_high] ? 1 : 0;
                    bpDataResult["pulse_" + ruleKeys.postfix].value = pulse_value;
                    bpDataResult["pulse_" + ruleKeys.postfix].goal = 0;
                }
            });

            return bpDataResult;
        };

        $scope.bgDataGroup = [];
        $scope.getDataGroupAvg = function (key, fromIdx, arrLength) {
            var bgDataArr = _.chain($scope.bgDataGroup)
                .slice(fromIdx, fromIdx+arrLength)
                .filter(function (data) {
                    return data.bgData[key].value;
                })
                .value();

            if (bgDataArr.length > 0) {
                return (_.sum(bgDataArr, function (data) {
                        return +data.bgData[key].value;
                    }) / bgDataArr.length).toFixed();
            } else {
                return '';
            }
        };

        $scope.getBpDataGroupAvg = function (key, fromIdx, arrLength) {
            var dataArr = _.chain($scope.bpDataGroup)
                .slice(fromIdx, fromIdx+arrLength)
                .filter(function (data) {
                    return data.bpData[key].value;
                })
                .value();

            if (dataArr.length > 0) {
                return (_.sum(dataArr, function (data) {
                        return +data.bpData[key].value;
                    }) / dataArr.length).toFixed();
            } else {
                return '';
            }
        };
        patDCtrl.getBgDataGroup = function (data, allRuleData, start, end) {
            // mock date
            var from = moment(start);
            var to = moment(end);
            var momentRun = to.clone();
            var result = [];
            data = _.map(data, function (d) {
                if (d.bg === 0) {
                    d.bg = null;
                }
                return d;
            });
            data = _.filter(data, function (d) {
                return !!d.bg;
            });

            while (momentRun.isAfter(from)) {
                var subOneDay = momentRun.clone().add(-1, "day");
                var oneDayPackage = {end: momentRun.format("MM/DD/YYYY"), start: subOneDay.format("MM/DD/YYYY")};
                var dataInDate = _.filter(data, function (bg) {
                    return moment(new Date(bg.timestamp * 1000)).isBetween(subOneDay, momentRun);
                });

                // if (bgInDate.length > 0) {
                    oneDayPackage.bgData = patDCtrl.getBgDataInterval(dataInDate, allRuleData);
                    result.push(oneDayPackage);
                // }
                momentRun.add(-1, "day");
            }

            return result;
        };

        patDCtrl.getBpDataGroup = function (data, allRuleData, start, end) {
            // mock date
            var from = moment(start);
            var to = moment(end);
            var momentRun = to.clone();
            var result = [];
            data = _.map(data, function (d) {
                if (d.systolic === 0) {
                    d.systolic = null;
                }
                if (d.diastolic === 0) {
                    d.diastolic = null;
                }
                if (d.hr === 0) {
                    d.hr = null;
                }
                return d;
            });
            data = _.filter(data, function (d) {
                return !!d.systolic || !!d.diastolic || !!d.hr;
            });
            while (momentRun.isAfter(from)) {
                var subOneDay = momentRun.clone().add(-1, "day");
                var oneDayPackage = {end: momentRun.format("MM/DD/YYYY"), start: subOneDay.format("MM/DD/YYYY")};
                var dataInDate = _.filter(data, function (bg) {
                    return moment(new Date(bg.timestamp * 1000)).isBetween(subOneDay, momentRun);
                });

                // if (bgInDate.length > 0) {
                oneDayPackage.bpData = patDCtrl.getBpDataInterval(dataInDate, allRuleData);
                result.push(oneDayPackage);
                // }
                momentRun.add(-1, "day");
            }

            return result;
        };

        var oriBpDataSummary = {
            avg: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            max: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            min: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            num: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            goal_low: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            goal_in: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0},
            goal_high: { sys_wakeup: 0, sys_morning: 0, sys_noon: 0, sys_afternoon: 0, sys_night: 0, dia_wakeup: 0, dia_morning: 0, dia_noon: 0, dia_afternoon: 0, dia_night: 0, pulse_wakeup: 0, pulse_morning: 0, pulse_noon: 0, pulse_afternoon: 0, pulse_night: 0}
        };

        $scope.bpDataSummary = _.cloneDeep(oriBpDataSummary);

        var setBpDataSummary = function () {
            $scope.bpDataSummary = _.cloneDeep(oriBpDataSummary);
            var bpDataSummary = $scope.bpDataSummary;
            var bpRuleKeys = ["time_bp_wakeup", "time_morning", "time_noon", "time_afternoon", "time_bp_night"];
            _.each(bpRuleKeys , function (key) {
                var rule = patDCtrl.ruleMap[key];
                var intervalSysValues = [];
                var intervalDiaValues = [];
                var intervalPulseValues = [];
                _.each($scope.bpDataGroup, function (dataGroup) {
                    var sysInterval = dataGroup.bpData["sys_" + rule.postfix];
                    var diaInterval = dataGroup.bpData["dia_" + rule.postfix];
                    var pulseInterval = dataGroup.bpData["pulse_" + rule.postfix];
                    if (sysInterval.value !== null) {
                        intervalSysValues.push(+sysInterval.value);
                        switch (sysInterval.goal) {
                            case -1:
                                bpDataSummary.goal_low["sys_" + rule.postfix]++;
                                break;
                            case 0:
                                bpDataSummary.goal_in["sys_" + rule.postfix]++;
                                break;
                            case 1:
                                bpDataSummary.goal_high["sys_" + rule.postfix]++;
                        }
                    }
                    if (diaInterval.value !== null) {
                        intervalDiaValues.push(+diaInterval.value);
                        switch (diaInterval.goal) {
                            case -1:
                                bpDataSummary.goal_low["dia_" + rule.postfix]++;
                                break;
                            case 0:
                                bpDataSummary.goal_in["dia_" + rule.postfix]++;
                                break;
                            case 1:
                                bpDataSummary.goal_high["dia_" + rule.postfix]++;
                        }
                    }
                    if (pulseInterval.value !== null) {
                        intervalPulseValues.push(+pulseInterval.value);
                        switch (pulseInterval.goal) {
                            case -1:
                                bpDataSummary.goal_low["pulse_" + rule.postfix]++;
                                break;
                            case 0:
                                bpDataSummary.goal_in["pulse_" + rule.postfix]++;
                                break;
                            case 1:
                                bpDataSummary.goal_high["pulse_" + rule.postfix]++;
                        }
                    }
                });

                if ( intervalSysValues.length > 0 ) {
                    bpDataSummary.max["sys_" + rule.postfix] = _.max(intervalSysValues);
                    bpDataSummary.min["sys_" + rule.postfix] = _.min(intervalSysValues);
                    bpDataSummary.num["sys_" + rule.postfix] = intervalSysValues.length;
                    bpDataSummary.avg["sys_" + rule.postfix] = (_.sum(intervalSysValues) / intervalSysValues.length).toFixed();
                }
                if ( intervalDiaValues.length > 0 ) {
                    bpDataSummary.max["dia_" + rule.postfix] = _.max(intervalDiaValues);
                    bpDataSummary.min["dia_" + rule.postfix] = _.min(intervalDiaValues);
                    bpDataSummary.num["dia_" + rule.postfix] = intervalDiaValues.length;
                    bpDataSummary.avg["dia_" + rule.postfix] = (_.sum(intervalDiaValues) / intervalDiaValues.length).toFixed();
                }
                if ( intervalPulseValues.length > 0 ) {
                    bpDataSummary.max["pulse_" + rule.postfix] = _.max(intervalPulseValues);
                    bpDataSummary.min["pulse_" + rule.postfix] = _.min(intervalPulseValues);
                    bpDataSummary.num["pulse_" + rule.postfix] = intervalPulseValues.length;
                    bpDataSummary.avg["pulse_" + rule.postfix] = (_.sum(intervalPulseValues) / intervalPulseValues.length).toFixed();
                }
            });

            $scope.bpDataSummary = bpDataSummary;
        };

        $scope.startLtEnd = true;

        $scope.isStartLtEnd = function () {
            $scope.startLtEnd = moment(patDCtrl.queryStartDate).isSameOrBefore(moment(patDCtrl.queryEndDate));
        };

        var setBgDataSummary = function () {
            $scope.bgDataSummary = _.cloneDeep(oriBgDataSummary);
            var bgDataSummary = $scope.bgDataSummary;
            var bgRuleKeys = ["time_bg_wakeup", "time_before_breakfast", "time_after_breakfast", "time_before_lunch", "time_after_lunch", "time_before_dinner", "time_after_dinner", "time_sleep", "time_bg_night"];
            _.each(bgRuleKeys , function (key) {
                var rule = patDCtrl.ruleMap[key];
                var intervalValues = [];
                _.each($scope.bgDataGroup, function (dataGroup) {
                    var interval = dataGroup.bgData[rule.prefix];
                    if (interval.value !== null) {
                        intervalValues.push(+interval.value);
                        switch (interval.goal) {
                            case -1:
                                bgDataSummary.goal_low[rule.prefix]++;
                                break;
                            case 0:
                                bgDataSummary.goal_in[rule.prefix]++;
                                break;
                            case 1:
                                bgDataSummary.goal_high[rule.prefix]++;
                        }
                    }
                });

                if ( intervalValues.length > 0 ) {
                    bgDataSummary.max[rule.prefix] = _.max(intervalValues);
                    bgDataSummary.min[rule.prefix] = _.min(intervalValues);
                    bgDataSummary.num[rule.prefix] = intervalValues.length;
                    bgDataSummary.avg[rule.prefix] = (_.sum(intervalValues) / intervalValues.length).toFixed();
                }
            });

            $scope.bgDataSummary = bgDataSummary;
        };

        var oriBgDataSummary = {
            avg: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            max: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            min: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            num: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            goal_low: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            goal_in: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
            goal_high: { time_bg_wakeup: 0, time_before_breakfast: 0, time_after_breakfast: 0, time_before_lunch: 0, time_after_lunch: 0, time_before_dinner: 0, time_after_dinner: 0, time_sleep: 0, time_bg_night: 0},
        };

        $scope.bgDataSummary = _.cloneDeep(oriBgDataSummary);

        $scope.allRuleData = [];

        patDCtrl.setRawData = function (start, end, callFunction) {
            patDCtrl.start = moment(start).format("MM/DD/YYYY");
            patDCtrl.end = moment(end).format("MM/DD/YYYY");
            start = +moment(start).format('x');
            end = +moment(end).add(1, "day").format('x');
            patientService.getPatientData("R_DATA_RANGE", patDCtrl.id, start/1000, end/1000).then(function (data) {
                patientService.getPatientData("R_ALLRULE", patDCtrl.id).then(function (allRuleData) {
                    $scope.allRuleData = allRuleData;

                    $scope.bgDataGroup = patDCtrl.getBgDataGroup(data, allRuleData[0], start, end);
                    setBgDataSummary();
                    $scope.bpDataGroup = patDCtrl.getBpDataGroup(data, allRuleData[0], start, end);
                    setBpDataSummary();
                    if(callFunction){
                        callFunction(patDCtrl.start,patDCtrl.end);
                    }
                });
            });
        };

        patDCtrl.dtColumns = [
            DTColumnBuilder.newColumn('timestamp').withTitle('<span translate="S_00299">Time</span>').renderWith(
                function(data, type, full, meta) {
                    return (data) ? moment(data * 1000).format('YYYY-MM-DD HH:mm:ss') : null;
                }),
            DTColumnBuilder.newColumn('hr').withTitle('<span translate="S_03129">PLUSEs</span><p>' + DATA_UNIT['hr'] + '</p>')
            .renderWith(function(data, type, full, meta) {
                if (data >= parseInt(patDCtrl.infoList.hr_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('rhr').withTitle('<span translate="S_03132">RHR</span><p>' + DATA_UNIT['hr'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(patDCtrl.infoList.rhr_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('systolic').withTitle('<span translate="S_03134">SYS</span><p>' + DATA_UNIT['bp'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(patDCtrl.infoList.sys_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('diastolic').withTitle('<span translate="S_03135">DIA</span><p>' + DATA_UNIT['bp'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(patDCtrl.infoList.dia_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('sleep').withTitle('<span translate="S_03174">Sleep</span><p>' + DATA_UNIT['sleep'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('bg').withTitle('<span translate="S_03278">blood glucose </span><p>' + DATA_UNIT['bg'] + '</p>').renderWith(function(data, type, full, meta) {
                if (data >= parseInt(patDCtrl.infoList.bg_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('bmi').withTitle('<span translate="S_03267">BMI</span><p>' + DATA_UNIT['bmi'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('bmr').withTitle('<span translate="S_03176">BMR</span><p>' + DATA_UNIT['bmr'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('wgt').withTitle('<span translate="S_03056">Weight</span><p>' + DATA_UNIT['wgt'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('bfp').withTitle('<span translate="S_03175">Body Fat</span><p>' + DATA_UNIT['bfp'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('steps').withTitle('<span translate="S_03257">Steps</span><p>' + DATA_UNIT['steps'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('co2').withTitle('<span translate="S_03137">CO2</span><p>' + DATA_UNIT['co2'] + '</p>').renderWith(function(data, type, full, meta) {

                if (data >= parseInt(patDCtrl.infoList.co2_rule)) {
                    return '<span class="danger">' + parseInt(data) + '</span>';
                }
                return (data) ? parseInt(data) : null;
            }),
            DTColumnBuilder.newColumn('pm2_5').withTitle('<span translate="S_03141">PM2.5</span><p>' + DATA_UNIT['pm2_5'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('tvocs').withTitle('<span translate="S_03139">TVOCs</span><p>' + DATA_UNIT['tvocs'] + '</p>').renderWith(function(data, type, full, meta) { return (data) ? data : null; })
        ];

        patDCtrl.dtInstance = {};


        //=============================Communcation Data =============================================
        patDCtrl.commOptions = DTOptionsBuilder.fromFnPromise(function() { return patientService.getPatientData("R_COMM_DATA", patDCtrl.id) })
            .withOption('paging', true)

        .withOption("dom", "pltrip")
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        patDCtrl.commColumns = [
            DTColumnBuilder.newColumn('timestamp').withTitle('<span translate="S_00500">Date</span>').renderWith(
                function(data, type, full, meta) {
                    return moment(data * 1000).format('YYYY-MM-DD');
                }),
            DTColumnBuilder.newColumn('sender_name').withTitle('<span translate="S_03262">Sender</span>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('receiver_name').withTitle('<span translate="S_03263">Receiver</span>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('content').withTitle('<span translate="S_02900">Content</span>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),

        ];
        patDCtrl.commInstance = {};

        //=============================Prescription Data =============================================

        patDCtrl.preOptions = DTOptionsBuilder.fromFnPromise(function() { return patientService.getPatientData("R_PRE_DATA", patDCtrl.id) })
            .withOption('paging', true)

        .withOption("dom", "pltrip")
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            })
            .withOption('createdRow', function(row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        patDCtrl.preColumns = [
            DTColumnBuilder.newColumn('id').withTitle("").renderWith(function(data, type, full, meta) {
                patDCtrl.prescriptionData[data] = full.data;
                return data;
            }).notVisible(),
            DTColumnBuilder.newColumn('timestamp').withTitle('<span translate="S_03168">Upload date</span>').renderWith(
                function(data, type, full, meta) {
                    return moment(data * 1000).format('YYYY-MM-DD');
                }),
            DTColumnBuilder.newColumn('name').withTitle('<span translate="S_03052">Prescription</span>').renderWith(function(data, type, full, meta) {
                return '<a ng-click="patDCtrl.prescription(' + full.id + ')">' + data + '<a>';
            })

        ];
        patDCtrl.preInstance = {};

        //=============================Device Data =============================================
        patDCtrl.devOptions = DTOptionsBuilder.fromFnPromise(function() { return patientService.getPatientData("R_DEV_INFO", patDCtrl.id) })
            .withOption('paging', true)

        .withOption("dom", "pltrip")
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            })
            .withOption('createdRow', function(row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        patDCtrl.devColumns = [
            DTColumnBuilder.newColumn('dev_name').withTitle('<span translate="S_03158">Device</span>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn(null).withTitle('').withClass('col-edit').renderWith(function(data, type, full, meta) {
                patDCtrl.deviceData[data.id] = data;
                return '<a ng-click="patDCtrl.editDevice(patDCtrl.deviceData[' + data.id + '])"><i class="fa fa-cog" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('').withClass('col-edit').renderWith(function(data, type, full, meta) {

                return '<a ng-click="patDCtrl.unlinkDevice(\'' + data.dev_name + '\',\'' + data.userId + '\',\'' + data.serialNum + '\')"><i class="fa fa-trash" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn('purchase_date').withTitle('<span translate="S_03169">Date of Purchase</span>').renderWith(function(data, type, full, meta) {
                return (data) ? moment(data * 1000).format('YYYY-MM-DD') : null;
            })
        ];
        patDCtrl.devInstance = {};
        //=============================Purchase Data =============================================

        patDCtrl.purOptions = DTOptionsBuilder.fromFnPromise(function() { return purchaseService.list(patDCtrl.id) })
            .withOption('paging', true)
            .withOption('order', [4, 'asc'])

        .withOption("dom", "pltrip")
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            })
            .withOption('createdRow', function(row, data, dataIndex) { //if datatable item can't call controller function need add this code
                // Recompiling so we can bind Angular directive to the DT				
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        patDCtrl.purColumns = [
            DTColumnBuilder.newColumn('title').withTitle('<span translate="S_03116">Service</span>').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn(null).withTitle('').withClass('col-edit').renderWith(function(data, type, full, meta) {
                patDCtrl.purchaseData[data.id] = data;
                return '<a ng-click="patDCtrl.editPurchase(patDCtrl.purchaseData[' + data.id + '])"><i class="fa fa-cog" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('').withClass('col-edit').renderWith(function(data, type, full, meta) {
                return '<a ng-click="patDCtrl.deletePurchase(patDCtrl.purchaseData[' + data.id + '])"><i class="fa fa-trash" aria-hidden="true" /></a>';
            }).notSortable(),
            DTColumnBuilder.newColumn('monthValue').withTitle('<span translate="S_03059">Package</span>').renderWith(function(data, type, full, meta) {
                return (data) ? data + " months" : null;
            }),
            DTColumnBuilder.newColumn('startDate').withTitle('<span translate="S_03162">Start Date</span>').renderWith(function(data, type, full, meta) { return (data) ? moment(data).format("YYYY-MM-DD") : null; }),
            DTColumnBuilder.newColumn('endDate').withTitle('<span translate="S_03163">End Date</span>').renderWith(function(data, type, full, meta) { return (data) ? moment(data).format("YYYY-MM-DD") : null; })
        ];
        patDCtrl.purInstance = {};


        //=============================Purchase Data =============================================

        patDCtrl.noteOptions = DTOptionsBuilder.fromFnPromise(function() {
                var result = patientService.getNote(patDCtrl.id);
                result.then(function(data) {
                    patDCtrl.noteData = data;
                })

                return result
            })
            .withOption('paging', true)
            .withOption("dom", "pltrip")
            .withOption('order', [0, 'desc'])
            .withOption('headerCallback', function(header) {
                $compile(angular.element(header).contents())($scope);
            })
            .withOption('createdRow', function(row, data, dataIndex) { //if datatable item can't call controller function need add this code
                // Recompiling so we can bind Angular directive to the DT				
                $compile(angular.element(row).contents())($scope);
            })
            .withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        patDCtrl.noteColumns = [
            DTColumnBuilder.newColumn('begin_time').withTitle('服務時間').renderWith(function(data, type, full, meta) {
                return (data) ? moment(data * 1000).format("YYYY-MM-DD HH:mm") : null;
            }),
            DTColumnBuilder.newColumn('service_type').withTitle('服務類別').renderWith(function(data, type, full, meta) { return (data) ? data : null; }),
            DTColumnBuilder.newColumn('note_content').withTitle('服務筆記').withClass('service').renderWith(function(data, type, full, meta) {
                data = eval(data);

                var result = "<table class='nest-table'>"
                for (var i in data) {
                    result += "<tr>"
                    result += "<td class='category'>"
                    result += data[i].cause;
                    result += "</td>"
                    result += "<td>"
                    result += data[i].description;
                    if (data[i].comment.length > 0)
                        result += "<br>註解：" + data[i].comment;
                    result += "</td>"
                    result += "</tr>"

                }
                result += "</table>"
                return result;
            }).notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('').withClass('col-edit').renderWith(function(data, type, full, meta) {
                return '<a ng-click="patDCtrl.editNote(' + data.id + ')"><i class="fa fa-cog" aria-hidden="true" /></a>';
            }).notSortable(),

        ];
        patDCtrl.noteInstance = {};


        $scope.$watch('$root.language', function() {
            patDCtrl.dtOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            patDCtrl.purOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            patDCtrl.devOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            patDCtrl.preOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');
            patDCtrl.commOptions.withLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');



        });

        patDCtrl.getPatientInfo = function() {

            patientService.getPatientData('R', patDCtrl.id).then(
                function(data) {
                    patDCtrl.infoList = patientService.setPatientInfo(data[0]);
                    patDCtrl.getPatientLastData();
                    patDCtrl.origThreshold = {
                        "id": patientInfo.id,
                        "sys_rule": parseInt(patDCtrl.infoList.sys_rule),
                        "dia_rule": parseInt(patDCtrl.infoList.dia_rule),
                        "bg_rule": parseInt(patDCtrl.infoList.bg_rule),
                        "hr_rule": parseInt(patDCtrl.infoList.hr_rule),
                        "rhr_rule": parseInt(patDCtrl.infoList.rhr_rule),
                        "co2_rule": parseInt(patDCtrl.infoList.co2_rule)
                    };
                    patDCtrl.name = patDCtrl.infoList.name;
                },
                function(failed) {
                    $log.log("fail");
                }
            )
        };
        patDCtrl.getPatientLastData = function() {
            patDCtrl.typeList.length = 0;
            $log.log(patDCtrl.typeList)
            patientService.getPatientData("R_LASTDATA", patDCtrl.id).then(function(data) {
                var arr = [];
                for (key in data) {
                    if (data[key].type == "AFIB" || data[key].type == "SLEEPSTART")
                        continue;
                    data[key].abnomal = false;
                    data[key].title = typeTitle[data[key].type];
                    switch (data[key].type) {
                        case "BP":
                            var systolic = parseInt(data[key].dataValue.split(";")[0]);
                            var diastolic = parseInt(data[key].dataValue.split(";")[1]);
                            if (systolic >= parseInt(patDCtrl.infoList.sys_rule) || diastolic >= parseInt(patDCtrl.infoList.dia_rule)) {
                                data[key].abnomal = true
                            }
                            data[key].dataValue = data[key].dataValue.replace(';', '/')
                            break;
                        case "HR":
                            $log.log(parseInt(data[key].dataValue) >= parseInt(patDCtrl.infoList.hr_rule));
                            if (parseInt(data[key].dataValue) >= parseInt(patDCtrl.infoList.hr_rule)) {
                                data[key].abnomal = true;
                            }
                        case "BG":
                            if (parseInt(data[key].dataValue) >= parseInt(patDCtrl.infoList.bg_rule)) {
                                data[key].abnomal = true;
                            }
                        case "RHR":
                            if (parseInt(data[key].dataValue) >= parseInt(patDCtrl.infoList.rhr_rule)) {
                                data[key].abnomal = true;
                            }
                            break;
                        case "CO2":
                            if (parseInt(data[key].dataValue) >= parseInt(patDCtrl.infoList.co2_rule)) {
                                data[key].abnomal = true;
                            }
                            break;
                        case "WEIGHT":
                        case "BFP":
                        case "BMI":
                            data[key].dataValue = new Number(data[key].dataValue).toFixed(2);
                            break;
                    }


                    arr.push(data[key]);

                }

                patDCtrl.typeList = arr;

            })
        };

        patDCtrl.getPatientDetail = function() {
            patDCtrl.loaded = false;
            patDCtrl.dataList = [];

            patientService.getPatientData("R_DATA", patDCtrl.id).then(function(data) {
                var historys = data;
                patDCtrl.dataList = historys;
                var response_field = ['timestamp', 'hr', 'rhr', 'systolic', 'diastolic', 'sleep', 'bmi', 'bmr', 'wgt', 'bfp', 'steps', 'co2', 'pm2_5', 'tvocs'];
                patDCtrl.exportData = patientService.createExportData(historys, response_field);
                patDCtrl.loaded = true;
            });
        };

        patDCtrl.showChartCard = function(type) {
            patDCtrl.currentType = DATA_TYPE[type];
            patDCtrl.showChart = true;
            patDCtrl.title = eChartService.getChartTitle(patDCtrl.currentType);
            patDCtrl.showPeriodTimeBar = type !== "BP";
            patDCtrl.setRawData(patDCtrl.queryStartDate, patDCtrl.queryEndDate, patDCtrl.drawChart);
        };


        patDCtrl.hideChartCard = function() {
            patDCtrl.showChart = false;
            patDCtrl.currentType = '';
        };


        patientService.getPatientData("R_ALLRULE", patDCtrl.id).then(function (allRuleData) {
            patDCtrl.BGDataObject.setAllRuleData(allRuleData);
        });
        patDCtrl.setBGDataObject = function(keyName, content){
            patDCtrl.BGDataObject[keyName] = content;
        };
        var tooltipFormatter =function (param) {
            return [
                param.name + ': ',
                '最大值: ' + param.data[5],
                '中位數: ' + param.data[3],
                '最小值: ' + param.data[1]
            ].join('<br/>')
        };
        patDCtrl.BGDataObject = $.extend(false, patDCtrl.BGDataObject,{
            getRuleColumn: function(){
                return {
                    morningTime:{time_bg_wakeup: "晨起", time_before_breakfast: "早餐前", time_after_breakfast:"早餐後"},
                    lunchTime:{time_before_lunch: "午餐前", time_after_lunch: "午餐後"},
                    dinnerTime:{time_before_dinner: "晚餐前", time_after_dinner: "晚餐後", time_sleep:"睡前"},
                    earlyMorning:{time_bg_night: "凌晨"}
                }
            },
            setAllRuleData: function(allRuleData){
                patDCtrl.BGDataObject.allRuleData = allRuleData;
            },
            getPieChartData: function(ruleType){
                var isNullData = true,
                    bgDataSummary = $scope.bgDataSummary,
                    goalArray = ["goal_high","goal_in","goal_low"],
                    valueArray = ['max','num','min'],
                    ruleTotal = 0,
                    legendData = [],
                    pieChartData;
                    goalArray.forEach(function(value){
                        ruleTotal = bgDataSummary[value][ruleType] + ruleTotal;
                    });
                    pieChartData = valueArray.map(function(value,key){
                        isNullData = bgDataSummary[goalArray[key]][ruleType] ? false:isNullData;
                        var labelText = {max: "高",num: "正常",min:"低"},
                            ruleValue = bgDataSummary[goalArray[key]][ruleType],
                            ruleValueAvg = Math.floor((ruleValue/ruleTotal)*100)/100*100,
                            lengText = labelText[value] + ": " + ruleValueAvg + "% (" + ruleValue + "/" + ruleTotal + ")";
                        legendData.push(lengText);
                        return $.extend(false,{},{
                            value: ruleValue,
                            name: lengText
                        })
                    });
                return {
                    isNullData: isNullData,
                    pieChartData: pieChartData,
                    legendData:legendData
                };
            },
            getPeriodTime: function(){
                var ruleColumnArray = {},
                    ruleMap = patDCtrl.ruleMap,
                    ruleRangeData = patDCtrl.BGDataObject.allRuleData[0],
                    ruleColumnObject = patDCtrl.BGDataObject.getRuleColumn();
                Object.keys(ruleColumnObject).forEach(function(object){
                    var timeRules = ruleColumnObject[object];
                    ruleColumnArray[object] = Object.keys(timeRules).map(function(timeRule){
                        var pieOption = eChartService.getDefaultOption("pie"),
                            ruleValue = ruleMap[timeRule],
                            pieChartData = patDCtrl.BGDataObject.getPieChartData(timeRule);
                        pieOption.legend.data = pieChartData.legendData;
                        pieOption.series[0].data = pieChartData.pieChartData;
                        return $.extend(false, {},{
                            title: timeRules[timeRule] + "(" + ruleRangeData[ruleValue.b] + "~" + ruleRangeData[ruleValue.e] + ")",
                            boxTitle: "目標:" + ruleRangeData[ruleValue.low] + "~" + ruleRangeData[ruleValue.high] + " mg/dl",
                            noData: pieChartData.isNullData,
                            pieChart: pieOption
                        });
                    });
                });
                patDCtrl.setBGDataObject("periodTime",ruleColumnArray);
            },
            getBoxPlotChartData: function(request){
                var BGRule = ['time_bg_wakeup','time_before_breakfast','time_after_breakfast','time_before_lunch', 'time_after_lunch',
                    'time_before_dinner', 'time_after_dinner', 'time_sleep', 'time_bg_night'];
                var xAxisData= {
                    1:{}, 2:{}, 3:{}, 4:{}, 5:{}, 6:{}, 7:{}, 8:{}, 9:{}
                };
                request.forEach(function(object,key){
                    var nowDate = moment().format("YYYY-MM-DD"),
                        allRule = $scope.allRuleData[0],
                        date = moment(new Date(object.timestamp * 1000)).format('YYYY/MM/DD'),
                        dataTimeMoment = moment(new Date(object.timestamp * 1000)).format('HH:mm'),
                        interval,
                        dataValue = object.dataValue;
                    for(var i = 0; i<BGRule.length; i++){
                        var isBetween = moment(nowDate+ " " + dataTimeMoment).isBetween(nowDate + " " + allRule[BGRule[i]+"_b"], nowDate + " " + allRule[BGRule[i]+"_e"]);
                        if(isBetween){
                            interval = i+1;
                            break;
                        }
                    }
                    xAxisData[interval] ? xAxisData[interval][date] = parseFloat(dataValue) : false;
                });
                var boxplotData = [];
                Object.keys(xAxisData).forEach(function(value){
                    xAxisData[value] = $.map(xAxisData[value], function(value) {
                        return value
                    });
                    boxplotData.push(xAxisData[value]);
                });
                return {
                    xAxisData: ["晨起","早餐前","早餐後","午餐前","午餐後","晚餐前","晚餐後","睡前","凌晨"],
                    yAxisName: "mh/dl",
                    boxplotData: echarts.dataTool.prepareBoxplotData(boxplotData, {boundIQR: 'none'}).boxData
                };
            }
        });
        patDCtrl.BPDataObject = $.extend(false, patDCtrl.BPDataObject,{
            getBoxPlotChartData: function(request){
                var BPRule = ['time_bp_wake','time_morning','time_noon','time_afternoon', 'time_bp_night'];
                var xAxisData= {
                    1:{sys:{},dia:{},pulse:{}},
                    2:{sys:{},dia:{},pulse:{}},
                    3:{sys:{},dia:{},pulse:{}},
                    4:{sys:{},dia:{},pulse:{}},
                    5:{sys:{},dia:{},pulse:{}}
                };
                request.forEach(function(object){
                    var nowDate = moment().format("YYYY-MM-DD"),
                        allRule = $scope.allRuleData[0],
                        date = moment(new Date(object.timestamp * 1000)).format('YYYY/MM/DD'),
                        dataTimeMoment = moment(new Date(object.timestamp * 1000)).format('HH:mm'),
                        interval,
                        bpValue = object.dataValue.split(";"),
                        hrValue = object.hrValue;
                    for(var i = 0; i<BPRule.length; i++){
                        var isBetween = moment(nowDate+ " " + dataTimeMoment).isBetween(nowDate + " " + allRule[BPRule[i]+"_b"], nowDate + " " + allRule[BPRule[i]+"_e"]);
                        if(isBetween){
                            interval = i+1;
                            break;
                        }
                    }
                    bpValue && xAxisData[interval] ? xAxisData[interval]["sys"][date]=parseFloat(bpValue[0]) : false;
                    bpValue && xAxisData[interval] ? xAxisData[interval]["dia"][date]=parseFloat(bpValue[1]) : false;
                    parseFloat(hrValue) && xAxisData[interval] ? xAxisData[interval]["pulse"][date]=parseFloat(hrValue) : false;
                });
                var sysValueData = [],diaValueData=[],pulseValueData=[];
                Object.keys(xAxisData).forEach(function(value){
                    var xAxisObject = xAxisData[value];
                    Object.keys(xAxisObject).forEach(function(value){
                        xAxisObject[value] = $.map(xAxisObject[value], function(value) {
                            return value
                        });
                        value === "sys" ? sysValueData.push(xAxisObject[value]):false;
                        value === "dia" ? diaValueData.push(xAxisObject[value]):false;
                        value === "pulse" ? pulseValueData.push(xAxisObject[value]):false;
                    });
                });
                return {
                    xAxisData: ["晨起","早上","中午","下午","晚上"],
                    yAxisName: ["mmHg","bpm"],
                    series: [{
                        type: 'boxplot',
                        data: echarts.dataTool.prepareBoxplotData(sysValueData, {boundIQR: 'none'}).boxData,
                        tooltip: {
                            formatter: tooltipFormatter
                        }
                    }, {
                        type: 'boxplot',
                        data: echarts.dataTool.prepareBoxplotData(diaValueData, {boundIQR: 'none'}).boxData,
                        tooltip: {
                            formatter: tooltipFormatter
                        }
                    },{
                        type: 'boxplot',
                        data: echarts.dataTool.prepareBoxplotData(pulseValueData, {boundIQR: 'none'}).boxData,
                        tooltip: {
                            formatter: tooltipFormatter
                        }
                    }]
                };
            }
        });
        patDCtrl.drawChart = function(start,end) {
            var config = eChartService.getDefaultConfig(),
                startDate = patDCtrl.queryStartDate,
                endDate = patDCtrl.queryEndDate,
                successFunction = function(request) {
                    patDCtrl.currentType ==="bg" || patDCtrl.currentType ==="bp" ? patDCtrl.drawBoxplotChart(request) : false;
                    patDCtrl.currentType ==="bg" ? patDCtrl.BGDataObject.getPeriodTime() : false;
                    patDCtrl.drawLineChart(request);
                };
            config.dataLoaded = false;
            patDCtrl.chartConfig = config;
            $(".flot-chart-content").addClass("finish");
            if(start && end){
                startDate = start;
                endDate = end;
            }
            patDCtrl.currentType ==="bp" ?
                patientService.getPatientBPData(patDCtrl.id, startDate, endDate).then(successFunction):
                patientService.getPatientChartData(patDCtrl.currentType, patDCtrl.id, startDate, endDate).then(successFunction);
        };
        patDCtrl.drawLineChart = function(request){
            var option = eChartService.getDefaultOption("line");
            option.series = eChartService.getSeries(patDCtrl.currentType, request);
            var legends = [];
            for (var i in option.series) {
                var legend = {
                    "name": option.series[i].name,
                    "icon": "rect",
                    "textStyle": {
                        fontSize: 18,
                        color: '#979797'
                    }
                };
                legends.push(legend);
            }
            option.legend.data = legends;
            $log.log(eChartService.parseDatasetToTime(patDCtrl.currentType, request));
            option.xAxis.data = eChartService.parseDatasetToTime(patDCtrl.currentType, request);
            if (request.length > 0) {
                option.yAxis.name = request[0].unit;
            }
            patDCtrl.chartConfig.dataLoaded = true;
            patDCtrl.chartOption.lineChart = option;
        };
        patDCtrl.drawBoxplotChart = function(request){
            var currentType = patDCtrl.currentType ==="bg" ? "BGDataObject" : "BPDataObject",
                boxplotOption = patDCtrl[currentType].getBoxPlotChartData(request),
                series,
                chartYAxisObject = {
                    type: 'value',
                    nameLocation: 'end',
                    nameGap: 21,
                    interval: 50,
                    min: 0,
                    //max: 350
                };
            patDCtrl.chartOption.boxplot = eChartService.getDefaultOption("boxplot");
            patDCtrl.chartOption.boxplot['xAxis']['data'] = boxplotOption.xAxisData;
            if(patDCtrl.currentType ==="bg"){
                patDCtrl.chartOption.boxplot['yAxis'].push($.extend(false,chartYAxisObject, {
                    name:boxplotOption.yAxisName,
                    interval: 50
                }));
                series = [{
                    type: 'boxplot',
                    data: boxplotOption.boxplotData,
                    tooltip: {
                        formatter: tooltipFormatter
                    }
                }];
            }else{
                patDCtrl.chartOption.boxplot['yAxis'].push($.extend(false,chartYAxisObject, {
                    name:boxplotOption.yAxisName[0],
                    max: 240,
                    interval: 40
                }));
                patDCtrl.chartOption.boxplot['yAxis'].push($.extend(false,chartYAxisObject, {
                    name:boxplotOption.yAxisName[1],
                    max: 300,
                    interval: 50
                }));
                series = boxplotOption.series;
            }
            patDCtrl.chartOption.boxplot['series'] = series;
        };

        patDCtrl.toggleFlag = function() {

            $mdDialog.show({
                controller: function($scope, flag, $mdDialog, patientService) {
                    $scope.flag = flag;
                    $scope.close = function() {
                        $mdDialog.cancel();
                    }

                    $scope.confirm = function() {

                        patientService.setFlag(patDCtrl.id, $scope.flag).then(function(response) {

                            $rootScope.alertBox("success", patDCtrl.name + " " + response.data.resultMsg);
                            $mdDialog.cancel();
                        }, function(failed) {
                            $rootScope.alertBox("error", patDCtrl.name + " " + response.data.resultMsg);
                            $mdDialog.cancel();
                        })

                    }
                },
                locals: { "flag": (patDCtrl.infoList.flag == "1") },
                templateUrl: 'view/dialog/flag.html',
                parent: angular.element(document.body),
                onRemoving: function() {
                    patDCtrl.getPatientInfo();
                }

            })

        };

        patDCtrl.createPurchase = function() {

            $mdDialog.show({
                controller: 'purchaseAddController',
                templateUrl: 'view/dialog/purchase_add.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { packageData: packageList, patientId: patDCtrl.id },
                controllerAs: 'purchaseAddCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.getPatientInfo();
                    patDCtrl.purInstance.reloadData();
                }
            });
        };


        patDCtrl.showEditor = function () {
            $mdDialog.show({
                controller: 'thresholdAndLifecycleController',
                templateUrl: 'view/dialog/thresholdAndLifecycle_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: {},
                controllerAs: 'thresholdAndLifecycleCtrl',
                bindToController: true
            });
        };

        patDCtrl.editPurchase = function(data) {
            data.package = data.packageId + "/" + data.monthValue;
            data.pickStartDate = new Date(data.startDate);
            $mdDialog.show({
                controller: 'purchaseEditController',
                templateUrl: 'view/dialog/purchase_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { packageData: packageList, purchaseData: data },
                controllerAs: 'purchaseEditCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.getPatientInfo();
                    patDCtrl.purInstance.reloadData();
                }
            });


        };


        patDCtrl.deletePurchase = function(data) {
            $log.log(data);
            $translate(["S_03280", "S_03239", "S_03240"], { Name: data.title }).then(function(trans) {
                var dailog_data = {
                    "titile": "Delete Purchase",
                    "message": trans.S_03280,
                    "funcOnConfirm": function() {
                        purchaseService.del(data).then(function(response) {
                            var result = response.data.resultMsg;
                            $rootScope.alertBox("success", trans[result]);
                            patDCtrl.getPatientInfo();
                            patDCtrl.purInstance.reloadData();

                        }, function(faild) {
                            var result = faild.data.resultMsg;
                            $rootScope.alertBox("danger", trans[result]);
                            patDCtrl.getPatientInfo();
                            patDCtrl.purInstance.reloadData();

                        })
                    }
                }

                dialogService.confirmBox(dailog_data);


            })


        };

        patDCtrl.group = function() {
            $mdDialog.show({
                controller: 'groupEditController',
                templateUrl: 'view/dialog/group_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                patientId: patDCtrl.id,
                controllerAs: 'groupEditCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.getPatientDetail();
                    patDCtrl.purInstance.reloadData();
                }
            });
        }

        patDCtrl.pushMessage = function() {
            var patientSelected = [];
            var pushList = [];
            var isAll = false;
            var isMessage = true;

            pushList.push({ "patientId": patDCtrl.id, "userId": patDCtrl.userId });

            $mdDialog.show({
                controller: 'messageController',
                templateUrl: 'view/dialog/push_notification.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientList: pushList, isAll: isAll, isMessage: isMessage },
                controllerAs: 'messageCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.commInstance.reloadData();
                }
            });
        };

        patDCtrl.prescription = function(imgId) {

            $mdDialog.show({
                controller: 'prescriptionController',
                templateUrl: 'view/dialog/prescription.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                prescriptionImg: patDCtrl.prescriptionData[imgId],
                controllerAs: 'preCtrl',
                bindToController: true
            });
        };

        patDCtrl.export = function() {

            $mdDialog.show({
                controller: 'exportController',
                templateUrl: 'view/dialog/export.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { exportData: patDCtrl.exportData, fileName: patDCtrl.name, dataHeader: patDCtrl.dataHeader, isData: (patDCtrl.active == 1) },
                controllerAs: 'expCtrl',
                bindToController: true
            });
        };

        patDCtrl.printChart = function() {
            //patDCtrl.active = 0;
            setTimeout(function() { window.print() }, 1500);
        };

        patDCtrl.delData = function() {
            var data = patDCtrl.infoList;
            var dailog_data = {
                "titile": "Delete Data",
                "message": "Do you want Delete  [" + data.name + "] Data?",
                "funcOnConfirm": function() {
                    patientService.deleteData(data.id).then(function(response) {
                        var result = response.data.resultMsg;
                        $rootScope.alertBox("success", result);
                        patDCtrl.getPatientDetail();
                        patDCtrl.dtInstance.reloadData();

                    }, function(faild) {
                        var result = faild.data.resultMsg;
                        $rootScope.alertBox("warning", result);
                        patDCtrl.getPatientDetail();
                        patDCtrl.dtInstance.reloadData();

                    })
                }
            }

            dialogService.confirmBox(dailog_data);


        };

        patDCtrl.unlinkDevice = function(deviceName, userId, serialNum) {
            $translate(["S_03439", "S_03440", "S_03441"], { "name": deviceName }).then(function(trans) {

                var dailog_data = {
                    "message": trans.S_03439,
                    "funcOnConfirm": function() {
                        var dataObj = {
                            "userId": userId,
                            "serialNum": serialNum
                        };
                        patientService.unlinkDevice(dataObj).then(function(response) {
                            patDCtrl.devInstance.reloadData();
                            $rootScope.alertBox("success", trans.S_03440);
                        }, function(failed) {
                            $rootScope.alertBox("danger", trans.S_03441);
                        })
                    }
                };
                dialogService.confirmBox(dailog_data);
            });

        };

        patDCtrl.editDevice = function(data) {
            $log.log(data);
            $mdDialog.show({
                controller: 'deviceEditController',
                templateUrl: 'view/dialog/device_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                deviceData: data,
                controllerAs: 'devCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.devInstance.reloadData();
                }
            });

        }

        if (patDCtrl.id == "") {
            $state.go("main.dashboard");
        } else {
            $state.current.onExit = function() {
                angular.element($window).triggerHandler('chartDestory');
            };
        }

        patDCtrl.editPatients = function() {

            $log.log("edit patient")
            $mdDialog.show({
                controller: 'patientEditController',
                templateUrl: 'view/dialog/patient_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientData: patDCtrl.infoList, branchList: branchList, pctrl: patDCtrl },
                controllerAs: 'patientEditCtrl',
                bindToController: true,
                onRemoving: function() { patDCtrl.getPatientInfo(); }
            });


        };


        patDCtrl.addNote = function() {

            $log.log("add note");
            $mdDialog.show({
                controller: 'noteAddController',
                templateUrl: 'view/dialog/note_add.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientData: patDCtrl.infoList },
                controllerAs: 'addNoteCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.noteInstance.reloadData();

                }
            });


        };

        patDCtrl.editNote = function(index) {

            $log.log("edit note");
            var editData = patDCtrl.noteData.filter(function(data) {
                return data.id == index;
            });
            $log.log(editData)
            $mdDialog.show({
                controller: 'noteEditController',
                templateUrl: 'view/dialog/note_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientData: patDCtrl.infoList, noteData: editData[0] },
                controllerAs: 'editNoteCtrl',
                bindToController: true,
                onRemoving: function() {
                    patDCtrl.noteInstance.reloadData();

                }
            });
        };

        patDCtrl.showEditor = function () {
            $log.log("edit lifecycle");
            $mdDialog.show({
                controller: 'lifecycleEditController',
                templateUrl: 'view/dialog/thresholdAndLifecycle_edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: false,
                locals: { patientData: patDCtrl.infoList },
                controllerAs: 'lifecycleEditorCtrl',
                bindToController: true,
                onRemoving: function () {
                    patDCtrl.noteInstance.reloadData();

                }
            });
        };

        patDCtrl.setThreshold = function() {
            patientService.setThreshold(patDCtrl.threshold).then(function(response) {
                $rootScope.alertBox("success", "update threshold success");
                patDCtrl.getPatientInfo();
                patDCtrl.dtInstance.reloadData();
            }, function(failded) {
                $rootScope.alertBox("danger", "update threshold fail");
            })
        };

        patDCtrl.cancelThreshold = function() {
            angular.copy(patDCtrl.origThreshold, patDCtrl.threshold);
        };


        patDCtrl.rerender = function(index) {
            if (index > 0) return;
            angular.element($window).triggerHandler('rerender');

        };

        angular.element($window).bind('resize', function() {

            patDCtrl.getPatientDetail();

        });


        patDCtrl.searchDetailData = function(){
            if(patDCtrl.searchMethod === 'select'){
                patDCtrl.setRawData(patDCtrl.placement.selected.start, patDCtrl.placement.selected.end, patDCtrl.drawChart);
            } else {
                patDCtrl.setRawData(patDCtrl.queryStartDate, patDCtrl.queryEndDate, patDCtrl.drawChart);
            }
        };

        patDCtrl.start = moment(patDCtrl.placement.selected.start).format("MM/DD/YYYY");
        patDCtrl.end = moment(patDCtrl.placement.selected.end).format("MM/DD/YYYY");

        patDCtrl.getPatientInfo();
        patDCtrl.getPatientDetail();
    });