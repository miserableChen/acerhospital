angular.module('abeingWellness.patientsDetail').controller("patientEditController",
    function($log, $compile, $cookies, $translate, $state, $rootScope, $scope, $mdDialog, $filter, patientService, pctrl, patientData, branchList, dialogService) {

        $scope.patient = {};
        $scope.patient.pickbirthday = new Date();
        $scope.isLoading = false;
        $scope.branchList = branchList;
        patientData.height = parseFloat(patientData.height);
        patientData.height = parseFloat(patientData.height);
        patientData.weight = parseFloat(patientData.weight);
        patientData.bloodType = (patientData.bloodType == "--") ? "" : patientData.bloodType;
        patientData.pickbirthday = new Date(patientData.birthday);

        var name = patientData.name.split(" ");
        patientData.firstName = name[0];
        patientData.lastName = name[1];
        patientData.patientId = patientData.id;
        $scope.patient = patientData;
        var origPatientData = {};
        angular.copy(patientData, origPatientData);
        var origBranchId = patientData.branchId;

        $scope.submit = function() {
            $scope.isLoading = true;
            $scope.patient.name = $scope.patient.firstName + " " + $scope.patient.lastName;
            $scope.patient.birthday = moment($scope.patient.pickbirthday).format('YYYY-MM-DD');
            if (origBranchId != $scope.patient.branchId) {
                var dailog_data = {
                    "message": $filter("translate")("S_03313"),
                    "funcOnConfirm": function() {
                        editPatient();
                        $state.go('main.dashboard');
                    },
                    "funcOnCancel": function() {

                        $mdDialog.show({
                            controller: 'patientEditController',
                            templateUrl: 'view/dialog/patient_edit.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: false,
                            fullscreen: false,
                            locals: { patientData: origPatientData, branchList: branchList, pctrl: pctrl },
                            controllerAs: 'patientEditCtrl',
                            bindToController: true,
                            onRemoving: function() {
                                $log.log(pctrl);
                                pctrl.getPatientInfo();
                            }
                        });
                    }
                };

                dialogService.confirmBox(dailog_data);
            } else {
                editPatient();
            }

            //$scope.patient.email=$scope.patient.username;
            /*$translate(["S_03213", "S_03216"], { Name: $scope.patient.name }).then(function (trans) {
            	patientService.update($scope.patient).then(
            		function (response) {
            			var patientId = $scope.patient.patientId;
            			var emgName = $scope.patient.emgName;
            			var emgPhone = $scope.patient.emgPhone;
            				patientService.updateEmergencyContact(patientId, emgName, emgPhone).then(function (response) {
            				$rootScope.alertBox("success", trans.S_03213);
            				$log.log(response.resultMsg);
            				$mdDialog.cancel();
            			}, function (failded) {
            				$scope.isLoading = false;
            				$rootScope.alertBox("danger", trans.S_03216);
            				$log.log("set contact fail");
            			})

            		}, function (failed) {
            			$scope.isLoading = false;
            			$rootScope.alertBox("danger", trans.S_03216);
            			$log.log("API fail")
            			//$mdDialog.cancel();
            		})
            })*/
        };

        function editPatient() {
            $log.log("editPatient");
            $translate(["S_03213", "S_03216"], { Name: $scope.patient.name }).then(function(trans) {
                $log.log($scope.patient);
                patientService.update($scope.patient).then(
                    function(response) {
                        $log.log($scope.patient);
                        var patientId = $scope.patient.patientId;
                        var emgName = $scope.patient.emgName;
                        var emgPhone = $scope.patient.emgPhone;
                        patientService.updateEmergencyContact(patientId, emgName, emgPhone).then(function(response) {
                            $rootScope.alertBox("success", trans.S_03213);
                            $log.log(response.resultMsg);
                            $mdDialog.cancel();
                        }, function(failded) {
                            $scope.isLoading = false;
                            $rootScope.alertBox("danger", trans.S_03216);
                            $log.log("set contact fail");
                        })

                    },
                    function(failed) {
                        $scope.isLoading = false;
                        $rootScope.alertBox("danger", trans.S_03216);
                        $log.log("API fail")
                            //$mdDialog.cancel();
                    });
            });

        }

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };


    });