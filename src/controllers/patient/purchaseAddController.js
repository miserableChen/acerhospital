angular.module('abeingWellness.patientsDetail').controller("purchaseAddController",
	function ($log, $compile, $cookies, $rootScope, $scope, $mdDialog,$filter, purchaseService, packageData, patientId) {
		var dateFormat = "YYYY/MM/DD";
		$scope.checkResult = true;
		$scope.errorMsg = "";
		$scope.minDate = new Date();
		$scope.purchase = {};
		$scope.isLoading=false;
		$scope.purchase.patientId = patientId;
		$scope.purchase.pickStartDate = new Date();
		$scope.purchase.startDate = moment(new Date()).format(dateFormat);
		$scope.purchase.endDate = moment(new Date()).format(dateFormat);		
		$scope.packages = packageData;

		$scope.submit = function () {
			$scope.isLoading=true;
			purchaseService.create($scope.purchase).then(
				function (response) {					
					var result = response.data.resultMsg;
					$rootScope.alertBox("success",$filter("translate")(result));
					$mdDialog.cancel();

				}, function (failed) {
					$log.log(failed);
					$scope.isLoading=false;
					$scope.checkResult = failed.data.result;
					$scope.errorMsg =$filter("translate")( failed.data.errorCode);

					return failed;
				});

		};

		$scope.calcDate = function () {
			if(typeof($scope.purchase.package)==="undefined")
				return;
			var id = $scope.purchase.package.split("/")[0];
			var monthValue = $scope.purchase.package.split("/")[1];
			$scope.purchase.startDate = moment($scope.purchase.pickStartDate).format(dateFormat);
			$scope.purchase.packageId = id;
			$scope.purchase.endDate = moment($scope.purchase.startDate).add(monthValue, 'months').format(dateFormat);

		}

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

