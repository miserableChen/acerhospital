angular.module('abeingWellness.patientsDetail').controller("purchaseEditController",
	function ($log, $compile, $cookies, $rootScope, $scope,$filter, $mdDialog, purchaseService,packageData,purchaseData) {
		$log.log("sample")
		var dateFormat="YYYY/MM/DD";
		$scope.checkResult=true;
		$scope.errorMsg="";
		$scope.isLoading=false;
		purchaseData.endDate=moment(purchaseData.endDate).format("YYYY/MM/DD")
		purchaseData.pickStartDate=new Date(moment(purchaseData.startDate).format("YYYY/MM/DD"));
		$log.log(purchaseData.pickStartDate);
		$scope.purchase = purchaseData;
		
		$scope.packages=packageData;

		$scope.submit = function () {
			$scope.isLoading=true;
			purchaseService.update($scope.purchase).then(
			function(response){
				var result = response.data.resultMsg;
				$rootScope.alertBox("success",$filter("translate")(result));
				$mdDialog.cancel();

			},function(failed){
					$log.log("edit purchase fail");
					$scope.isLoading=false;
					$scope.checkResult=failed.data.result;
					$scope.errorMsg=failed.data.errorCode;
					
				return failed;
			});			

		};

		$scope.calcDate=function(){

			var id = $scope.purchase.package.split("/")[0];
			var monthValue = $scope.purchase.package.split("/")[1];
			$scope.purchase.startDate=moment($scope.purchase.pickStartDate).format(dateFormat);
			$scope.purchase.packageId= id;
			$scope.purchase.endDate=moment($scope.purchase.startDate).add(monthValue,'months').format(dateFormat);

		}

		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};


	});

