angular.module('abeingWellness').controller('profileController',
    function($scope, $rootScope, memberService, $log, $filter, profileData, dialogService, currentUserService) {
        profileData = profileData.data.result[0];
        var name = profileData.name.split(" ");
        profileData.firstName = name[0];
        profileData.lastName = name[1];
        $scope.profile = profileData;
        $scope.showPassword = false;

        $scope.submit = function() {


            $scope.profile.name = $scope.profile.firstName + " " + $scope.profile.lastName;
            memberService.update($scope.profile).then(
                function(response) {
                    $log.log(response)
                    var result = response.data.resultMsg;
                    $rootScope.alertBox("success", $filter("translate")(result));
                    $log.log(currentUserService.currentUser);
                    currentUserService.currentUser.name = $scope.profile.firstName + " " + $scope.profile.lastName;
                    $scope.checkResult = response.data.result;
                    $scope.profile.oldpassword = null;
                    $scope.profile.password = null;
                    $scope.profile.confirmPassword = null;
                    $scope.showPassword = false;

                },
                function(failed) {
                    $scope.checkResult = failed.data.result;
                    $scope.errorMsg = $filter("translate")(failed.data.errorCode);
                    $scope.profile.oldpassword = null;
                    $scope.profile.password = null;
                    $scope.profile.confirmPassword = null;
                    return failed;
                });

        }

        $scope.cancel = function() {
            $scope.$parent.mainCtrl.hrefHome();
        }

        $scope.togglePassword = function() {
            $scope.showPassword = !($scope.showPassword);

        }

    }
)