angular.module('abeingWellness.patient.driectives')
    .directive('categoryItem', function($injector, $log) {
        return {
            template: '<div class="category">' +
                '<h4>{{noteCtrl.categoryItem.Category}}</h4>' +
                '<div ng-repeat="categoryId in noteCtrl.categoryItem.subCategory" name="subcategory"><input ng-model="noteCtrl.categorybox" type="checkbox" ng-checked="noteCtrl.subCategory[categoryId].checked" ng-click="noteCtrl.toggleCategory(categoryId)" >{{noteCtrl.subCategory[categoryId].label}}</div>' +
                '</div>',
            restrict: 'E',
            replace: true,
            controllerAs: 'noteCtrl',
            scope: {
                category: "@",
                servicename: "@"
            },
            controller: function($scope) {
                var service = $injector.get($scope.servicename)

                this.categoryItem = JSON.parse($scope.category);
                this.subCategory = service.subCategoryItems;
                $log.log(service.subCategoryItems)
                this.toggleCategory = function(id) {
                    if (this.categorybox)
                        service.addSelectItems(id);
                    else
                        service.removeSelectItem(id);
                }

            },
            link: function(scope, element, attr, ctrl) {
                // attr.$observe('catetgory', function(actual_value) {

                //     $log.log("ctrl");
                //     $log.log(actual_value);
                // });
            }
        }
    })
    .directive('contenteditable', function($log) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                $log.log("object");
                // view -> model
                elm.on('blur', function() {
                    ctrl.$setViewValue(elm.html());
                });

                scope.$on('setValue', function() {
                    $log.log("hello value");
                    ctrl.$setViewValue(elm.html());
                });

                // model -> view
                ctrl.$render = function() {
                    elm.html(ctrl.$viewValue);
                };

                // load init value from DOM
                ctrl.$setViewValue(elm.html());
            }
        };
    })