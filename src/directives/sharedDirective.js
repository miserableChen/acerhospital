angular.module('abeingWellness.shared.driectives')
    .directive('pageHeader', function() {
        return {
            templateUrl: 'view/directives/nav.html',
            restrict: 'E',
            replace: true
        }
    })
    .directive('headerNotification', function() {
        return {
            templateUrl: 'view/directives/header-notification.html',
            restrict: 'E'
        }
    })
    .directive('sidebar', ['groupService', function(groupService) {
        return {
            templateUrl: 'view/directives/sidebar.html',
            restrict: 'E',
            replace: true,
            scope: false,
            controller: function($scope) {
                var self = this;
                $scope.selectedMenu = 'home';
                $scope.collapseVar = 0;
                $scope.multiCollapseVar = 0;

                $scope.check = function(x) {

                    if (x == $scope.collapseVar)
                        $scope.collapseVar = 0;
                    else
                        $scope.collapseVar = x;
                };

                $scope.multiCheck = function(y) {

                    if (y == $scope.multiCollapseVar)
                        $scope.multiCollapseVar = 0;
                    else
                        $scope.multiCollapseVar = y;
                };
                //$log.log($scope.$parent.mainCtrl.groups);
                groupService.updateList().then(function(data) {
                    // self.groups = data;
                    $scope.$parent.mainCtrl.groups = data;
                });

            },
            link: function(scope, element, attrs, controller) {
                $(element).metisMenu();
            }
        };
    }])
    .directive("no-link", function() {
        return {
            restrict: "E",
            template: "<a href='javascript:void(0)'></a>",
            replace: true,
            transclude: true

        };
    })
    // .directive('loginDialog', function (AUTH_EVENTS, AUTH_MESSAGE) {
    //     return {
    //         restrict: 'A',
    //         template: '<div ng-if="visible">{{errMsg}}</div>',
    //         link: function (scope) {
    //             var showDialog = function (evnet, data) {
    //                 scope.errMsg = AUTH_MESSAGE[evnet.name]
    //                 scope.visible = true;
    //             };

//             scope.visible = false;
//             scope.$on(AUTH_EVENTS.loginFailed, showDialog);
//             scope.$on(AUTH_EVENTS.notAuthenticated, showDialog);
//             scope.$on(AUTH_EVENTS.sessionTimeout, showDialog)
//         }
//     };
// })
.directive("patientCard", function() {
        return {
            templateUrl: 'view/directives/patientCard.html',
            restrict: 'E',
            replace: true,
            scope: {
                typeData: '='
            },
            link: function(scope, elemt, attr) {

                scope.transDate = function(typeData) {
                    if (moment(typeData.timestamp * 1000).isValid())
                        return moment(typeData.timestamp * 1000).format("MM/DD/YYYY") + " at " + moment(typeData.timestamp * 1000).format("HH:mm");
                    else
                        return '--'
                }
            }

        };
    })
    .directive('pwCheck', function() {

        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {

                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function() {

                    scope.$apply(function() {

                        ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                    });
                });
            }
        }
    });