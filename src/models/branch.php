<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */
require_once 'branch_object.php';
require_once 'crud_controller.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class branch_Controller extends controller_object
{
    public function process_request($data)
    {
        try{           
            $crud_model = new branch_object();
            $type = isset($data->type) ? $data->type : '';
            $result = null;
            
            switch ($type) {
                case 'C':
                    $name = isset($data->name) ? $data->name : '';
                    $managerId = isset($data->managerId) ? $data->managerId : '';
                    $address = isset($data->address) ? $data->address : '';
                    $telecom = isset($data->telecom) ? $data->telecom : '';
                    
                    $request_data = array(
                    ':lastUpdated' => time(),
                    ':name' => $name,
                    ':managerId' => $managerId,
                    ':address' => $address,
                    ':telecom' => $telecom
                    );
                    $result = $crud_model->create($request_data);
                    break;
                case 'U':
                    $id = isset($data->id) ? $data->id : '';
                    $name = isset($data->name) ? $data->name : '';
                    $managerId = isset($data->managerId) ? $data->managerId : '';
                    $address = isset($data->address) ? $data->address : '';
                    $telecom = isset($data->telecom) ? $data->telecom : '';
                    
                    $request_data = array(
                    ':lastUpdated' => time(),
                    ':name' => $name,
                    ':managerId' => $managerId,
                    ':address' => $address,
                    ':telecom' => $telecom,
                    ':id' => $id
                    );
                    $result = $crud_model->update($request_data);
                    break;
                case 'RAL':
                    
                    $request_filed = array(
                    'id',
                    'name',
                    'lastUpdated',
                    'managerId',
                    'manager_name',
                    'address',
                    'telecom'
                    );
                    $result = $crud_model->retrieve_all($request_filed);
                    break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $result = $crud_model->delete($request_data);
                    break;
                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        return  $result;
    }
}

$branch = new branch_Controller();
print_r(json_encode($branch->process_request($data)));