<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';
class branch_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `branch`(`lastUpdated`, `name`,`managerId`, `address`, `telecom`) VALUES (:lastUpdated,:name,:managerId,:address,:telecom)';
    const RETRIEVE_SQL = 'select branch.* , branch.name as manager_name from branch LEFT JOIN member ON branch.managerId = member.id where branch.id=:id';
    const RETRIEVE_ALL_SQL = 'select branch.* , member.name as manager_name from branch LEFT JOIN member ON branch.managerId = member.id ';
    const UPDATE_SQL = 'UPDATE `branch` SET `lastUpdated`=:lastUpdated,`name`=:name,`managerId`=:managerId,`address`=:address,`telecom`=:telecom WHERE id=:id ';
    const DELETE_SQL = 'DELETE FROM branch WHERE id=:id ';
}
