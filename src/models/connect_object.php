<?php
require_once 'response_procces.php';

define('DBBASE', 'abeing');
define('DBUSER', 'abeing');
define('DBPASS', 'abeing4acer');
define('DBHOST', 'localhost');
//define('DBHOST', '10.36.156.132');
define('DBPORT', '3306');

$dbbase = DBBASE;
$dbuser = DBUSER;         // Your MySQL username
$dbpass = DBPASS;     // ...and password
$dbhost = DBHOST.':'.DBPORT;  // Internal IP for MYSQL Server

date_default_timezone_set("Asia/Taipei");

// connect to database
// try{
// $dblink = mysql_connect($dbhost, $dbuser, $dbpass)
//     or die(ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR));
// }
//mysql_query("SET NAMES 'UTF8'");

set_time_limit(0);

class connect_object
{
    private $dbbase = DBBASE;
    private $dbuser = DBUSER; // Your MySQL username
    private $dbpass = DBPASS; // ...and password
    private $dbhost = DBHOST;
    private $dbport = DBPORT;
    private $dsn = null;
    public function __construct($host = null, $port = null, $dbname = null, $user = null, $password = null)
    {
        if (isset($host)) {
            $this->host = $host;
        }
        if (isset($port)) {
            $this->dbport = $port;
        }
        if (isset($dbname)) {
            $this->dbbase = $dbname;
        }
        if (isset($user)) {
            $this->dbuser = $user;
        }
        if (isset($password)) {
            $this->dbpass = $password;
        }
    }
    public function get_dbname()
    {
        return $this->dbbase;
    }
    public function set_dbname($dbname)
    {
        $this->dbbase = $dbname;
    }
    public function get_dbhost()
    {
        return $this->dbhost;
    }
    public function set_dbhost($host)
    {
        $this->dbhost = $host;
    }
    public function get_dbport()
    {
        return $this->dbport;
    }
    public function set_dbport($port)
    {
        $this->dbport = $port;
    }
    public function get_dbuser()
    {
        return $this->dbuser;
    }
    public function set_dbuser($user)
    {
        $this->dbuser = $user;
    }
    public function get_dbpass()
    {
        return $this->dbpass;
    }
    public function set_dbpass($password)
    {
        $this->dbpass = $password;
    }
    public function get_dbdsn()
    {
        $this->dsn = 'mysql:host='.$this->dbhost.';dbname='.$this->dbbase.';port='.$this->dbport;

        return $this->dsn;
    }
}
