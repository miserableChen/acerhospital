<?php

class controller_object
{
    protected $role;
    
    
    public function __construct(){
        self::check_authtoken();
    }
    
    protected static function check_role(){
        if($_SESSION['Role'] > self::$role){
            $result=ResponseProccess::parserError(403,"Role denied",ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
            die(json_encode($result));
        }
    }
    
    protected static function check_authtoken(){
        session_start(3600);
        try{
            $headers = apache_request_headers();
            if(isset($headers['AuthToken']) && $headers['AuthToken']=="22bb00fdce90a9e743fcb2cc5569818b"){
                return true;
            }
            
            if(isset($headers['AuthToken']) && isset($_SESSION['AuthToken'])){
                if($_SESSION['AuthToken'] !== $headers['AuthToken']){
                    $result=ResponseProccess::parserError(403,"AuthToken Error",ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
                }
            }
            else{
                $result=ResponseProccess::parserError(403,"unset AuthToken Error",ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                die(json_encode($result));
            }
            
        } catch (PDOException $e) {
            $pdo->rollback();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        
        return true;
    }
}
?>