<?php

interface crud_data_interface
{
    public function set_create_sql($sql);
    public function get_create_sql();
    public function set_retrieve_sql($sql);
    public function get_retrieve_sql();
    public function set_retrieve_all_sql($sql);
    public function get_retrieve_all_sql();
    public function set_update_sql($sql);
    public function get_update_sql();
    public function set_delete_sql($sql);
    public function get_delete_sql();
}
//set sql please use ":xxx" ex.. insert into data (key1,key2,key3) values(:xxx,:yyy,:zzz)...
class crud_data implements crud_data_interface
{
    private $create_sql = null;
    private $retrieve_sql = null;
    private $retrieve_all_sql = null;
    private $update_sql = null;
    private $delete_sql = null;
    public function __construct($create_sql = null, $retrieve_sql = null, $retrieve_all_sql = null, $update_sql = null, $delete_sql = null)
    {
        if (isset($create_sql)) {
            $this->create_sql = $create_sql;
        }
        if (isset($retrieve_sql)) {
            $this->retrieve_sql = $retrieve_sql;
        }
        if (isset($retrieve_all_sql)) {
            $this->retrieve_all_sql = $retrieve_all_sql;
        }
        if (isset($update_sql)) {
            $this->update_sql = $update_sql;
        }
        if (isset($delete_sql)) {
            $this->delete_sql = $delete_sql;
        }
    }
    
    public function set_create_sql($sql)
    {
        $this->create_sql = $sql;
    }
    public function get_create_sql()
    {
        if ($this->create_sql == null) {
            throw new Exception('Create SQL is unset..', 1);
        }
        
        return $this->create_sql;
    }
    public function set_retrieve_sql($sql)
    {
        $this->retrieve_sql = $sql;
    }
    public function get_retrieve_sql()
    {
        if ($this->retrieve_sql == null) {
            throw new Exception('Retrieve SQL is unset..', 1);
        }
        
        return $this->retrieve_sql;
    }
    public function set_retrieve_all_sql($sql)
    {
        $this->retrieve_all_sql = $sql;
    }
    public function get_retrieve_all_sql()
    {
        if ($this->retrieve_all_sql == null) {
            throw new Exception('Retrieve All SQL is unset..', 1);
        }
        
        return $this->retrieve_all_sql;
    }
    public function set_update_sql($sql)
    {
        $this->update_sql = $sql;
    }
    public function get_update_sql()
    {
        if ($this->update_sql == null) {
            throw new Exception('Update SQL is unset..', 1);
        }
        
        return $this->update_sql;
    }
    public function set_delete_sql($sql)
    {
        $this->delete_sql = $sql;
    }
    public function get_delete_sql()
    {
        if ($this->delete_sql == null) {
            throw new Exception('Delete SQL is unset..', 1);
        }
        
        return $this->delete_sql;
    }
    public function set_type($type)
    {
        $this->type = $type;
    }
    public function get_type()
    {
        return $this->type;
    }
}

class crud_object
{
    protected $CRUD = null;
    const  CREATE_SQL = '';
    const  RETRIEVE_SQL = '';
    const  RETRIEVE_ALL_SQL = '';
    const  UPDATE_SQL = '';
    const  DELETE_SQL = '';
    public function __construct(crud_data_interface  $crud_data = null)
    {
        if ($crud_data == null) {
            //print_r(static::RETRIEVE_SQL);
            // print_r(static::RETRIEVE_ALL_SQL);
            $default_data = new crud_data(
            static::CREATE_SQL,
            static::RETRIEVE_SQL,
            static::RETRIEVE_ALL_SQL,
            static::UPDATE_SQL,
            static::DELETE_SQL
            );
            $this->CRUD = $default_data;
        } else {
            $this->CRUD = $crud_data;
        }
    }
    
    public function set_sql_data(crud_data_interface  $crud_data)
    {
        $this->CRUD = $crud_data;
    }
    
    protected static function parserResult($rs, $resultMsg)
    {
        $result = '{"result":'.$rs.',"resultMsg":"'.$resultMsg.'"}';
        $result = json_decode($result);
        
        return $result;
    }
    
    public function create($data_arr,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_CREATE_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_CREATE_FAIL;
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare($this->CRUD->get_create_sql());
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",$success_msg);
            } else {
                $result=ResponseProccess::parserError(400,'create fail',$fail_msg);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function update($data_arr,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_UPDATE_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_UPDATE_FAIL;
            
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare($this->CRUD->get_update_sql());
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",$success_msg);
            } else {
                $result=ResponseProccess::parserError(400,'UPDATE fail',$fail_msg);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();           
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    public function retrieve($data_arr, $retriveve_field=null,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_RETRIVE_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_RETRIVE_FAIL;
            
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare($this->CRUD->get_retrieve_sql());
            $sth->execute($data_arr);
            $rs = $sth->fetchAll(PDO::FETCH_CLASS);
            $tmp_arr=[];

            foreach ($rs as  $getinfo) {
                $arr_filed = array();                
                if(isset($retriveve_field)){
                    $getinfo=(array)($getinfo);
                    foreach ($retriveve_field as $filed) {
                        $arr_filed[$filed] = $getinfo[$filed];
                    }
                }
                else{                   
                     $arr_filed= $getinfo;
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,$success_msg);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function retrieve_all($retriveve_field=null,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_RETRIVE_ALL_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_RETRIVE_ALL_FAIL;
            
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare($this->CRUD->get_retrieve_all_sql());
            $sth->execute();
            $rs = $sth->fetchAll(PDO::FETCH_CLASS);
            $tmp_arr=[];

            foreach ($rs as  $getinfo) {
                $arr_filed = array();                
                if(isset($retriveve_field)){
                    $getinfo=(array)($getinfo);
                    foreach ($retriveve_field as $filed) {
                        $arr_filed[$filed] = $getinfo[$filed];
                    }
                }
                else{                   
                     $arr_filed= $getinfo;
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,$success_msg);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function retrieve_all_with_data($data_arr, $retriveve_field,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_RETRIVE_ALL_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_RETRIVE_ALL_FAIL;
            
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare($this->CRUD->get_retrieve_all_sql());
           //print_r($this->CRUD->get_retrieve_all_sql());
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            
            $result=ResponseProccess::parserResult($tmp_arr,$success_msg);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function delete($data_arr,$success_msg=null,$fail_msg=null)
    {
        try {
            $success_msg=isset($success_msg)?$success_msg:ResponseProccess::STATUS_DELETE_SUCCESS;
            $fail_msg=isset($fail_msg)?$fail_msg:ResponseProccess::STATUS_DELETE_FAIL;
            
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare($this->CRUD->get_delete_sql());
            $sth->execute($data_arr);
            $rs_count = $sth->rowCount();
            $result = $pdo->commit();
            
            if ($result && $rs_count > 0) {
                $result=ResponseProccess::parserResult("true",$success_msg);
            } else {
                $result=ResponseProccess::parserError(400,'delete fail',$fail_msg);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();           
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
}