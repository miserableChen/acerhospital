<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */
require_once 'emergency_object.php';
require_once 'crud_controller.php';

$post_data = file_get_contents('php://input');
$data = json_decode($post_data);

class emergency_Controller extends controller_object
{
    private $current_id = null;
    private $current_role = null;
    public function __construct()
    {
        parent::__construct();
        $current_id = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : '';
        $current_role = isset($_COOKIE['role']) ? $_COOKIE['role'] : '';
    }
    
    public function process_request($data)
    {
        try{
            $crud_model = new emergency_object();
            
            $type = isset($data->type)?$data->type:'';
            $result = null;
            switch ($type) {
                case 'R_EMG_DATA':
                    $branchId = isset($data->branchId) ? $data->branchId : '';
                    
                    $request_data = array(
                    ':branchId' => $branchId,
                    );
                    
                    $response_field = array(
                    'patientId','name','createTime'
                    );
                    $result = $crud_model->retrieve($request_data, $response_field);
                    break;
                case 'R_EMG_HISTORY_DATA':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    $dataType = isset($data->dataType) ? $data->dataType : '';
                    $duringDate= isset($data->duringDate) ? $data->duringDate : -99999;

                    $request_data = array(
                    ':patientId' => $patientId,
                    ':dataType' => $dataType,
                    ':duringDate' =>$duringDate
                    );
                    
                    $response_field = array(
                        'createTime','patientId','type','dataValue'
                    );
                    $result = $crud_model->retrieve_emergency_history_data($request_data, $response_field);
                    break;    
                case 'SET_EMG_READED':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    $createTime = isset($data->createTime) ? $data->createTime : '';
                    $request_data = array(
                    ':patientId' => $patientId,
                    ':createTime' => $createTime
                    );
                    
                    $result = $crud_model->update($request_data);
                    break;

                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        
        return  $result;
    }
}

$patient = new emergency_Controller();
print_r(json_encode($patient->process_request($data)));