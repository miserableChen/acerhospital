<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';

class emergency_object extends crud_object
{
    const CREATE_SQL =null;
    const RETRIEVE_SQL = 'select distinct CONCAT(patient.firstName," ",patient.lastName) name,emergency_data.createTime,emergency_data.patientId from emergency_data,patient where emergency_data.patientid=patient.id and patient.branchId=:branchId and readed=1';
    const RETRIEVE_ALL_SQL = null;    
    const UPDATE_SQL = 'UPDATE `emergency_data` SET `readed`=0 WHERE patientId=:patientId and createTime=:createTime ';
    const DELETE_SQL = 'DELETE FROM package WHERE id=:id ';
    const RETRIEVE_EMG_HISTORY_DATA='select distinct createTime,patientId,type,dataValue from emergency_data where patientId=:patientId and type = :dataType and FROM_UNIXTIME(emergency_data.`createTime`) BETWEEN DATE_ADD(NOW(), INTERVAL :duringDate day) AND NOW() order by emergency_data.`createTime`';

    public function retrieve_emergency_history_data($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIEVE_EMG_HISTORY_DATA);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }    
    
}