<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */
require_once 'link_pdo.php';
$today = date('Y-m-d H:i:s');
$patient_id = isset($_GET['patient_id']) ? $_GET['patient_id'] : '99999';
$sinceStart = isset($_GET['sinceStart']) ? $_GET['sinceStart'] : '';
$sinceEnd = isset($_GET['sinceEnd']) ? $_GET['sinceEnd'] : '';
    //query all of device from patient owner
    $sqlax = 'select * from view_patient_have_dev where patientId=:patient_id';
    $devp = '';
    $datap = '';
    $outp = '';
    $do = new database_object();
    $pdo = $do->get_database_object();
    $sth = $pdo->prepare($sqlax);
    $sth->bindParam(':patient_id', $patient_id);
    $sth->execute();
    $dev_rs = $sth->fetchAll();
    //query all of data from device
    foreach ($dev_rs as $get_dev_info) {
        if ($devp != '') {
            $devp .= ',';
        }
        $devp .= '{"source_id":'.$get_dev_info['source_id'];
        $devp .= ',"code":"'.$get_dev_info['code'].'"';
        $devp .= ',"serialNum":"'.$get_dev_info['serialNum'].'"';
        $devp .= ',"data":[';

        $source_id = $get_dev_info['source_id'];
        $sqlax = 'select * from data where patientId=:patient_id and sourceId=:source_id';
        // query specific date data
        if ($sinceStart != '' && $sinceEnd != '') {
            $sqlax .= ' and `timestamp` between :sinceStart and :sinceEnd order by timestamp';
        }

        $sth = $pdo->prepare($sqlax);
        $sth->bindParam(':patient_id', $patient_id);
        $sth->bindParam(':source_id', $source_id);
        if ($sinceStart != '' && $sinceEnd != '') {
            $sth->bindParam(':sinceStart', $sinceStart);
            $sth->bindParam(':sinceEnd', $sinceEnd);
        }
        $sth->execute();
        $rs = $sth->fetchAll();
        foreach ($rs as $getinfo) {
            if ($datap != '') {
                $datap .= ',';
            }
            $datap .= '{"patientId":"'.$getinfo ['patientId'].'"';
            $datap .= ',"deviceId":"'.$getinfo['deviceId'].'"';
            $datap .= ',"type":"'.$getinfo['type'].'"';
            $datap .= ',"dataValue":"'.$getinfo['dataValue'].'"';
            $datap .= ',"unit":"'.$getinfo['unit'].'"';
            $datap .= ',"status":"'.$getinfo['status'].'"';
            $datap .= ',"timestamp":"'.$getinfo['timestamp'].'"}';
        }
        $devp .= $datap;
        $devp .= ']}';
    }

    $outp = '{"result":['.$devp.']}';
    echo $outp;
