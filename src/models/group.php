<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

require_once 'group_object.php';
require_once 'crud_controller.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class group_Controller extends controller_object
{
    private $current_id = null;
    private $current_role = null;
    
    public function __construct()
    {
        parent::__construct();
        $current_id = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : '';
        $current_role = isset($_COOKIE['role']) ? $_COOKIE['role'] : '';
    }
    
    public function process_request($data)
    {
        try{
            $crud_model = new group_object();
            $type = isset($data->type) ? $data->type : 'RAL';
            $result = null;
            switch ($type) {
                case 'C':
                    $branchId = isset($data->branchId) ? $data->branchId : '';
                    $name = isset($data->name) ? $data->name : '';
                    
                    $request_data = array(
                        ':name' => $name,
                        ':branchId'=>$branchId
                    );

                    $result = $crud_model->create_group($request_data);
                    
                    break;
                case 'U':
                    $id = isset($data->id) ? $data->id : '';
                    $name = isset($data->name) ? $data->name : '';
                        
                    $request_data = array(
                        ':name' => $name,
                        ':id' => $id
                    );
                    
                    $result = $crud_model->update($request_data);
                
                    break;
                case 'R':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id,
                    );
                    $request_filed = array(
                        'id',
                        'name'
                    );
                    $result = $crud_model->retrieve($request_data, $request_filed);
                    break;
                case 'RAL':
                    $branchId = isset($data->branchId) ? $data->branchId : '';
                    
                    $request_data = array(
                        ':branchId' => $branchId,
                    );
                    $response_field = array(
                        'id',
                        'name',
                        'branchId'
                    );
                    $result = $crud_model->retrieve_all_with_data($request_data, $response_field);
                    break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    
                    if ($id == '') {
                        $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }
                    $request_data = array(
                        ':id' => $id
                    );
                    $result = $crud_model->delete($request_data);
                    break;
        
                case 'JOIN':
                    $groupId = isset($data->groupId) ? $data->groupId : '';
                    
                    if ($groupId == '' or isset($data->patientObj) == false) {
                        $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }
            
                    $request_data = array(
                        'groupId' => $groupId,
                        'patientObj' => $data->patientObj
                    );
                    
                    $result = $crud_model->join_patient_group($request_data);
                    
                    
                    break;
    
                case 'LEAVE':
                    $id = isset($data->id) ? $data->id : '';
                    
                    if ($id == '' or isset($data->patientObj) == false) {
                        $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }
        
                    $request_data = array(
                        'id' => $id,
                        'patientObj' => $data->patientObj
                    );
        
                    $result = $crud_model->leave_patient_group($request_data);
                
                
                    break;

                case 'R_PATIENT':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    
                    if ($patientId == '') {
                         $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }

                    $request_data = array(
                        ':patientId' => $patientId,
                    );
                    $request_filed = array(
                        'groupId',
                        'name'
                    );
                    $result = $crud_model->retrieve_patient_group($request_data, $request_filed);
                break;

                case 'U_PATIENT':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    
                    if ($patientId == '' or isset($data->groupObjId) == false) {
                         $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }

                    $request_data = array(
                        ':patientId' => $patientId,
                        'groupObjId' => $data->groupObjId
                    );
                    $result = $crud_model->update_patient_group($request_data);

                break;
                case 'CHG_PATIENT_GROUP':
                    $src_group_id = isset($data->src_group_id) ? $data->src_group_id : '';
                    $dst_group_id = isset($data->dst_group_id) ? $data->dst_group_id : '';
                    if ($src_group_id == '' or  $dst_group_id=='' or isset($data->patientObj) == false) {
                        $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                         break;
                    }
        
                    $request_data = array(
                        'dst_group_id' => $dst_group_id,
                        'src_group_id' => $src_group_id,
                        'patientObj' => $data->patientObj
                    );

                     $result = $crud_model->change_patient_group($request_data);
                break;
                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
                }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        return  $result;
    }
}

$group = new group_Controller();
//$data = (object) array('type' => 'R_PATIENT', 'patientId' => '1');

print_r(json_encode($group->process_request($data)));