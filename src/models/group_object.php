<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';

class group_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `groups`(`name`,`branchId`) VALUES (:name,:branchId)';
    const RETRIEVE_SQL = 'SELECT * FROM groups WHERE id=:id';
    const RETRIEVE_ALL_SQL = 'SELECT * FROM groups where branchId=:branchId or branchId=-1' ;
    const UPDATE_SQL = 'UPDATE `groups` SET `name`=:name WHERE `id`=:id';
    const DELETE_SQL = 'DELETE FROM groups WHERE id=:id';

    const JOIN_PATIENT_GROUP =
            'INSERT INTO `patientgroup`(`patientId`, `groupId`)'.
            ' VALUES (:patientId, :groupId)';
    const LEAVE_PATIENT_GROUP =
            'DELETE '.
            'FROM  `patientgroup` '.
            'WHERE `groupId` = :groupId AND `patientId` = :patientId ';
    const LEAVE_PATIENT_GROUP_NAME =
            'DELETE `patientgroup` '.
            'FROM  `patientgroup` '.
            'LEFT JOIN `groups` ON `groups`.`id` = `patientgroup`.`groupId` '.
            'WHERE `groups`.`id` = :groupId AND `patientgroup`.`patientId` = :patientId ';
    const RETRIEVE_PATIENT_GROUP =
            'SELECT * FROM `patientgroup` '.
            'LEFT JOIN `groups` ON `groups`.`id` = `patientgroup`.`groupId` '.
            'WHERE `patientId`=:patientId';

    public function create_group($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare($this->CRUD->get_create_sql());
            $sth->execute($data_arr);
            $InsertId = $pdo->lastInsertId();
            $sql_result = $pdo->commit();

            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",$InsertId);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_CREATE_GROUP_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function join_patient_group($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::JOIN_PATIENT_GROUP);

            foreach ($data_arr['patientObj'] as $patientId) {
                $value = array(
                    ':groupId' => $data_arr['groupId'],
                    ':patientId' => $patientId
                );
                $sth->execute($value);
            }

            $sql_result = $pdo->commit();
            $num_rows = $sth->rowCount();
            if ($sql_result && $num_rows > 0) {
                 $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_JOIN_GROUP_SUCCESS);
            } else {
               $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_JOIN_GROUP_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollback();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {            
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function leave_patient_group($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::LEAVE_PATIENT_GROUP_NAME);
            foreach ($data_arr['patientObj'] as $patientId) {
                $value = array(
                    ':groupId' => $data_arr['id'],
                    ':patientId' => $patientId
                );
                $sth->execute($value);
            }

            $sql_result = $pdo->commit();
            $num_rows = $sth->rowCount();
            if ($sql_result && $num_rows > 0) {
                 $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_LEAVE_GROUP_SUCCESS);
            } else {
               $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_LEAVE_GROUP_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function retrieve_patient_group($data_arr, $retriveve_field)
    {
        $result['result'] = [];
        $result['debug'] = [__FILE__.':'.__LINE__];
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIEVE_PATIENT_GROUP);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            foreach ($rs as $getinfo) {
                $arr_filed = array();

                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $result['result'][] = $arr_filed;
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function update_patient_group($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            foreach ($data_arr['groupObjId'] as $key => $value) {
                if ($value == false) {
                    $sth = $pdo->prepare(self::LEAVE_PATIENT_GROUP);
                } else {
                    $sth = $pdo->prepare(self::JOIN_PATIENT_GROUP);
                }

                $value = array(
                    ':patientId' => $data_arr[':patientId'],
                    ':groupId' => $key
                );
                $sth->execute($value);
            }

            $sql_result = $pdo->commit();
            $num_rows = $sth->rowCount();
            if ($sql_result) {
                 $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_JOIN_GROUP_SUCCESS);
            } else {
               $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_JOIN_GROUP_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

      public function change_patient_group($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::JOIN_PATIENT_GROUP);

            foreach ($data_arr['patientObj'] as $patientId) {
                $value = array(
                    ':groupId' => $data_arr['dst_group_id'],
                    ':patientId' => $patientId
                );
                $sth->execute($value);
            }

            $sth = $pdo->prepare(self::LEAVE_PATIENT_GROUP_NAME);

            foreach ($data_arr['patientObj'] as $patientId) {
                $value = array(
                    ':groupId' => $data_arr['src_group_id'],
                    ':patientId' => $patientId
                );
                $sth->execute($value);
            }            

            $sql_result = $pdo->commit();
            $num_rows = $sth->rowCount();

            if ($sql_result) {
                 $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_JOIN_GROUP_SUCCESS);
            } else {
               $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_JOIN_GROUP_FAIL);
            }
            
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollback();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {            
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }
}
