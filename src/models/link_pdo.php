<?php

require_once 'connect_object.php';

class database_object
{
    private $connect_obj = null;

    private $db = null;

    public function __construct($connect_obj = null)
    {
        try {
            $this->connect_obj = isset($connect_obj) ? $connect_obj : new connect_object();
            date_default_timezone_set('Asia/Taipei');
            $this->db = new PDO($this->connect_obj->get_dbdsn(),
            $this->connect_obj->get_dbuser(),
            $this->connect_obj->get_dbpass(),
            array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET UTF8',
            ));
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            throw $e;            
        }
    }

    public function disconnect()
    {
        $this->db = null;
    }

    public function connect()
    {
        $this->db = null;
        try {
            date_default_timezone_set('Asia/Taipei');
            $this->db = new PDO($this->connect_obj->get_dbdsn(),
            $this->connect_obj->get_dbuser(),
            $this->connect_obj->get_dbpass(),
            array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
            ));
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            throw $e;           
        }
    }

    public function get_database_object()
    {
        return $this->db;
    }
}
