<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */
require_once 'log_object.php';
require_once 'crud_controller.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class log_Controller extends controller_object
{
    public function process_request($data)
    {
        $crud_model = new log_object();
        $type =$data->type ;
        $result = null;
        try{
            switch ($type) {
                case 'C':
                    $message = isset($data->message) ? $data->message : '';
                    $member_id =isset($data->member_id) ? $data->member_id : '';

                    $request_data = array(               
                    ':message' => $message,
                    ':member_id'=>$member_id
                    );

                    $result = $crud_model->create($request_data);
                break;           
                case 'RAL':
                    $response_filed = array(
                    'create_time',                
                    'username',
                    'message'                   
                    );
                    $result = $crud_model->retrieve_all($response_filed);
                break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $result = $crud_model->delete($request_data);
                break;
                case 'CLEAN_LOG':
                    $result = $crud_model->clean_log();
                break;
                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
             die(json_encode($result));
        }
        return  $result;
    }
}

$log = new log_Controller();
print_r(json_encode($log->process_request($data)));
