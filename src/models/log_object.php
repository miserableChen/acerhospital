<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';
class log_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `log`(`message`,`member_id`) VALUES (:message,:member_id)';
    const RETRIEVE_ALL_SQL = 'select log.create_time,member.username,log.message from log,member where log.member_id=member.id';
    const RETRIEVE_SQL =  self::RETRIEVE_ALL_SQL.' and log.member_id=:member_id';
    const UPDATE_SQL = 'UPDATE `log` SET `message`=:message WHERE id=:id ';
    const DELETE_SQL = 'DELETE FROM log WHERE id=:id ';
    const DELETE_ALL_SQL = 'TRUNCATE log';
    
    public function clean_log(){
        $result = false;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::DELETE_ALL_SQL);
            $sth->execute();
            $sql_result = $pdo->commit();
           if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_CHK_PASSWORD_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_CHK_PASSWORD_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
}