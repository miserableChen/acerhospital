<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

require_once 'login_object.php';

$post_data = file_get_contents('php://input');

$data = json_decode($post_data);

class login_Controller 
{
    public function process_request($data)
    {
        
        try{
            $crud_model = new login_object();
            if(!isset($data->type)){
                throw new UnexpectedValueException();
            }
            $result = null;
            
            switch ($data->type) {
                case 'LOGIN':                    
                    if(!isset($data->username) || !isset($data->password)){
                        throw new UnexpectedValueException();
                    }
                    $request_data = array(
                    ':username' => $data->username,
                    ':password'=>md5($data->password)
                    );
                    session_start(3600);
                    $result = $crud_model->login($request_data);
                    break;
                case 'LOGIN_BY_TOKEN':
                    session_start(3600);
                    $user_data=$crud_model->get_userdata_by_token();
                    $result = $crud_model->login($user_data);
                    break;
                case 'LOGOUT':
                    session_start();
                    unset($_SESSION['AuthToken']);
                    unset($_SESSION['UserData']);
                    break;
                default:
                    $result=ResponseProccess::parserError(403,NULL,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        return  $result;
    }

}

$login = new login_Controller();

print_r(json_encode($login->process_request($data)));

?>