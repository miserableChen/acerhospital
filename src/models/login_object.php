<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';
class login_object extends crud_object
{
    const LOGIN_SQL = 'select member.* , branch.name as branch_name from member LEFT JOIN branch ON member.branchId = branch.id where username=:username and password=:password';
    const RETRIEVE_ALL_SQL = NULL;
    const RETRIEVE_SQL =  NULL;
    const UPDATE_SQL = NULL;
    const DELETE_SQL = NULL;
    const DELETE_ALL_SQL = NULL;
    
    public function get_userdata_by_token(){        
        $headers = apache_request_headers();
        if(isset($headers['AuthToken']) && isset($_SESSION['AuthToken'])){
            if($_SESSION['AuthToken'] === $headers['AuthToken']){
                return  $_SESSION['UserData'];
            }
            else{
                 $result=ResponseProccess::parserError(403,"AuthToken Error",ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                die(json_encode($result));
            }
        }
        else{
            $result=ResponseProccess::parserError(403,"unset AuthToken Error",ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
            die(json_encode($result));
        }
    }
    
    public function login($data_arr){
        $result = false;
        try{
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::LOGIN_SQL);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $num_rows = $sth->rowCount();
            
            if ($num_rows == 1) {
                $outp = '';                
                $session_id = md5(time());
                $_SESSION['AuthToken'] = $session_id;
                
                foreach ($rs as $getinfo) {
                    $result = array();
                    $result['user']['id']=$getinfo ['id'];
                    $result['user']['username']=$getinfo ['username'];
                    $result['user']['name']=$getinfo ['name'];
                    $result['user']['branchId']=$getinfo ['branchId'];
                    $result['user']['branchName']=$getinfo ['branch_name'];
                    $result['user']['role']=$getinfo ['role'];
                    $result['user']['title']=$getinfo ['title'];
                }
                $_SESSION['UserData']=$data_arr;
                $result['id']=$session_id;
            } else {
                $result = ResponseProccess::parserError(403,null,ResponseProccess::STATUS_LOGIN_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        
        return $result;
    }
}

?>