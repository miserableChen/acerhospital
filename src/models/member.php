<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */

require_once 'member_object.php';
require_once 'crud_controller.php';

define('ADMIN_ROLE', '0');

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class member_Controller extends controller_object
{
    private $current_id = null;
    private $current_role = null;

    public function __construct()
    {
        parent::__construct();
        $current_id = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : '';
        $current_role = isset($_COOKIE['role']) ? $_COOKIE['role'] : '';
        
    }

    public function process_request($data)
    {
        try{
            $crud_model = new member_object();
            $type = isset($data->type) ? $data->type : '';
            $result = null;
        
            switch ($type) {
                case 'C':
                    $username = isset($data->username) ? $data->username : '';
                    $name = isset($data->name) ? $data->name : '';
                    $password = isset($data->password) ? $data->password : '';
                    $branchId = isset($data->branchId) ? $data->branchId : null;
                    $role = isset($data->role) ? $data->role : '1';
                    $title = isset($data->title) ? $data->title : '';

                    $request_data = array(
                        ':username' => $data->username,
                    );
                    $result =(object) $crud_model->check_is_duplicate($request_data);
                    
                    if ($result->result=="false") {
                        break;
                    }

                    $request_data = array(
                        ':lastupdate' => time(),
                        ':username' => $username,
                        ':name' => $name,
                        ':password' => md5($password),
                        ':branchId' => $branchId,
                        ':role' => $role,
                        ':title' => $title
                    );
                    $result =(object) $crud_model->create($request_data);
                    
                    break;
                case 'U':
                    $id = isset($data->id) ? $data->id : '';
                    $username = isset($data->username) ? $data->username : '';
                    $name = isset($data->name) ? $data->name : '';
                    $branchId = isset($data->branchId) ? $data->branchId : null;
                    $role = isset($data->role) ? $data->role : '1';
                    $title = isset($data->title) ? $data->title : '';
                    if (isset($data->password)) {
                        $password = $data->password;                        
                        $oldpassword = isset($data->oldpassword) ? $data->oldpassword :"";
                        $request_data = array(
                            ':password' => md5($oldpassword),
                            ':id' => $data->id
                        );                       
                        $result =(object) $crud_model->check_password($request_data);
                        if ($result->result=="false") {
                            break;
                        }
                        else{
                            $request_data = array(
                                ':lastupdate' => time(),
                                ':password' => md5($password),
                                ':id' => $id,
                                ':username' => $username
                            );
                            $result = $crud_model->change_password($request_data);
                        }                   
                    }

                    $request_data = array(
                        ':lastupdate' => time(),
                        ':name' => $name,
                        ':branchId' => $branchId,
                        ':role' => $role,
                        ':title' => $title,
                        ':id' => $id,
                        ':username' => $username
                    );
                    $result = $crud_model->update($request_data);
                    break;
                case 'R':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id
                    );
                    $request_filed = array(
                        'id',
                        'name',
                        'username',
                        'branchId',
                        'branch_name',
                        'lastUpdated',
                        'title',
                        'role'
                    );
                    $result = $crud_model->retrieve($request_data, $request_filed);
                    break;
                case 'RAL':
                    $request_filed = array(
                        'id',
                        'name',
                        'username',
                        'branchId',
                        'branch_name',
                        'lastUpdated',
                        'title',
                        'role'
                    );
                    $result = $crud_model->retrieve_all($request_filed);
                    break;
                case 'D':
                    $username = isset($data->username) ? $data->username : '';
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id,
                        ':username' => $username
                    );
                    $result = $crud_model->delete($request_data);
                    break;
                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
                }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
             die(json_encode($result));
        }
        return  $result;
    }
}

$member = new member_Controller();
print_r(json_encode($member->process_request($data)));
