<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';

class member_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `member`(`lastUpdated`, `username`, `password`, `name`, `branchId`, `role`, `title`) VALUES (:lastupdate,:username,:password,:name,:branchId,:role,:title)';
    const RETRIEVE_SQL = 'select member.* , branch.name as branch_name from member LEFT JOIN branch ON member.branchid = branch.id where member.id=:id ';
    const RETRIEVE_ALL_SQL = "select member.* ,branch.name as branch_name from member LEFT JOIN branch ON member.branchid = branch.id where  username <> 'admin'";
    const UPDATE_SQL = 'UPDATE `member` SET `lastUpdated`=:lastupdate,`name`=:name,`branchId`=:branchId,`role`=:role,`title`=:title WHERE id=:id and username=:username';
    const DELETE_SQL = 'DELETE FROM member WHERE id=:id and username=:username';
    const CHK_PWD_SQL = 'SELECT member.* , branch.name as branch_name FROM member LEFT JOIN branch ON member.branchid = branch.id WHERE member.id=:id AND member.password=:password';
    const UPDATE_PWD_SQL = 'UPDATE `member` SET `lastUpdated`=:lastupdate,`password`=:password WHERE id=:id and username=:username';
    const CHK_DUPLICATE_SQL = 'SELECT * FROM `member` WHERE `username`=:username';
    
    public function check_password($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::CHK_PWD_SQL);
            $sth->execute($data_arr);
            $sql_result = $sth->rowCount();
            if ($sql_result > 0) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_CHK_PASSWORD_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_CHK_PASSWORD_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function check_is_duplicate($data_arr)
    {
        $result = false;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::CHK_DUPLICATE_SQL);
            $sth->execute($data_arr);
            $rs = $sth->rowCount();
            $sql_result = $sth->rowCount();
            if ($sql_result == 0) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_VALID_DUPLICATE_SUCCESS);                
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_VALID_DUPLICATE_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function change_password($data_arr)
    {
        $result = false;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_PWD_SQL);
            $sth->execute($data_arr);
            $result = $pdo->commit();
            $rs_count = $sth->rowCount();
            if ($result && $rs_count > 0) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_CHG_PASSWORD_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_CHG_PASSWORD_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
}