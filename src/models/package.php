<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */

require_once 'package_object.php';
require_once 'crud_controller.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class package_Controller extends controller_object
{
    public function process_request($data)
    {
        try{
            $crud_model = new package_object();
            $type = isset($data->type) ? $data->type : '';
            $result = null;
            switch ($type) {
                case 'C':
                    $title = isset($data->title) ? $data->title : '';
                    $monthValue = isset($data->monthValue) ? $data->monthValue : '';
                    $price = isset($data->price) ? $data->price : '';
                    $note = isset($data->note) ? $data->note : '';
                    $request_data = array(
                        ':lastUpdated' => time(),
                        ':title' => $title,
                        ':monthValue' => $monthValue,
                        ':price' => $price,
                        ':note' => $note
                    );
                    
                    $result =$crud_model->create($request_data);
                    break;
                
                case 'U':
                    $id = isset($data->id) ? $data->id : '';
                    $title = isset($data->title) ? $data->title : '';
                    $monthValue = isset($data->monthValue) ? $data->monthValue : '';
                    $price = isset($data->price) ? $data->price : '';
                    $note = isset($data->note) ? $data->note : '';
                    
                    $request_data = array(
                        ':lastUpdated' => time(),
                        ':title' => $title,
                        ':monthValue' => $monthValue,
                        ':price' => $price,
                        ':note' => $note,
                        ':id' => $id
                    );
                    
                    $result =$crud_model->update($request_data);
                    break;
                case 'RAL':
                    
                    $request_filed = array(
                        'id',
                        'title',
                        'monthValue',
                        'price',
                        'note'
                    );
                    $result = $crud_model->retrieve_all($request_filed);
                    break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id
                    );
                    $result = $crud_model->delete($request_data);
                    break;
                default:
                     $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
             die(json_encode($result));
        }
        return  $result;
    }
}

$package = new package_Controller();
print_r(json_encode($package->process_request($data)));