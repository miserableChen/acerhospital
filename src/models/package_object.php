<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';

class package_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `package`(`lastUpdated`, `title`,`monthValue`, `price`,`note`) VALUES (:lastUpdated,:title,:monthValue,:price,:note)';
    const RETRIEVE_SQL = 'select * from package where package.id=:id';
    const RETRIEVE_ALL_SQL = 'select * from package';
    const UPDATE_SQL = 'UPDATE `package` SET `lastUpdated`=:lastUpdated,`title`=:title,`monthValue`=:monthValue,`price`=:price,`note`=:note WHERE id=:id  ';
    const DELETE_SQL = 'DELETE FROM package WHERE id=:id ';
    
//    public function retrieve($data_arr, $retriveve_field)
//    {
//        $result['result'] = [];
//        try {
//            $do = new database_object();
//            $pdo = $do->get_database_object();
//            $sth = $pdo->prepare($this->CRUD->get_retrieve_sql());
//            $sth->execute($data_arr);
//            $rs = $sth->fetchAll();
//            foreach ($rs as $getinfo) {
//                $arr_filed = array();
//
//                foreach ($retriveve_field as $filed) {
//                    if ($filed == 'note') {
//                        $arr_filed[$filed] = nl2br($getinfo[$filed]);
//                    } else {
//                        $arr_filed[$filed] = $getinfo[$filed];
//                    }
//                }
//                $result['result'][] = $arr_filed;
//            }
//            $do->disconnect();
//        } catch (PDOException $e) {
//            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
//        } catch (Exception $e) {
//            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
//        }
//
//        return $result;
//    }
//
//    public function retrieve_all($retriveve_field)
//    {
//        $outp = '';
//        $result['result'] = [];
//        try {
//            $do = new database_object();
//            $pdo = $do->get_database_object();
//            $sth = $pdo->prepare($this->CRUD->get_retrieve_all_sql());
//            $sth->execute();
//            $rs = $sth->fetchAll();
//            foreach ($rs as $getinfo) {
//                $arr_filed = array();
//
//                foreach ($retriveve_field as $filed) {
//                    if ($filed == 'note') {
//                        $arr_filed[$filed] = $getinfo[$filed];
//                    } else {
//                        $arr_filed[$filed] = $getinfo[$filed];
//                    }
//                }
//                $result['result'][] = $arr_filed;
//            }
//            $do->disconnect();
//        } catch (PDOException $e) {
//            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
//        } catch (Exception $e) {
//            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
//        }
//
//        return $result;
//    }
}