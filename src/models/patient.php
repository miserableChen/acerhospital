<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */
require_once 'patient_object.php';
require_once 'crud_controller.php';

$post_data = file_get_contents('php://input');
$data = json_decode($post_data);

class patient_Controller extends controller_object
{
    private $current_id = null;
    private $current_role = null;
    public function __construct()
    {
        parent::__construct();
        $current_id = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : '';
        $current_role = isset($_COOKIE['role']) ? $_COOKIE['role'] : '';
    }
    
    public function process_request($data)
    {
        try{
            $crud_model = new patient_object();
            $type = isset($data->type)?$data->type:'';
            $result = null;
            switch ($type) {
                case 'R':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $response_field = array(
                    'id', 'userId', 'email',
                    'name', 'gender', 'birthday',
                    'telecom', 'address', 'weight',
                    'height', 'bloodType', 'flag', 'package',
                    'emgName', 'emgPhone',
                     'sys_rule','dia_rule','hr_rule','rhr_rule','co2_rule','bg_rule',
                    // 'systolic', 'diastolic', 'bp_time',
                    // 'hr', 'hr_time',
                    // 'rhr', 'rhr_time',
                    // 'co2', 'co2_time',
                    // 'tvocs', 'tvocs_time',
                    // 'pm2_5', 'pm2_5_time',
                    // 'steps', 'steps_time',
                    // 'sleep', 'sleep_time',
                    // 'bmi', 'bmi_time',
                    // 'bmr', 'bmr_time',
                    // 'bfp', 'bfp_time',
                    // 'wgt', 'wgt_time',
                    // 'bg', 'bg_time',
                    'branchId',
                    'laststatus'
                    );
                    $result = $crud_model->retrieve($request_data, $response_field);
                    break;
                case 'R_ALLRULE':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id
                    );
                    $response_field = array(
                        'patientid', 'bg_morning_l', 'bg_morning_h', 'bg_before_meal_l',
                        'bg_before_meal_h', 'bg_after_meal_l', 'bg_after_meal_h', 'bg_before_sleep_l',
                        'bg_before_sleep_h', 'bg_daybreak_l', 'bg_daybreak_h', 'sys_wakeup_l',
                        'sys_wakeup_h', 'dia_wakeup_l', 'dia_wakeup_h', 'sys_morning_l', 'sys_morning_h',
                        'dia_morning_l', 'dia_morning_h', 'sys_noon_l', 'sys_noon_h',
                        'dia_noon_l', 'dia_noon_h', 'sys_afternoon_l', 'sys_afternoon_h',
                        'dia_afternoon_l', 'dia_afternoon_h', 'sys_night_l', 'sys_night_h', 'dia_night_l',
                        'dia_night_h', 'time_bg_wakeup_b', 'time_bg_wakeup_e', 'time_before_breakfast_b',
                        'time_before_breakfast_e', 'time_after_breakfast_b', 'time_after_breakfast_e', 'time_before_lunch_b',
                        'time_before_lunch_e', 'time_after_lunch_b', 'time_after_lunch_e', 'time_before_dinner_b',
                        'time_before_dinner_e', 'time_after_dinner_b', 'time_after_dinner_e', 'time_sleep_b',
                        'time_sleep_e', 'time_bg_night_b', 'time_bg_night_e', 'time_bp_wake_b',
                        'time_bp_wake_e', 'time_morning_b', 'time_morning_e', 'time_noon_b',
                        'time_noon_e', 'time_afternoon_b', 'time_afternoon_e', 'time_bp_night_b',
                        'time_bp_night_e',
                    );
                    $result = $crud_model->retrieve_rule_data($request_data, $response_field);
                    break;
                case 'R_LASTDATA':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $response_field = array(
                        'id','patientId','timestamp','type','dataValue','unit'
                    );
                    $result = $crud_model->retrieve_last_data($request_data, $response_field);
                    break;
                case 'R_CHARTDATA':
                    $response_field = array(
                        'patientId','timestamp','dataValue','unit'
                    );
                    $result = $crud_model->retrieve_chart_data($data, $response_field);
                    break;
                case 'BG_CHARTDATA':
                    $response_field = array(
                        'patientId','timestamp','dataValue','unit','interval'
                    );
                    $result = $crud_model->retrieve_chart_data($data, $response_field);
                    break;
                case 'UPDATE_BG_RULE':
                    $id = isset($data->id) ? $data->id : '';
                    $bg_morning_l = isset($data->bg_morning_l) ? $data->bg_morning_l : '';
                    $bg_before_meal_l = isset($data->bg_before_meal_l) ? $data->bg_before_meal_l : '';
                    $bg_after_meal_l = isset($data->bg_after_meal_l) ? $data->bg_after_meal_l : '';
                    $bg_before_sleep_l = isset($data->bg_before_sleep_l) ? $data->bg_before_sleep_l : '';
                    $bg_daybreak_l = isset($data->bg_daybreak_l) ? $data->bg_daybreak_l : '';
                    $bg_morning_h = isset($data->bg_morning_h) ? $data->bg_morning_h : '';
                    $bg_before_meal_h = isset($data->bg_before_meal_h) ? $data->bg_before_meal_h : '';
                    $bg_after_meal_h = isset($data->bg_after_meal_h) ? $data->bg_after_meal_h : '';
                    $bg_before_sleep_h = isset($data->bg_before_sleep_h) ? $data->bg_before_sleep_h : '';
                    $bg_daybreak_h = isset($data->bg_daybreak_h) ? $data->bg_daybreak_h : '';
                    $time_bg_wakeup_b = isset($data->time_bg_wakeup_b) ? $data->time_bg_wakeup_b : '';
                    $time_before_breakfast_b = isset($data->time_before_breakfast_b) ? $data->time_before_breakfast_b : '';
                    $time_after_breakfast_b = isset($data->time_after_breakfast_b) ? $data->time_after_breakfast_b : '';
                    $time_before_lunch_b = isset($data->time_before_lunch_b) ? $data->time_before_lunch_b : '';
                    $time_after_lunch_b = isset($data->time_after_lunch_b) ? $data->time_after_lunch_b : '';
                    $time_before_dinner_b = isset($data->time_before_dinner_b) ? $data->time_before_dinner_b : '';
                    $time_after_dinner_b = isset($data->time_after_dinner_b) ? $data->time_after_dinner_b : '';
                    $time_sleep_b = isset($data->time_sleep_b) ? $data->time_sleep_b : '';
                    $time_bg_night_b = isset($data->time_bg_night_b) ? $data->time_bg_night_b : '';
                    $time_bg_wakeup_e = isset($data->time_bg_wakeup_e) ? $data->time_bg_wakeup_e : '';
                    $time_before_breakfast_e = isset($data->time_before_breakfast_e) ? $data->time_before_breakfast_e : '';
                    $time_after_breakfast_e = isset($data->time_after_breakfast_e) ? $data->time_after_breakfast_e : '';
                    $time_before_lunch_e = isset($data->time_before_lunch_e) ? $data->time_before_lunch_e : '';
                    $time_after_lunch_e = isset($data->time_after_lunch_e) ? $data->time_after_lunch_e : '';
                    $time_before_dinner_e = isset($data->time_before_dinner_e) ? $data->time_before_dinner_e : '';
                    $time_after_dinner_e = isset($data->time_after_dinner_e) ? $data->time_after_dinner_e : '';
                    $time_sleep_e = isset($data->time_sleep_e) ? $data->time_sleep_e : '';
                    $time_bg_night_e = isset($data->time_bg_night_e) ? $data->time_bg_night_e : '';
                    $request_data = array(
                        ':id' => $id,
                        ':bg_morning_l' => $bg_morning_l,
                        ':bg_before_meal_l' => $bg_before_meal_l,
                        ':bg_after_meal_l' => $bg_after_meal_l,
                        ':bg_before_sleep_l' => $bg_before_sleep_l,
                        ':bg_daybreak_l' => $bg_daybreak_l,
                        ':bg_morning_h' => $bg_morning_h,
                        ':bg_before_meal_h' => $bg_before_meal_h,
                        ':bg_after_meal_h' => $bg_after_meal_h,
                        ':bg_before_sleep_h' => $bg_before_sleep_h,
                        ':bg_daybreak_h' => $bg_daybreak_h,
                        ':time_bg_wakeup_b' => $time_bg_wakeup_b,
                        ':time_before_breakfast_b' => $time_before_breakfast_b,
                        ':time_after_breakfast_b' => $time_after_breakfast_b,
                        ':time_before_lunch_b' => $time_before_lunch_b,
                        ':time_after_lunch_b' => $time_after_lunch_b,
                        ':time_before_dinner_b' => $time_before_dinner_b,
                        ':time_after_dinner_b' => $time_after_dinner_b,
                        ':time_sleep_b' => $time_sleep_b,
                        ':time_bg_night_b' => $time_bg_night_b,
                        ':time_bg_wakeup_e' => $time_bg_wakeup_e,
                        ':time_before_breakfast_e' => $time_before_breakfast_e,
                        ':time_after_breakfast_e' => $time_after_breakfast_e,
                        ':time_before_lunch_e' => $time_before_lunch_e,
                        ':time_after_lunch_e' => $time_after_lunch_e,
                        ':time_before_dinner_e' => $time_before_dinner_e,
                        ':time_after_dinner_e' => $time_after_dinner_e,
                        ':time_sleep_e' => $time_sleep_e,
                        ':time_bg_night_e' => $time_bg_night_e
                    );
                    $result = $crud_model->update_bg_patient_rule($request_data);
                    break;
                case 'UPDATE_BP_RULE':
                    $id = isset($data->id) ? $data->id : '';
                    $sys_wakeup_l = isset($data->sys_wakeup_l) ? $data->sys_wakeup_l: '';
                    $sys_morning_l = isset($data->sys_morning_l) ? $data->sys_morning_l: '';
                    $sys_noon_l = isset($data->sys_noon_l) ? $data->sys_noon_l: '';
                    $sys_afternoon_l = isset($data->sys_afternoon_l) ? $data->sys_afternoon_l: '';
                    $sys_night_l = isset($data->sys_night_l) ? $data->sys_night_l: '';
                    $sys_wakeup_h = isset($data->sys_wakeup_h) ? $data->sys_wakeup_h: '';
                    $sys_morning_h = isset($data->sys_morning_h) ? $data->sys_morning_h: '';
                    $sys_noon_h = isset($data->sys_noon_h) ? $data->sys_noon_h: '';
                    $sys_afternoon_h = isset($data->sys_afternoon_h) ? $data->sys_afternoon_h: '';
                    $sys_night_h = isset($data->sys_night_h) ? $data->sys_night_h: '';
                    $dia_wakeup_l = isset($data->dia_wakeup_l) ? $data->dia_wakeup_l: '';
                    $dia_morning_l = isset($data->dia_morning_l) ? $data->dia_morning_l: '';
                    $dia_noon_l = isset($data->dia_noon_l) ? $data->dia_noon_l: '';
                    $dia_afternoon_l = isset($data->dia_afternoon_l) ? $data->dia_afternoon_l: '';
                    $dia_night_l = isset($data->dia_night_l) ? $data->dia_night_l: '';
                    $dia_wakeup_h = isset($data->dia_wakeup_h) ? $data->dia_wakeup_h: '';
                    $dia_morning_h = isset($data->dia_morning_h) ? $data->dia_morning_h: '';
                    $dia_noon_h = isset($data->dia_noon_h) ? $data->dia_noon_h: '';
                    $dia_afternoon_h = isset($data->dia_afternoon_h) ? $data->dia_afternoon_h: '';
                    $dia_night_h = isset($data->dia_night_h) ? $data->dia_night_h: '';
                    $time_bp_wake_b = isset($data->time_bp_wake_b) ? $data->time_bp_wake_b: '';
                    $time_morning_b = isset($data->time_morning_b) ? $data->time_morning_b: '';
                    $time_noon_b = isset($data->time_noon_b) ? $data->time_noon_b: '';
                    $time_afternoon_b = isset($data->time_afternoon_b) ? $data->time_afternoon_b: '';
                    $time_bp_night_b = isset($data->time_bp_night_b) ? $data->time_bp_night_b: '';
                    $time_bp_wake_e = isset($data->time_bp_wake_e) ? $data->time_bp_wake_e: '';
                    $time_morning_e = isset($data->time_morning_e) ? $data->time_morning_e: '';
                    $time_noon_e = isset($data->time_noon_e) ? $data->time_noon_e: '';
                    $time_afternoon_e = isset($data->time_afternoon_e) ? $data->time_afternoon_e: '';
                    $time_bp_night_e = isset($data->time_bp_night_e) ? $data->time_bp_night_e: '';
                    $request_data = array(
                        ':id' => $id,
                        ':sys_wakeup_l' => $sys_wakeup_l,
                        ':sys_morning_l' => $sys_morning_l,
                        ':sys_noon_l' => $sys_noon_l,
                        ':sys_afternoon_l' => $sys_afternoon_l,
                        ':sys_night_l' => $sys_night_l,
                        ':sys_wakeup_h' => $sys_wakeup_h,
                        ':sys_morning_h' => $sys_morning_h,
                        ':sys_noon_h' => $sys_noon_h,
                        ':sys_afternoon_h' => $sys_afternoon_h,
                        ':sys_night_h' => $sys_night_h,
                        ':dia_wakeup_l' => $dia_wakeup_l,
                        ':dia_morning_l' => $dia_morning_l,
                        ':dia_noon_l' => $dia_noon_l,
                        ':dia_afternoon_l' => $dia_afternoon_l,
                        ':dia_night_l' => $dia_night_l,
                        ':dia_wakeup_h' => $dia_wakeup_h,
                        ':dia_morning_h' => $dia_morning_h,
                        ':dia_noon_h' => $dia_noon_h,
                        ':dia_afternoon_h' => $dia_afternoon_h,
                        ':dia_night_h' => $dia_night_h,
                        ':time_bp_wake_b' => $time_bp_wake_b,
                        ':time_morning_b' => $time_morning_b,
                        ':time_noon_b' => $time_noon_b,
                        ':time_afternoon_b' => $time_afternoon_b,
                        ':time_bp_night_b' => $time_bp_night_b,
                        ':time_bp_wake_e' => $time_bp_wake_e,
                        ':time_morning_e' => $time_morning_e,
                        ':time_noon_e' => $time_noon_e,
                        ':time_afternoon_e' => $time_afternoon_e,
                        ':time_bp_night_e' => $time_bp_night_e
                    );
                    $result = $crud_model->update_bp_patient_rule($request_data);
                    break;
                case 'RAL':
                    $branchId = isset($data->branchId) ? $data->branchId : '';
                    
                    $request_data = array(
                    ':branchId' => $branchId,
                    );
                    $response_field = array(
                    'id', 'userId', 'email',
                    'name', 'gender', 'birthday',
                    'telecom', 'address', 'weight',
                    'height', 'bloodType', 'flag', 'package',
                    'sys_rule','dia_rule','hr_rule','rhr_rule','co2_rule','bg_rule',
                    'systolic', 'diastolic', 'bp_time',
                    'hr', 'hr_time',
                    'rhr', 'rhr_time',
                    // 'co2', 'co2_time',
                    // 'tvocs', 'tvocs_time',
                    // 'pm2_5', 'pm2_5_time',
                    // 'steps', 'steps_time',
                    'sleep', 'sleep_time',
                    'bmi', 'bmi_time',
                    // 'bmr', 'bmr_time',
                    // 'bfp', 'bfp_time',
                    'bg', 'bg_time',
                    'afib', 'afib_time',
                    'laststatus'
                    );
                    
                    $result = $crud_model->retrieve_all_with_data($request_data, $response_field);
                    
                    break;
                case 'R_GROUP':
                    $group_id = isset($data->id) ? $data->id : '';
                    $branchId = isset($data->branchId) ? $data->branchId : '';
                    
                    $request_data = array(
                    ':groupId' => $group_id,
                    ':branchId' => $branchId
                    );
                    $response_field = array(
                    'id', 'userId', 'email',
                    'name', 'gender', 'birthday',
                    'telecom', 'address', 'weight',
                    'height', 'bloodType', 'flag', 'package',
                    'sys_rule','dia_rule','hr_rule','rhr_rule','co2_rule','bg_rule',
                    'systolic', 'diastolic', 'bp_time',
                    'hr', 'hr_time',
                    'rhr', 'rhr_time',
                    // 'co2', 'co2_time',
                    // 'tvocs', 'tvocs_time',
                    // 'pm2_5', 'pm2_5_time',
                    // 'steps', 'steps_time',
                    'sleep', 'sleep_time',
                    'bmi', 'bmi_time',
                    // 'bmr', 'bmr_time',
                    // 'bfp', 'bfp_time',
                    // 'wgt', 'wgt_time',
                    'bg', 'bg_time',
                    'afib', 'afib_time',
                    'laststatus'
                    );
                    
                    $result = $crud_model->retrieve_group($request_data, $response_field);
                    
                    break;
                case 'R_DATA':
                    $id = isset($data->id) ? $data->id : '';
                    $duringDate= isset($data->duringDate) ? $data->duringDate : -99999;
                    $request_data = array(
                    ':id' => $id,
                    ':duringDate'=> $duringDate
                    );
                    $response_field = array(
                    'patientId','timestamp',
                    'systolic', 'diastolic', 'bp_unit',
                    'hr', 'hr_unit',
                    'rhr', 'rhr_unit',
                    'co2', 'co2_unit',
                    'tvocs', 'tvocs_unit',
                    'pm2_5', 'pm2_5_unit',
                    'steps', 'steps_unit',
                    'sleep', 'sleep_unit',
                    'bmi', 'bmi_unit',
                    'bmr', 'bmr_unit',
                    'bfp', 'bfp_unit',
                    'wgt', 'wgt_unit',
                    'bg', 'bg_unit'
                    );
                    $result = $crud_model->retrive_data_hisotry($request_data, $response_field);
                    break;
                case 'R_DATA_RANGE':
                    $id = isset($data->id) ? $data->id : '';
                    $start= isset($data->start) ? $data->start: -99999;
                    $end= isset($data->end) ? $data->end: -99999;
                    $request_data = array(
                        ':id' => $id,
                        ':start'=> $start,
                        ':end'=> $end
                    );

                    $response_field = array(
                        'patientId','timestamp',
                        'systolic', 'diastolic', 'bp_unit',
                        'hr', 'hr_unit',
                        'rhr', 'rhr_unit',
                        'co2', 'co2_unit',
                        'tvocs', 'tvocs_unit',
                        'pm2_5', 'pm2_5_unit',
                        'steps', 'steps_unit',
                        'sleep', 'sleep_unit',
                        'bmi', 'bmi_unit',
                        'bmr', 'bmr_unit',
                        'bfp', 'bfp_unit',
                        'wgt', 'wgt_unit',
                        'bg', 'bg_unit'
                    );
                    $result = $crud_model->retrive_data_hisotry_date_range($request_data, $response_field);
                    break;
                case 'R_DATA_COUNT':
                    $id = isset($data->id) ? $data->id : '';
                    $dataType= isset($data->dataType) ? $data->dataType : '';
                    $start= isset($data->duringDate) ? $data->duringDate : -99999;
                    $request_data = array(
                    ':id' => $id,
                    ':duringDate'=> $start
                    );
                    
                    $result = $crud_model->get_history_data_count($request_data,$dataType);
                    break;
                case 'R_COMM_DATA':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $response_field = array(
                    'sender_name',
                    'receiver_name',
                    'timestamp',
                    'content'
                    );
                    $result = $crud_model->retrive_communication_hisotry($request_data, $response_field);
                    break;
                case 'R_PRE_DATA':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':id' => $id
                    );
                    $response_field = array(
                    'id',
                    'timestamp',
                    'name',
                    'data'
                    );
                    $result = $crud_model->retrive_prescription_hisotry($request_data, $response_field);
                    break;
                case 'R_DEV_INFO':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                    ':patientId' => $id
                    );
                    $response_field = array(
                    'id',
                    'dev_name',
                    'serialNum',
                    'userId',
                    'purchase_date'
                    );
                    $result = $crud_model->retrive_device_info($request_data, $response_field);
                    break;
                case 'U_DEV_INFO':
                    $id = isset($data->id) ? $data->id : '';
                    $purchaseDate = isset($data->purchaseDate) ? $data->purchaseDate : '';
                    $request_data = array(
                    ':id' => $id,
                    ':purchaseDate' => $purchaseDate
                    );
                    $result = $crud_model->update_device_info($request_data);
                    break;
                case 'U_THRESHOLD':
                    $id = isset($data->id) ? $data->id : '';
                    $sys_rule = isset($data->sys_rule) ? $data->sys_rule : '';
                    $dia_rule = isset($data->dia_rule) ? $data->dia_rule : '';
                    $bg_rule = isset($data->bg_rule) ?  $data->bg_rule  : '';
                    $hr_rule = isset($data->hr_rule) ?  $data->hr_rule  : '';
                    $rhr_rule = isset($data->rhr_rule) ? $data->rhr_rule : '';
                    $co2_rule = isset($data->co2_rule) ? $data->co2_rule : '';
                    $request_data = array(
                    ':id' => $id,
                    ':sys_rule' => $sys_rule,
                    ':dia_rule' => $dia_rule,
                    ':bg_rule' => $bg_rule,
                    ':hr_rule' => $hr_rule,
                    ':rhr_rule' => $rhr_rule,
                    ':co2_rule' => $co2_rule
                    );
                    $result = $crud_model->update_threshold($request_data);
                    break;
                case 'C_EMG_DATA'://for emergency contact
                    $id = isset($data->id) ? $data->id : '';
                    $emgName = isset($data->emgName) ? $data->emgName : '';
                    $emgPhone = isset($data->emgPhone) ? $data->emgPhone : '';
                    $request_data = array(
                    ':id' => $id,
                    ':emgName' => $emgName,
                    ':emgPhone' => $emgPhone
                    );
                    
                    $result = $crud_model->insert_emergency_contact($request_data);
                    break;
                case 'U_EMG_DATA'://for emergency contact
                    $id = isset($data->id) ? $data->id : '';
                    $emgName = isset($data->emgName) ? $data->emgName : '';
                    $emgPhone = isset($data->emgPhone) ? $data->emgPhone : '';
                    $request_data = array(
                    ':id' => $id,
                    ':emgName' => $emgName,
                    ':emgPhone' => $emgPhone
                    );
                    
                    $result = $crud_model->update_emergency_contact($request_data);
                    break;
                case 'R_EXIPRY_LIST':
                    $response_field = array(
                    'id',
                    'name',
                    'endDate'
                    );
                    $result = $crud_model->retrive_exipry_list($response_field);
                    break;
                case 'SET_FLAG':
                    $id = isset($data->id) ? $data->id : '';
                    $flag = isset($data->flag) ? $data->flag : '';
                    
                    $request_data = array(
                    ':id' => $id,
                    ':flag' => $flag
                    );
                    $result = $crud_model->set_flag($request_data);
                    break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    
                    $request_data = array(
                    ':patientId' => $id
                    );
                    
                    $result = $crud_model->delete($request_data);
                    break;
                case 'BP_DATA':
                    $response_field = array(
                        'patientId','dataValue','hrValue','interval','timestamp'
                    );
                    $result = $crud_model->retrieve_bp_data($data, $response_field);
                    break;
                default:
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
            die(json_encode($result));
        }
        
        return  $result;
    }
}

$patient = new patient_Controller();
print_r(json_encode($patient->process_request($data)));