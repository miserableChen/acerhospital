<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';

$post_data = file_get_contents('php://input');
$data = json_decode($post_data);

class patient_object extends crud_object
{
    const DELETE_SQL = 'DELETE FROM `data` WHERE patientId=:patientId ';
    const RETRIEVE_ALL_SQL = "SELECT DISTINCT patient.*,".
    "CONCAT(patient.firstName,' ',patient.lastName) as `name`, ".
    "SPLIT_STR(bp_last_data.dataValue, ';', 1) AS systolic, SPLIT_STR(bp_last_data.dataValue, ';', 2) AS diastolic, bp_last_data.timestamp AS bp_time, ".
    "hr_last_data.dataValue AS hr, hr_last_data.timestamp AS hr_time,".
    "rhr_last_data.dataValue AS rhr, rhr_last_data.timestamp AS rhr_time,".
    "(round(sleep_last_data.dataValue/60,2)) AS sleep, sleep_last_data.timestamp AS sleep_time, ".
    "round(bmi_last_data.dataValue,2) AS bmi, bmi_last_data.timestamp AS bmi_time,".
    "afib_last_data.dataValue AS afib, afib_last_data.timestamp AS afib_time, ".
    "bg_last_data.dataValue AS bg, bg_last_data.timestamp AS bg_time, ".
    "view_status.status as laststatus, ".
    "purchasehistory.endDate as package ".
    "FROM  `patient` ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BP') bp_last_data on bp_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='HR') hr_last_data on hr_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='RHR') rhr_last_data on rhr_last_data.patientId=patient.id ".  
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='SleepTime') sleep_last_data on sleep_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BMI') bmi_last_data on bmi_last_data.patientId=patient.id ".    
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BG') bg_last_data on bg_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='AFIB') afib_last_data on afib_last_data.patientId=patient.id ".
    "LEFT JOIN view_status on view_status.patientId=patient.id ".
    "LEFT JOIN emergency_contact on emergency_contact.patientId=patient.id ".
    "LEFT JOIN purchasehistory on purchasehistory.id=patient.purchaseId ".
    "WHERE branchId=:branchId";
    
    const GET_RETRIEVE_GROUP = "SELECT * ".
    "FROM ( ".
    "SELECT DISTINCT patient.*,emergency_contact.name as 'emgName',emergency_contact.telecom as 'emgPhone' ,".
    "CONCAT(patient.firstName,' ',patient.lastName) name, ".
    "SPLIT_STR(bp_last_data.dataValue, ';', 1) AS systolic, SPLIT_STR(bp_last_data.dataValue, ';', 2) AS diastolic,bp_last_data.timestamp AS bp_time,".
    "hr_last_data.dataValue AS hr, hr_last_data.timestamp AS hr_time,".
    "rhr_last_data.dataValue AS rhr,rhr_last_data.timestamp AS rhr_time,".
    "(round(sleep_last_data.dataValue/60,2)) AS sleep,sleep_last_data.timestamp AS sleep_time,".
    "round(bmi_last_data.dataValue,2) AS bmi,bmi_last_data.timestamp AS bmi_time, ".
    "bg_last_data.dataValue AS bg, bg_last_data.timestamp AS bg_time, ".
    "afib_last_data.dataValue AS afib, afib_last_data.timestamp AS afib_time, ".
    "view_status.status as laststatus, ".
    "purchasehistory.endDate as package, ".
    "groups.name As groupName, ".
    "groups.id As groupId ".
    "FROM  `patient` ".
    "LEFT JOIN (SELECT patientId,dataValue,timestamp FROM last_data WHERE type='BP') bp_last_data ON bp_last_data.patientId=patient.id ".
    "LEFT JOIN (SELECT patientId,dataValue,timestamp FROM last_data WHERE type='HR') hr_last_data ON hr_last_data.patientId=patient.id ".
    "LEFT JOIN (SELECT patientId,dataValue,timestamp FROM last_data WHERE type='RHR') rhr_last_data ON rhr_last_data.patientId=patient.id ".
    "LEFT JOIN (SELECT patientId,dataValue,timestamp FROM last_data WHERE type='SleepTime') sleep_last_data ON sleep_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BMI') bmi_last_data on bmi_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BG') bg_last_data on bg_last_data.patientId=patient.id ".
    "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='AFIB') afib_last_data on afib_last_data.patientId=patient.id ".
    "LEFT JOIN view_status ON view_status.patientId=patient.id ".
    "LEFT JOIN emergency_contact ON emergency_contact.patientId=patient.id ".
    "LEFT JOIN purchasehistory on purchasehistory.id=patient.purchaseId ".
    "LEFT JOIN (select patientgroup.patientId, groups.name, groups.id FROM groups,patientgroup WHERE patientgroup.groupId=groups.id) groups ON groups.patientId=patient.id ".
    ") patient_group ".
    "WHERE groupId=:groupId and branchId=:branchId";
    
    // const RETRIEVE_SQL = self::RETRIEVE_ALL_SQL.' where patient.id=:id ';
    
    const RETRIEVE_SQL = "SELECT DISTINCT patient.*,emergency_contact.name as emgName,emergency_contact.telecom as emgPhone ,".
    "CONCAT(patient.firstName,' ',patient.lastName) name, ".
    "view_status.status as laststatus, ".
    "purchasehistory.endDate as package, ".
    "branch.id as branchId ".
    "FROM  `patient` ".
    "LEFT JOIN view_status on view_status.patientId=patient.id ".
    "LEFT JOIN emergency_contact on emergency_contact.patientId=patient.id ".
    "LEFT JOIN purchasehistory on purchasehistory.id=patient.purchaseId ".
    "LEFT JOIN branch on branch.id=patient.branchId ".
    "WHERE patient.id=:id ";

    
    //const RETRIEVE_LASTDATA_SQL = "SELECT patient.sys_rule,patient.dia_rule,patient.hr_rule,patient.rhr_rule,patient.co2_rule,patient.bg_rule,last_data.* FROM patient,last_data WHERE patient.id=:id and patient.id=last_data.patientId";
     const RETRIEVE_LASTDATA_SQL = "SELECT * FROM last_data WHERE patientId=:id" ;
    // const RETRIEVE_SQL_BAK = "SELECT DISTINCT patient.*,emergency_contact.name as emgName,emergency_contact.telecom as emgPhone ,".
    // "CONCAT(patient.firstName,' ',patient.lastName) name, ".
    // "SPLIT_STR(bp_last_data.dataValue, ';', 1) AS systolic, SPLIT_STR(bp_last_data.dataValue, ';', 2) AS diastolic, bp_last_data.timestamp AS bp_time, ".
    // "hr_last_data.dataValue AS hr, hr_last_data.timestamp AS hr_time,".
    // "rhr_last_data.dataValue AS rhr, rhr_last_data.timestamp AS rhr_time,".
    // "co2_last_data.dataValue AS co2, co2_last_data.timestamp AS co2_time,".
    // "tvocs_last_data.dataValue AS tvocs, tvocs_last_data.timestamp AS tvocs_time,".
    // "pm2_5_last_data.dataValue AS pm2_5, pm2_5_last_data.timestamp AS pm2_5_time, ".
    // "step_last_data.dataValue AS steps, step_last_data.timestamp AS steps_time,".
    // "(round(sleep_last_data.dataValue/60,2)) AS sleep, sleep_last_data.timestamp AS sleep_time, ".
    // "round(bmi_last_data.dataValue,2) AS bmi, bmi_last_data.timestamp AS bmi_time,".
    // "bmr_last_data.dataValue AS bmr, bmr_last_data.timestamp AS bmr_time, ".
    // "round(bfp_last_data.dataValue,2) AS bfp, bfp_last_data.timestamp AS bfp_time, ".
    // "round(wgt_last_data.dataValue,2) AS wgt, wgt_last_data.timestamp AS wgt_time,".
    // "bg_last_data.dataValue AS bg, bg_last_data.timestamp AS bg_time, ".
    // "view_status.status as laststatus, ".
    // "purchasehistory.endDate as package, ".
    // "branch.id as branchId ".
    // "FROM  `patient` ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BP') bp_last_data on bp_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='HR') hr_last_data on hr_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='RHR') rhr_last_data on rhr_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='CO2') co2_last_data on co2_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='TVOCS') tvocs_last_data on tvocs_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='PM2_5') pm2_5_last_data on pm2_5_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='Steps') step_last_data on step_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='SleepTime') sleep_last_data on sleep_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BMI') bmi_last_data on bmi_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BMR') bmr_last_data on bmr_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BFP') bfp_last_data on bfp_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='WEIGHT') wgt_last_data on wgt_last_data.patientId=patient.id ".
    // "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='BG') bg_last_data on bg_last_data.patientId=patient.id ".
    //  "LEFT JOIN (select patientId,dataValue,timestamp from last_data where type='AFIB') afib_last_data on afib_last_data.patientId=patient.id ".
    // "LEFT JOIN view_status on view_status.patientId=patient.id ".
    // "LEFT JOIN emergency_contact on emergency_contact.patientId=patient.id ".
    // "LEFT JOIN purchasehistory on purchasehistory.id=patient.purchaseId ".
    // "LEFT JOIN branch on branch.id=patient.branchId ".
    // "WHERE patient.id=:id ";

    const RETRIVE_DATA_HISTORY = "SELECT DISTINCT data_history.patientId, data_history.`timestamp` ,".
    "SPLIT_STR(data_bp.dataValue, ';', 1) AS systolic, SPLIT_STR(data_bp.dataValue, ';', 2) AS diastolic, data_bp.unit AS bp_unit, ".
    "data_hr.dataValue AS hr, data_hr.unit AS hr_unit, ".
    "data_rhr.dataValue AS rhr, data_rhr.unit AS rhr_unit, ".
    "data_co2.dataValue AS co2, data_co2.unit AS co2_unit, ".
    "data_tvocs.dataValue AS tvocs, data_tvocs.unit AS tvocs_unit, ".
    "data_pm2_5.dataValue AS pm2_5, data_pm2_5.unit AS pm2_5_unit, ".
    "data_step.dataValue AS steps, data_step.unit AS steps_unit, ".
    "(round(data_sleep.dataValue/60,2)) AS sleep, data_sleep.timestamp AS sleep_time, data_sleep.unit AS sleep_unit, ".
    "round(data_bmi.dataValue,2) AS bmi, data_bmi.unit AS bmi_unit, ".
    "data_bmr.dataValue AS bmr, data_bmr.unit AS bmr_unit, ".
    "data_bg.dataValue AS bg, data_bg.unit AS bg_unit, ".
    "round(data_bfp.dataValue,2) AS bfp, data_bfp.unit AS bfp_unit, ".
    "round(data_weight.dataValue,2) AS wgt, data_weight.unit AS wgt_unit ".
    "FROM  `data_history` ".
    "LEFT JOIN data_bp ON data_history.`timestamp` = data_bp.`timestamp` and data_bp.patientId =data_history.patientId ".
    "LEFT JOIN data_hr ON data_history.`timestamp` = data_hr.`timestamp` and data_hr.patientId =data_history.patientId ".
    "LEFT JOIN data_rhr ON data_history.`timestamp` = data_rhr.`timestamp` and data_rhr.patientId =data_history.patientId ".
    "LEFT JOIN data_co2 ON data_history.`timestamp` = data_co2.`timestamp` and data_co2.patientId =data_history.patientId ".
    "LEFT JOIN data_tvocs ON data_history.`timestamp` = data_tvocs.`timestamp` and data_tvocs.patientId =data_history.patientId ".
    "LEFT JOIN data_pm2_5 ON data_history.`timestamp` = data_pm2_5.`timestamp` and data_pm2_5.patientId =data_history.patientId ".
    "LEFT JOIN data_step ON data_history.`timestamp` = data_step.`timestamp` and data_step.patientId =data_history.patientId ".
    "LEFT JOIN data_sleep ON data_history.`timestamp` = data_sleep.`timestamp` and data_sleep.patientId =data_history.patientId ".
    "LEFT JOIN data_bmi ON data_history.`timestamp` = data_bmi.`timestamp` and data_bmi.patientId =data_history.patientId ".
    "LEFT JOIN data_bmr ON data_history.`timestamp` = data_bmr.`timestamp` and data_bmr.patientId =data_history.patientId ".
    "LEFT JOIN data_bfp ON data_history.`timestamp` = data_bfp.`timestamp` and data_bfp.patientId =data_history.patientId ".
    "LEFT JOIN data_weight ON data_history.`timestamp` = data_weight.`timestamp` and data_weight.patientId =data_history.patientId ".
    "LEFT JOIN data_bg ON data_history.`timestamp` = data_bg.`timestamp` and data_bg.patientId =data_history.patientId ".
    "WHERE data_history.`patientId` =:id and FROM_UNIXTIME(data_history.`timestamp`) BETWEEN DATE_ADD(NOW(), INTERVAL :duringDate day) AND NOW() order by data_history.`timestamp`";

    const RETRIVE_DATA_HISTORY_DATE_RANGE =
        "SELECT DISTINCT data_history.patientId, data_history.`timestamp` ,".
    "SPLIT_STR(data_bp.dataValue, ';', 1) AS systolic, SPLIT_STR(data_bp.dataValue, ';', 2) AS diastolic, data_bp.unit AS bp_unit, ".
    "data_hr.dataValue AS hr, data_hr.unit AS hr_unit, ".
    "data_rhr.dataValue AS rhr, data_rhr.unit AS rhr_unit, ".
    "data_co2.dataValue AS co2, data_co2.unit AS co2_unit, ".
    "data_tvocs.dataValue AS tvocs, data_tvocs.unit AS tvocs_unit, ".
    "data_pm2_5.dataValue AS pm2_5, data_pm2_5.unit AS pm2_5_unit, ".
    "data_step.dataValue AS steps, data_step.unit AS steps_unit, ".
    "(round(data_sleep.dataValue/60,2)) AS sleep, data_sleep.timestamp AS sleep_time, data_sleep.unit AS sleep_unit, ".
    "round(data_bmi.dataValue,2) AS bmi, data_bmi.unit AS bmi_unit, ".
    "data_bmr.dataValue AS bmr, data_bmr.unit AS bmr_unit, ".
    "data_bg.dataValue AS bg, data_bg.unit AS bg_unit, ".
    "round(data_bfp.dataValue,2) AS bfp, data_bfp.unit AS bfp_unit, ".
    "round(data_weight.dataValue,2) AS wgt, data_weight.unit AS wgt_unit ".
    "FROM  `data_history` ".
    "LEFT JOIN data_bp ON data_history.`timestamp` = data_bp.`timestamp` and data_bp.patientId =data_history.patientId ".
    "LEFT JOIN data_hr ON data_history.`timestamp` = data_hr.`timestamp` and data_hr.patientId =data_history.patientId ".
    "LEFT JOIN data_rhr ON data_history.`timestamp` = data_rhr.`timestamp` and data_rhr.patientId =data_history.patientId ".
    "LEFT JOIN data_co2 ON data_history.`timestamp` = data_co2.`timestamp` and data_co2.patientId =data_history.patientId ".
    "LEFT JOIN data_tvocs ON data_history.`timestamp` = data_tvocs.`timestamp` and data_tvocs.patientId =data_history.patientId ".
    "LEFT JOIN data_pm2_5 ON data_history.`timestamp` = data_pm2_5.`timestamp` and data_pm2_5.patientId =data_history.patientId ".
    "LEFT JOIN data_step ON data_history.`timestamp` = data_step.`timestamp` and data_step.patientId =data_history.patientId ".
    "LEFT JOIN data_sleep ON data_history.`timestamp` = data_sleep.`timestamp` and data_sleep.patientId =data_history.patientId ".
    "LEFT JOIN data_bmi ON data_history.`timestamp` = data_bmi.`timestamp` and data_bmi.patientId =data_history.patientId ".
    "LEFT JOIN data_bmr ON data_history.`timestamp` = data_bmr.`timestamp` and data_bmr.patientId =data_history.patientId ".
    "LEFT JOIN data_bfp ON data_history.`timestamp` = data_bfp.`timestamp` and data_bfp.patientId =data_history.patientId ".
    "LEFT JOIN data_weight ON data_history.`timestamp` = data_weight.`timestamp` and data_weight.patientId =data_history.patientId ".
    "LEFT JOIN data_bg ON data_history.`timestamp` = data_bg.`timestamp` and data_bg.patientId =data_history.patientId ".
    "WHERE data_history.`patientId` =:id and FROM_UNIXTIME(data_history.`timestamp`) BETWEEN FROM_UNIXTIME(:start) AND FROM_UNIXTIME(:end) order by data_history.`timestamp`";

    const RETRIVE_BP_DATA_HISTORY_COUNT= "select * from data_bp where patientId=:id and FROM_UNIXTIME(`timestamp`) BETWEEN DATE_ADD(NOW(), INTERVAL :duringDate day) AND NOW() order by `timestamp`";
    const RETRIVE_BG_DATA_HISTORY_COUNT= "select * from data_bg where patientId=:id and FROM_UNIXTIME(`timestamp`) BETWEEN DATE_ADD(NOW(), INTERVAL :duringDate day) AND NOW() order by `timestamp`";
    
    const SET_FLAG = "UPDATE `patient` SET flag=:flag WHERE id=:id ";
    
    const UPDATE_EXPIRED_DATE = "UPDATE `patient` SET purchaseId=:purchaseId WHERE id=:patientId ";
    
    const UPDATE_STATUS = "UPDATE `data` SET status=0 WHERE patientId=:id ";
       
    const SET_EMG_CONTACT = 'INSERT INTO `emergency_contact`(`patientId`, `name`, `telecom`) VALUES (:id,:emgName,:emgPhone)';
    const UPDATE_EMG_CONTACT = 'UPDATE `emergency_contact` SET name=:emgName,telecom=:emgPhone WHERE patientId=:id ';
    const GET_COMMUNICATION_HISTORY = "SELECT member.name as sender_name,CONCAT(patient.firstName,' ',patient.lastName) as receiver_name,notificationhistory.timestamp,notificationhistory.content ".
    'FROM `notificationhistory`,member,patient WHERE member.id=notificationhistory.sender and patient.id=notificationhistory.receiver and receiver=:id ';
    
    const GET_DEVICE_INFO = "SELECT source.id,source.userId,source.`serialNum`,CONCAT(source.`manufacturer`,' ',source.`serialNum`) as  dev_name,source.`purchaseDate` as purchase_date FROM source, patient WHERE patient.userId = source.userId AND patient.id =:patientId";
    const UPDATE_DEVICE_INFO = 'UPDATE `source` SET purchaseDate=:purchaseDate WHERE id=:id ';
    
    const GET_PRESCRIPTION_HISTORY = 'select * from prescription where patientId=:id';

    const GET_EXPIRY_LIST = "select patient.id,concat(patient.lastName,' ',patient.firstName) 'name',purchasehistory.endDate ".  
                            "from patient,purchasehistory ".
                            "where ".
                            "purchaseId=purchasehistory.id AND ".
                            "purchasehistory.endDate = DATE_ADD(CURRENT_DATE,INTERVAL 3 DAY) ";

    const UPDATE_THRESHOLD = "UPDATE `patient` SET sys_rule=:sys_rule,dia_rule=:dia_rule,bg_rule=:bg_rule,hr_rule=:hr_rule,rhr_rule=:rhr_rule,co2_rule=:co2_rule WHERE id=:id ";

    const UPDATE_BG_PATIENT_RULE = "UPDATE `patient_rule` SET ".
        "bg_morning_l = :bg_morning_l, ".
        "bg_before_meal_l = :bg_before_meal_l, ".
        "bg_after_meal_l = :bg_after_meal_l, ".
        "bg_before_sleep_l = :bg_before_sleep_l, ".
        "bg_daybreak_l = :bg_daybreak_l, ".
        "bg_morning_h = :bg_morning_h, ".
        "bg_before_meal_h = :bg_before_meal_h, ".
        "bg_after_meal_h = :bg_after_meal_h, ".
        "bg_before_sleep_h = :bg_before_sleep_h, ".
        "bg_daybreak_h = :bg_daybreak_h, ".
        "time_bg_wakeup_b = :time_bg_wakeup_b, ".
        "time_before_breakfast_b = :time_before_breakfast_b, ".
        "time_after_breakfast_b = :time_after_breakfast_b, ".
        "time_before_lunch_b = :time_before_lunch_b, ".
        "time_after_lunch_b = :time_after_lunch_b, ".
        "time_before_dinner_b = :time_before_dinner_b, ".
        "time_after_dinner_b = :time_after_dinner_b, ".
        "time_sleep_b = :time_sleep_b, ".
        "time_bg_night_b = :time_bg_night_b, ".
        "time_bg_wakeup_e = :time_bg_wakeup_e, ".
        "time_before_breakfast_e = :time_before_breakfast_e, ".
        "time_after_breakfast_e = :time_after_breakfast_e, ".
        "time_before_lunch_e = :time_before_lunch_e, ".
        "time_after_lunch_e = :time_after_lunch_e, ".
        "time_before_dinner_e = :time_before_dinner_e, ".
        "time_after_dinner_e = :time_after_dinner_e, ".
        "time_sleep_e = :time_sleep_e, ".
        "time_bg_night_e = :time_bg_night_e ".
        "WHERE patientid = :id";

    const UPDATE_BP_PATIENT_RULE = "UPDATE `patient_rule` SET  ".
        "sys_wakeup_l = :sys_wakeup_l, ".
        "sys_morning_l = :sys_morning_l, ".
        "sys_noon_l = :sys_noon_l, ".
        "sys_afternoon_l = :sys_afternoon_l, ".
        "sys_night_l = :sys_night_l, ".
        "sys_wakeup_h = :sys_wakeup_h, ".
        "sys_morning_h = :sys_morning_h, ".
        "sys_noon_h = :sys_noon_h, ".
        "sys_afternoon_h = :sys_afternoon_h, ".
        "sys_night_h = :sys_night_h, ".
        "dia_wakeup_l = :dia_wakeup_l, ".
        "dia_morning_l = :dia_morning_l, ".
        "dia_noon_l = :dia_noon_l, ".
        "dia_afternoon_l = :dia_afternoon_l, ".
        "dia_night_l = :dia_night_l, ".
        "dia_wakeup_h = :dia_wakeup_h, ".
        "dia_morning_h = :dia_morning_h, ".
        "dia_noon_h = :dia_noon_h, ".
        "dia_afternoon_h = :dia_afternoon_h, ".
        "dia_night_h = :dia_night_h, ".
        "time_bp_wake_b = :time_bp_wake_b, ".
        "time_morning_b = :time_morning_b, ".
        "time_noon_b = :time_noon_b, ".
        "time_afternoon_b = :time_afternoon_b, ".
        "time_bp_night_b = :time_bp_night_b, ".
        "time_bp_wake_e = :time_bp_wake_e, ".
        "time_morning_e = :time_morning_e, ".
        "time_noon_e = :time_noon_e, ".
        "time_afternoon_e = :time_afternoon_e, ".
        "time_bp_night_e = :time_bp_night_e ".
        "WHERE patientid = :id ";
    const RETRIEVE_RULE_SQL= "select * from `patient_rule` where patientId=:id";
    public function retrieve_rule_data($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIEVE_RULE_SQL);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }

            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }
                               
     public function retrieve_last_data($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIEVE_LASTDATA_SQL);            
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }

     public function retrieve_chart_data($data_arr, $retriveve_field)
    {
        try {            
            $chart_data='data_'.$data_arr->data_type;
            $id=$data_arr->id;           
            $start_date=$data_arr->start_date;
            $end_date=$data_arr->end_date;
            
            $sql =  "SELECT * FROM $chart_data WHERE patientId=:id and timestamp between UNIX_TIMESTAMP(:start) and UNIX_TIMESTAMP(:end) " ;

            $do = new database_object();
            $pdo = $do->get_database_object();           
            $sth = $pdo->prepare($sql);       
            $sth->bindParam(":id",$id);
            $sth->bindParam(":start",$start_date);
            $sth->bindParam(":end",$end_date);     
            $sth->execute();
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }

    public function retrive_data_hisotry_date_range($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIVE_DATA_HISTORY_DATE_RANGE);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();

                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }

            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_ALL_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function retrive_data_hisotry($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::RETRIVE_DATA_HISTORY);            
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_ALL_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function update_threshold($data_arr)
    {
        try {
;
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_THRESHOLD);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_THRESHOLD_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_THRESHOLD_FAIL);
            } 
                 
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }


    public function update_bg_patient_rule($data_arr)
    {
        try {
            ;
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_BG_PATIENT_RULE);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();

            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",$sth);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_THRESHOLD_FAIL);
            }

            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function update_bp_patient_rule($data_arr)
    {
        try {
            ;
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_BP_PATIENT_RULE);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();

            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_THRESHOLD_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_THRESHOLD_FAIL);
            }

            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }

    public function update_expired_date($data_arr)
    {
        try {
;
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_EXPIRED_DATE);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_EXPIRE_DATE_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_EXPIRE_DATE_FAIL);
            } 
                 
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }

    public function set_flag($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::SET_FLAG);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            if ($sql_result) {
                if ($data_arr[':flag'] == 0) {
                    $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_FLAG_UNLOCK_SUCCESS);
                } else {
                    $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_FLAG_LOCK_SUCCESS);
                }
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_SET_FLAG_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function update_status($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_STATUS);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_STATUS_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_STATUS_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function insert_emergency_contact($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::SET_EMG_CONTACT);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_STATUS_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_STATUS_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function update_emergency_contact($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_EMG_CONTACT);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
            if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_CONTACT_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_CONTACT_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
   
    public function retrive_communication_hisotry($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::GET_COMMUNICATION_HISTORY);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[]= $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_COMM_HISTORY_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function retrive_prescription_hisotry($data_arr, $retriveve_field)
    {
        
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::GET_PRESCRIPTION_HISTORY);
            
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_PRESCRIPTION_HISTORY_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function retrive_device_info($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::GET_DEVICE_INFO);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_DEVICE_INFO_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function update_device_info($data_arr)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $pdo->beginTransaction();
            $sth = $pdo->prepare(self::UPDATE_DEVICE_INFO);
            $sth->execute($data_arr);
            $sql_result = $pdo->commit();
             if ($sql_result) {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_DEVICE_SUCCESS);
            } else {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_UPDATE_DEVICE_FAIL);
            }
            $do->disconnect();
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function retrive_exipry_list($retriveve_field){
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::GET_EXPIRY_LIST);
            $sth->execute();
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_EXPIRY_LIST_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }

    public function retrieve_group($data_arr, $retriveve_field)
    {
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare(self::GET_RETRIEVE_GROUP);
            $sth->execute($data_arr);
            $rs = $sth->fetchAll();
            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }
            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_GROUP_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }


    public function get_history_data_count($data_arr,$dataType){
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sql="";
            if($dataType=='BP'){                
                 $sql=self::RETRIVE_BP_DATA_HISTORY_COUNT;
            }
            else if($dataType=='BG'){
                 $sql=self::RETRIVE_BG_DATA_HISTORY_COUNT;
            }
            else{
                throw new UnexpectedValueException("UnexpectedValueException");
            }
            $sth = $pdo->prepare($sql);          
            $sth->execute($data_arr);
            $rs = $sth->rowCount();           
            $result=ResponseProccess::parserResult($rs,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (UnexpectedValueException $e) {
            $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);                 
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;

    }

    public function retrieve_bp_data($data_arr, $retriveve_field)
    {
        try {
            $id=$data_arr->id;
            $start_date=$data_arr->start_date;
            $end_date=$data_arr->end_date;

            $sql =  "SELECT timekey.patientId, hr.dataValue AS hrValue, bp.dataValue, timekey.interval, timekey.timestamp "
                . "FROM (SELECT bp.timestamp, bp.patientId, bp.interval FROM `data_bp` AS bp UNION SELECT hr.timestamp, hr.patientId, hr.interval FROM `data_hr` AS hr) AS timekey "
                . "LEFT JOIN `data_bp` AS bp ON timekey.timestamp = bp.timestamp AND timekey.patientId = bp.patientId "
                . "LEFT JOIN `data_hr` AS hr ON timekey.timestamp = hr.timestamp AND timekey.patientId = hr.patientId "
                . "WHERE timekey.patientId=:id AND timekey.timestamp BETWEEN UNIX_TIMESTAMP(:start) and UNIX_TIMESTAMP(:end)";

            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = $pdo->prepare($sql);
            $sth->bindParam(":id",$id);
            $sth->bindParam(":start",$start_date);
            $sth->bindParam(":end",$end_date);
            $sth->execute();
            $rs = $sth->fetchAll();

            $tmp_arr=[];
            foreach ($rs as $getinfo) {
                $arr_filed = array();
                foreach ($retriveve_field as $filed) {
                    $arr_filed[$filed] = $getinfo[$filed];
                }
                $tmp_arr[] = $arr_filed;
            }

            $result=ResponseProccess::parserResult($tmp_arr,ResponseProccess::STATUS_RETRIVE_SUCCESS);
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }

        return $result;
    }
}