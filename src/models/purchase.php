<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');

/* isset ( $data->queryType ) ? $data->queryType : ""; */

require_once 'purchase_object.php';
require_once 'crud_controller.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class purchasehistory_Controller extends controller_object
{
    public function process_request($data)
    {
        try{
            $crud_model = new purchasehistory_object();

            $type = isset($data->type) ? $data->type : '';
            $result = null;
            switch ($type) {
                case 'C':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    $packageId = isset($data->packageId) ? $data->packageId : '';
                    $startDate = isset($data->startDate) ? $data->startDate : '';
                    $endDate = isset($data->endDate) ? $data->endDate : '';
                    $request_data = array(
                        ':startDate' => $startDate,
                        ':endDate' => $endDate,
                        ':patientId' => $patientId
                    );
                    $result =(object) $crud_model->validate_overlap($request_data);

                    if ($result->result=="false") {
                        break;
                    }

                    $request_data = array(
                        ':patientId' => $patientId,
                        ':packageId' => $packageId,
                        ':startDate' => $startDate,
                        ':endDate' => $endDate
                    );
                    $result = (object)$crud_model->create($request_data,ResponseProccess::STATUS_CREATE_PURCHASE_SUCCESS,ResponseProccess::STATUS_CREATE_PURCHASE_FAIL);

                    if ($result->result=="false") {
                    break;                  
                    }

                    $crud_model->check_and_update_expired();

                break;
                case 'U':
                    $id = isset($data->id) ? $data->id : '';
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    $packageId = isset($data->packageId) ? $data->packageId : '';
                    $startDate = isset($data->startDate) ? $data->startDate : '';
                    $endDate = isset($data->endDate) ? $data->endDate : '';

                    $request_data = array(
                            ':startDate' => $startDate,
                            ':endDate' => $endDate,
                            ':patientId' => $patientId,
                            ':id' => $id
                    );
                        
                    $result =(object) $crud_model->validate_overlap($request_data);
                    
                    if ($result->result=="false") {                     
                        break;
                    }

                    $request_data = array(
                        ':packageId' => $packageId,
                        ':startDate' => $startDate,
                        ':endDate' => $endDate,
                        ':id' => $id
                    );
                    $result =(object) $crud_model->update($request_data,ResponseProccess::STATUS_UPDATE_PURCHASE_SUCCESS,ResponseProccess::STATUS_UPDATE_PURCHASE_FAIL);
                    if ($result->result=="false") {
                        break;
                    }

                    $crud_model->check_and_update_expired();

                break;
                case 'R':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id
                    );
                    $request_filed = array(
                        'id',
                        'patientId',
                        'packageId',
                        'startDate',
                        'endDate'
                    );

                    $result = $crud_model->retrieve($request_data, $request_filed);
                break;
                case 'RAL':
                    $patientId = isset($data->patientId) ? $data->patientId : '';
                    $request_data = array(
                        ':patientId' => $patientId,
                    );
                    $request_filed = array(
                        'id',
                        'title',
                        'packageId',
                        'patientId',
                        'monthValue',
                        'startDate',
                        'endDate'
                    );

                    $result = $crud_model->retrieve_all_with_data($request_data, $request_filed);
                break;
                case 'D':
                    $id = isset($data->id) ? $data->id : '';
                    $request_data = array(
                        ':id' => $id
                    );
                    $result = $crud_model->delete($request_data,ResponseProccess::STATUS_DELETE_PURCHASE_SUCCESS,ResponseProccess::STATUS_DELETE_PURCHASE_FAIL);
                break;
                case 'CHK_DUE_DATE':
                    $result = $crud_model->check_and_update_expired();
                break;            
                default:                
                    $result=ResponseProccess::parserError(403,null,ResponseProccess::STATUS_PERMISSION_ACCESS_DENIED);
                    die(json_encode($result));
            }
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
            die(json_encode($result));
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
             die(json_encode($result));
        }
        return  $result;
    }
}

$purchasehistory = new purchasehistory_Controller();
print_r(json_encode($purchasehistory->process_request($data)));
