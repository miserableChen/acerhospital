<?php

require_once 'link_pdo.php';
require_once 'crud_object.php';
require_once 'patient_object.php';

$post_date = file_get_contents('php://input');
$data = json_decode($post_date);

class purchasehistory_object extends crud_object
{
    const CREATE_SQL = 'INSERT INTO `purchasehistory`(`patientId`, `packageId`,`startDate`, `endDate`) VALUES (:patientId,:packageId,:startDate,:endDate)';
    const RETRIEVE_ALL_SQL = 'SELECT pch.*,pkg.title,pkg.monthValue from purchasehistory pch LEFT JOIN package pkg ON pkg.id=pch.packageId  where pch.patientId=:patientId';
    const RETRIEVE_SQL = 'SELECT pch.*,pkg.title,pkg.monthValue from purchasehistory pch ,package pkg  where pkg.id=pch.packageId and pch.id=:id';
    const UPDATE_SQL = 'UPDATE `purchasehistory` SET `packageId`=:packageId,`startDate`=:startDate,`endDate`=:endDate WHERE `id`=:id ';
    const DELETE_SQL = 'DELETE FROM purchasehistory WHERE `id`=:id';
    
    const CHK_OVERLAP = 'SELECT * FROM `purchasehistory` WHERE ('.
    '(:startDate  BETWEEN `startDate` and `endDate`) '.
    'or (:endDate BETWEEN `startDate` and `endDate`) '.
    'or (startDate BETWEEN :startDate and :endDate) '.
    'or (endDate BETWEEN :startDate and :endDate) '.
    ') and patientId=:patientId'
    ;
    //const CHK_OVERLAP_FOR_UPDATE="SELECT * FROM `purchasehistory` WHERE ((:startDate  BETWEEN `startDate` and `endDate`) or (:endDate BETWEEN `startDate` and `endDate`)) and patientId=:patientId and id<>:id ";
    const CHK_OVERLAP_FOR_UPDATE = self::CHK_OVERLAP.' and id<>:id ';
    const CHK_PURCHASE_INCLUDE_TODAY = 'SELECT * FROM `purchasehistory` WHERE NOW() between `startDate` and `endDate`';
    
    public static function get_need_update_expired_date_patient()
    {
        $result = [];
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = null;
            $sth = $pdo->prepare(self::CHK_PURCHASE_INCLUDE_TODAY);
            $sth->execute();
            $result = $sth->fetchAll();            
        } catch (PDOException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
        
        return $result;
    }
    
    public function check_and_update_expired()
    {
        $patient_model = new patient_object();
        
        try {
            
            $patients = self::get_need_update_expired_date_patient();
            $fail_list=[];
            
            foreach ($patients as $patient) {
                $request_data = array(
                ':patientId' => $patient['patientId'],
                ':purchaseId' => $patient['id']
                );
                
                $sql_result = (object)$patient_model->update_expired_date($request_data);
               //print_r($sql_result);
                if (!$sql_result->result) {
                    $fail_list[]=$patient['patientId'];
                }
                
            }
            if(sizeof($fail_list)>0){
                $result=ResponseProccess::parserError(400,null,sprintf(ResponseProccess::STATUS_UPDATE_EXPIRE_DATE_FAIL_LIST,implode(",",$fail_list)));
            }
            else{
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_UPDATE_EXPIRE_DATE_SUCCESS);
            }
        } catch (PDOException $e) {
            $pdo->rollBack();
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
    
    public function validate_overlap($data_arr)
    {
        $result = null;
        try {
            $do = new database_object();
            $pdo = $do->get_database_object();
            $sth = null;
            if (isset($data_arr[':id'])) {
                $sth = $pdo->prepare(self::CHK_OVERLAP_FOR_UPDATE);
            } else {
                $sth = $pdo->prepare(self::CHK_OVERLAP);
            }
            $sth->execute($data_arr);
            $rs = $sth->rowCount();
           
            if ($rs > 0) {
                $result=ResponseProccess::parserError(400,null,ResponseProccess::STATUS_DATE_IS_OVERLAP);  
                     
            } else {
                $result=ResponseProccess::parserResult("true",ResponseProccess::STATUS_DATE_NOT_OVERLAP);
            }
             
            $do->disconnect();
        } catch (PDOException $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_SQL_PDO_ERROR);
        } catch (Exception $e) {
            $result=ResponseProccess::parserError(500,$e->getMessage(),ResponseProccess::STATUS_EXCEPTION_ERROR);
        }
        
        return $result;
    }
}