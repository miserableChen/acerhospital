<?php

class ResponseProccess
{
    //success
    const STATUS_CREATE_SUCCESS                         = "S_03212";
    const STATUS_RETRIVE_SUCCESS                        = "S_RETRIVE_SUCCESS";
    const STATUS_RETRIVE_ALL_SUCCESS                    = "S_RETRIVE_ALL_SUCCESS";
    const STATUS_UPDATE_SUCCESS                         = "S_03213";
    const STATUS_DELETE_SUCCESS                         = "S_03214";
    
    const STATUS_UPDATE_EXPIRE_DATE_SUCCESS             = "S_UPDATE_EXPIRE_DATE_SUCCESS";
    const STATUS_FLAG_UNLOCK_SUCCESS                    = "S_03218";
    const STATUS_FLAG_LOCK_SUCCESS                      = "S_03219";
    const STATUS_RETRIVE_EMG_PATITNET_SUCCESS           = "S_RETRIVE_EMG_PATITNET_SUCCESS";
    const STATUS_RETRIVE_COMM_HISTORY_SUCCESS           = "S_RETRIVE_COMM_HISTORY_SUCCESS";
    const STATUS_RETRIVE_GROUP_SUCCESS                  = "S_RETRIVE_GROUP_SUCCESS";
    const STATUS_RETRIVE_DEVICE_INFO_SUCCESS            = "S_RETRIVE_DEVICE_INFO_SUCCESS";
    const STATUS_RETRIVE_PRESCRIPTION_HISTORY_SUCCESS   = "S_RETRIVE_PRESCRIPTION_HISTORY_SUCCESS";
    const STATUS_RETRIVE_EXPIRY_LIST_SUCCESS            = "S_RETRIVE_EXPIRY_LIST_SUCCESS";
    const STATUS_UPDATE_STATUS_SUCCESS                  = "S_UPDATE_STATUS_SUCCESS";
    const STATUS_UPDATE_CONTACT_SUCCESS                 = "S_UPDATE_CONTACT_SUCCESS";
    const STATUS_UPDATE_DEVICE_SUCCESS                  = "S_03235";
    const STATUS_CHK_PASSWORD_SUCCESS                   = "S_CHK_PASSWORD_SUCCESS";
    const STATUS_VALID_DUPLICATE_SUCCESS                = "S_VALID_DUPLICATE_SUCCESS";
    const STATUS_CHG_PASSWORD_SUCCESS                   = "S_CHG_PASSWORD_SUCCESS";
    const STATUS_CLEAN_LOG_SUCCESS                      = "S_CLEAN_LOG_SUCCESS";
    const STATUS_JOIN_GROUP_SUCCESS                     = "S_03229";
    const STATUS_LEAVE_GROUP_SUCCESS                    = "S_03233";
    const STATUS_CREATE_PURCHASE_SUCCESS                = "S_03237";
    const STATUS_DELETE_PURCHASE_SUCCESS                = "S_03239";
    const STATUS_UPDATE_PURCHASE_SUCCESS                = "S_03241";
    const STATUS_UPDATE_THRESHOLD_SUCCESS               = "STATUS_UPDATE_THRESHOLD_SUCCESS";

    //fail
    const STATUS_CREATE_FAIL                    = "S_03215";
    const STATUS_RETRIVE_FAIL                   = "S_RETRIVE_FAIL";
    const STATUS_RETRIVE_ALL_FAIL               = "S_RETRIVE_ALL_FAIL";
    const STATUS_RETRIVE_EXPIRY_LIST_FAIL       = "S_RETRIVE_EXPIRY_LIST_FAIL";
    const STATUS_UPDATE_FAIL                    = "S_03216";
    const STATUS_DELETE_FAIL                    = "S_03217";
    const STATUS_UPDATE_DEVICE_FAIL             = "S_03236";
    const STATUS_UPDATE_THRESHOLD_FAIL           = "STATUS_UPDATE_THRESHOLD_FAIL";
    const STATUS_CHK_PASSWORD_FAIL              = "S_03223";
    const STATUS_VALID_DUPLICATE_FAIL           = "S_03225";
    const STATUS_CHG_PASSWORD_FAIL              = "S_03224";
    const STATUS_CLEAN_LOG_FAIL                 = "S_CLEAN_LOG_FAIL";
    
    const STATUS_UPDATE_EXPIRE_DATE_FAIL        = "S_UPDATE_EXPIRE_DATE_FAIL";
    const STATUS_UPDATE_EXPIRE_DATE_FAIL_LIST   = "S_UPDATE_EXPIRE_DATE_FAIL_LIST :%S ";
    const STATUS_SET_FLAG_FAIL                  = "S_SET_FLAG_FAIL";
    const STATUS_UPDATE_STATUS_FAIL             = "S_UPDATE_STATUS_FAIL";
    const STATUS_UPDATE_CONTACT_FAIL            = "S_03221";
    const STATUS_CREATE_GROUP_FAIL              = "S_CREATE_GROUP_FAIL";
    const STATUS_JOIN_GROUP_FAIL                = "S_JOIN_GROUP_FAIL";
    const STATUS_LEAVE_GROUP_FAIL               = "S_LEAVE_GROUP_FAIL";
    const STATUS_CREATE_PURCHASE_FAIL           = "S_03238";
    const STATUS_DELETE_PURCHASE_FAIL           = "S_03240";
    const STATUS_UPDATE_PURCHASE_FAIL           = "S_03242";
    const STATUS_LOGIN_FAIL                     = "S_03109";
    //error
    const STATUS_EXCEPTION_ERROR                = "S_03244";
    const STATUS_SQL_PDO_ERROR                  = "S_03245";
    const STATUS_PERMISSION_ACCESS_DENIED       = "S_03246";
    
    //other
    const STATUS_DATE_IS_OVERLAP                  = "S_03243";
    const STATUS_DATE_NOT_OVERLAP                 = "S_DATE_NOT_OVERLAP";
    
    public static function parserResult($rs, $responseCode)
    {
        $result=[];
        header('HTTP/1.1 200');
        if(gettype($rs)== "array" ){
            $result['result'] = [];
            $result['result']=$rs;
            $result['resultMsg']=$responseCode;
        }
        else{
            $result['result']=$rs;
            $result['resultMsg']=$responseCode;
        }
        
        
        return $result;
    }
    
    public static function parserError($responseCode,$responseHeaderMsg=null, $errorCode)
    {
        $result=[];
        if(!isset($responseHeaderMsg)){
            header('HTTP/1.1 '. $responseCode );
        }
        else{            
            header('HTTP/1.1 '. $responseCode.' '.$responseHeaderMsg);
        }
        $result['result']="false";
        $result['errorCode']=$errorCode;        
        return $result;
    }
   
    public static function ErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno)) {
            // This error code is not included in error_reporting
            return;
        }
        
        switch ($errno) {
            case E_USER_ERROR:
                throw new Exception('Error '.$errno.' '.$errstr.' '.$errfile.':'.$errline);
                break;
            case E_USER_WARNING:
                // echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
                break;
            case E_USER_NOTICE:
                // echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
                break;
            default:
                throw new Exception('Unknow Error '.$errno.' '.$errstr.' '.$errfile.':'.$errline);
                break;
    }
}

}
set_error_handler("ResponseProccess::ErrorHandler");