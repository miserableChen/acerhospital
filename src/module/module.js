angular.module("abeingWellness", ['ui.router', 'ui.bootstrap', 'ngCookies', 'abeingWellness.login', 'datatables',
        'abeingWellness.patients', 'abeingWellness.patientsDetail', 'abeingWellness.admin', 'abeingWellness.shared.driectives', 'pascalprecht.translate', 'ngSanitize'
    ])
    .constant(
        'fhirConfig', {
            "shopName": "宏碁藥局",
            "apiUrl": "http://" + location.host + ":8080",
            "langPath": "assets/resources/lang/",
            // "language":"zh-tw"
            "language": "en"
        })
    .constant(
        'AUTH_EVENTS', {
            loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated',
            notAuthorized: 'auth-not-authorized'
        })
    .constant(
        'AUTH_MESSAGE', {
            'auth-login-success': 'logint',
            'auth-login-failed': "Invalid account or the account doesn't exist",
            'auth-logout-success': 'logout ',
            'auth-session-timeout': 'session timeout',
            'auth-not-authenticated': 'not -authenticated',
            'auth-not-authorized': 'not authorized'
        })

.constant(
        'USER_ROLES', {
            all: "*",
            admin: '0',
            caregiver: '1',
            patient: '2'
        })
    .constant(
        'DATA_TYPE', {
            bp: "BP",
            hr: "HR",
            rhr: "RHR",
            co2: 'CO2',
            tvocs: "TVOCS",
            pm2_5: "PM2_5",
            steps: "STEPS",
            sleep: "SLEEPTIME",
            bmi: "BMI",
            bmr: "BMR",
            bfp: "BodyFat",
            wgt: "Weight",
            afib: "AFIB",
            BP: "bp",
            HR: "hr",
            RHR: "rhr",
            BG: "bg",
            CO2: 'co2',
            TVOCS: "tvocs",
            PM2_5: "pm2_5",
            STEPS: "step",
            SLEEPTIME: "sleep",
            BMI: "bmi",
            BMR: "bmr",
            BFP: "bfp",
            WEIGHT: "weight",
            AFIB: "AFIB"

        })
    .constant(
        'DATA_UNIT', {
            bp: "mmHg",
            hr: "Beats / Min",
            rhr: "Beats / Min",
            co2: 'PPM',
            tvocs: "PPB",
            pm2_5: "μg/m^3",
            steps: "steps",
            sleep: "Hours",
            bmi: "kg/㎡",
            bmr: "cal/day",
            bfp: "%",
            wgt: "kg",
            bg: "mg/dl"
        })
    // .constant(
    // 'ADMIN_MODE_CONFIG', {
    //     shop:"Acer",
    //     branch:"Admin"
    // })
    .run(function($rootScope, Session, AUTH_EVENTS, AuthService, $state, $timeout, $cookies, $log, $mdDialog, fhirConfig, DTDefaultOptions, $translate) {
        $rootScope.alerts = [];
        $rootScope.timer = "";

        // if (typeof ($cookies.get("lang")) === "undefined") {            
        //     // var lang = window.navigator.userLanguage || window.navigator.language;
        //     // var relang = lang.toLowerCase();
        //     // $cookies.put("lang", relang);
        //     $cookies.put("lang","zh-tw");

        // }
        $cookies.put("lang", fhirConfig.language);
        $rootScope.language = $cookies.get("lang");
        $translate.use($cookies.get("lang"));
        moment.locale($cookies.get("lang"));
        // $log.log($rootScope.language);
        //$log.log(fhirConfig.langPath + $rootScope.language + '.json')
        //DTDefaultOptions.setLanguageSource(fhirConfig.langPath + $rootScope.language + '.json');

        DTDefaultOptions.setLoadingTemplate('<img src="assets/resources/images/spinner.gif">');
        $rootScope.$on('$stateChangeStart', function(event, next) {
            if (typeof(Session.userRole) === 'undefined') {
                Session.create(
                    $cookies.get("authToken"),
                    $cookies.get("userId"),
                    $cookies.get("userRole")
                );
            }

            if (next.url != "/login") {
                var authorizedRoles = next.data.authorizedRoles;
                if (!AuthService.isAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    if (AuthService.isAuthenticated()) {
                        // user is not allowed
                        // $log.log("user is not allowed");
                        Session.destroy();
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                        $state.go("login");
                    } else {
                        // user is not logged in
                        // $log.log("user is not logged in");
                        Session.destroy();
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                        $state.go("login");
                    }
                }
            }

            $mdDialog.cancel();
        });


        $rootScope.AlertMouseover = function() {
            $timeout.cancel($rootScope.timer);
            $rootScope.alertShow = true;
        };

        $rootScope.AlertMouseout = function() {
            $rootScope.timer = $timeout(function() {
                $rootScope.alertShow = false;
                $rootScope.alerts = [];

            }, 3000);
        };

        $rootScope.alertBox = function(alert_type, alert_message) {
            $rootScope.alertShow = true;
            $rootScope.alerts.push({ type: alert_type, msg: alert_message });
            $rootScope.timer = $timeout(function() {
                $rootScope.alertShow = false;
                $rootScope.alerts = [];
                $timeout.cancel($rootScope.timer);
            }, 7000);
        }

        $rootScope.closeAlert = function(index) {
            $rootScope.alerts.splice(index, 1);
        };


    });



angular.module('abeingWellness.login', []);
angular.module('abeingWellness.patients', ['datatables', 'datatables.bootstrap', 'ngMaterial', 'ngMessages', 'ngSanitize', 'ngCsv'])
angular.module('abeingWellness.patientsDetail', ['datatables', 'datatables.bootstrap', 'ng-echarts', 'ui.bootstrap', 'ngMaterial', 'ngMessages', 'ngSanitize', 'abeingWellness.patient.driectives', 'anguFixedHeaderTable', 'nvd3'])
angular.module('abeingWellness.admin', ['datatables', 'datatables.bootstrap', 'ngMaterial']);
angular.module('abeingWellness.patient.driectives', []);
angular.module('abeingWellness.shared.driectives', []);

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});