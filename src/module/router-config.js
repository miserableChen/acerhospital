angular.module("abeingWellness").config(function($urlRouterProvider, $stateProvider, $logProvider, $provide, USER_ROLES, fhirConfig, $translateProvider) {


    switchLog(false);
    $urlRouterProvider.otherwise('/login');
    $stateProvider.state("/", {
            url: "/"
        })
        .state("login", {
            // ======================login state======================		
            url: "/login",
            templateUrl: "view/login.html",
            controller: 'LoginController',
            controllerAs: 'loginCtrl'
        })
        .state("forgetpw", {
            // ======================login state======================		
            url: "/forgetPassword",
            templateUrl: "view/forgotpw.html",
            data: {
                authorizedRoles: [USER_ROLES.all]
            }
        })

    .state("main", {
            // ======================dashboard state=======================
            url: "/main",
            abstract: true,
            templateUrl: "view/main.html",
            controller: 'MainController',
            controllerAs: 'mainCtrl',
            data: {
                authorizedRoles: [USER_ROLES.caregiver, USER_ROLES.admin, USER_ROLES.patient]
            }
        })
        .state("main.profile", {
            // ======================dashboard state=======================
            url: "/profile",
            templateUrl: "view/profile.html",
            controller: 'profileController',
            data: {
                authorizedRoles: [USER_ROLES.caregiver, USER_ROLES.admin]
            },
            resolve: {
                profileData: function($q, $cookies, memberService) {
                    var currentId = $cookies.get("userId");
                    return memberService.getMemberData(currentId);
                }

            }
        })
        .state("main.dashboard", {
            // ======================patient state======================
            url: "/dashboard/",
            templateUrl: "view/dashboard.html",
            controller: 'dashboardController',
            controllerAs: 'patCtrl',
            data: {
                authorizedRoles: [USER_ROLES.caregiver]
            },
            resolve: {
                groupData: function($q, groupService) {
                    var d = $q.defer();
                    groupService.updateList().then(function(data) {
                        d.resolve(data);
                    }, function() {
                        console.log("groupData error");
                        d.reject();

                    });
                    return d.promise;
                }
            }


        })
        .state("main.groups", {
            // ======================patient state======================
            url: "/groups/:groupId/",
            templateUrl: "view/dashboard.html",
            controller: 'dashboardController',
            controllerAs: 'patCtrl',
            data: {
                authorizedRoles: [USER_ROLES.caregiver]
            },
            resolve: {
                groupData: function($q, groupService) {
                    var d = $q.defer();
                    groupService.updateList().then(function(data) {
                        d.resolve(data);
                    }, function() {
                        console.log("groupData error");
                        d.reject();

                    });
                    return d.promise;
                }
            }
        })
        .state("main.patient", {
            // ======================patientDetail	 state======================				
            url: "/dashboard/patient/:id",
            templateUrl: "view/patientDetail.html",
            controller: 'patientDetailController',
            controllerAs: "patDCtrl",
            onExit: function() {},
            data: {
                authorizedRoles: [USER_ROLES.caregiver, USER_ROLES.patient]
            },
            // views:{
            // 	"tab1":{templateUrl:"view/overview.html"}
            // },
            // 一定要是 object 形式, 可有複數
            resolve: {
                // Example showing returning of custom made promise
                patientInfo: function($q, $stateParams, patientService, purchaseService, $rootScope, AUTH_EVENTS) {
                    var d = $q.defer();
                    if (typeof($stateParams.id) === 'undefined' || $stateParams.id === "") {
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                        d.reject();
                    }
                    purchaseService.refresh();
                    patientService.getPatientData('R', $stateParams.id).then(function(data) {
                        d.resolve(patientService.setPatientInfo(data[0]));
                    }, function() {
                        console.log("get patient error");
                        d.reject();

                    });
                    return d.promise;

                },
                packageList: function($q, packageService) {
                    var d = $q.defer();
                    packageService.list().then(
                        function(data) {
                            d.resolve(data.result);
                        },
                        function(fail) {
                            console.log("get purchase list error");
                            d.reject();
                        })
                    return d.promise;
                },
                branchData: function(branchService) {
                    return branchService.list();
                }

            }


        })
        .state("main.menu", {
            // ======================patient state======================
            url: "/admin/menu",
            templateUrl: "view/admin-menu.html",
            data: {
                authorizedRoles: [USER_ROLES.admin]
            }
        })
        .state("main.member", {
            // ======================patient state======================
            url: "/admin/member",
            templateUrl: "view/member.html",
            controller: 'memberController',
            controllerAs: 'memCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin]
            },
            resolve: {
                branchData: function($q, $log, dataService, branchService) {
                    return branchService.list();
                }
            }
        })
        .state("main.branch", {
            // ======================patient state======================
            url: "/admin/branch",
            templateUrl: "view/branch.html",
            controller: 'branchController',
            controllerAs: 'branchCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin]
            },
            resolve: {
                memberData: function($q, $log, dataService, memberService) {
                    return memberService.list();
                }
            }
        })
        .state("main.service", {
            // ======================patient state======================
            url: "/admin/service",
            templateUrl: "view/package.html",
            controller: 'packageController',
            controllerAs: 'pkgCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin]
            }
        })
        .state("main.log", {
            // ======================patient state======================
            url: "/admin/log",
            templateUrl: "view/log.html",
            controller: 'logController',
            controllerAs: 'logCtrl',
            data: {
                authorizedRoles: [USER_ROLES.admin]
            }
        });
    //=========================================================================================================	
    function switchLog(enabled) {
        $logProvider.debugEnabled(enabled);
        $provide.decorator('$log', function($delegate) {
            //Original methods
            var origInfo = $delegate.info;
            var origLog = $delegate.log;

            //Override the default behavior
            $delegate.info = function() {
                if ($logProvider.debugEnabled())
                    origInfo.apply(null, arguments)
            };

            //Override the default behavior    
            $delegate.log = function() {

                if ($logProvider.debugEnabled())
                    origLog.apply(null, arguments)
            };

            return $delegate;
        });
    }
    //=========================================================================================================	

    $translateProvider.useStaticFilesLoader({
        prefix: 'assets/resources/lang/',
        suffix: '.json'
    });

    $translateProvider.preferredLanguage(fhirConfig.language);
    $translateProvider.useSanitizeValueStrategy(null);
});