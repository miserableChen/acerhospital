angular.module("abeingWellness").factory('AuthService', function ($q, $cookies,$interval,$state, $rootScope, dataService,currentUserService, logService, Session) {
    var authService = {};

    authService.login = function (credentials) {

        credentials.type = "LOGIN";
        credentials.password = hex_md5(credentials.password);
        return dataService.doRequestFuncFree('models/login.php', credentials, "POST")
            .then(function (res) {
                $cookies.put("authToken", res.data.id);
                Session.create(res.data.id, res.data.user.id,
                    res.data.user.role);
                logService.log(res.data.user.username + " Login");
                return res.data.user;
            });
    };

    authService.login_by_token = function (authToken) {
        var dataObj = {
            type: "LOGIN_BY_TOKEN"
        };
        return dataService.doRequestFuncFree('models/login.php', dataObj, "POST", { "AuthToken": authToken })
            .then(function (res) {
                $cookies.put("authToken", res.data.id);
                Session.create(res.data.id, res.data.user.id,
                    res.data.user.role);
                logService.log(res.data.user.username + " Login");
                return res.data.user;
            });
    };

    authService.logout = function () {

        logService.log(currentUserService.currentUser.username + " Logout.");
        for (var i in $rootScope.timerArr) {
            $interval.cancel($rootScope.timerArr[i]);
        }

        $cookies.remove('userId');
        $cookies.remove('userRole');
        if ($cookies.get("remember") != "true") {
            $cookies.remove("userName");
        }

        $cookies.remove("name");
        $cookies.remove("branchId");
        $cookies.remove("branchName");
        $cookies.remove("title");
        $cookies.remove("authToken");
        Session.destroy();
        var dataObj = {
            type: "LOGOUT"
        }
        dataService.doRequestFuncFree('models/login.php', dataObj, "POST").then(
            function(){
                $state.go("login");
            }
        )
        

    }

    authService.isAuthenticated = function () {
        return !!Session.userId;
    };

    authService.isAuthorized = function (authorizedRoles, elemet) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (authorizedRoles == "*" || (authService.isAuthenticated() &&
            authorizedRoles.indexOf(Session.userRole) !== -1));
    };

    return authService;
})