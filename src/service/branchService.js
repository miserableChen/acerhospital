angular.module('abeingWellness.admin').service("branchService", function ($q,$filter,$log,$rootScope, $http, $cookies, dataService) {
    var branchService = this;
    var sUrl = "models/branch.php";
    branchService.create = function (dataObj) {
        dataObj.type = "C";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    branchService.list = function () {
        var deferred = $q.defer();
        var dataObj = {};
        dataObj.type = "RAL";
        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data);
        },function(failed){
             $rootScope.alertBox("danger",$filter("translate")( failed.data.errorCode));

        });
        return deferred.promise;
    };

    branchService.update = function (dataObj) {
        dataObj.type = "U";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    branchService.del = function (dataObj) {
        dataObj.type = "D";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

});