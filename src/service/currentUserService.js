angular.module("abeingWellness").service('currentUserService', function () {
  this.currentUser={};
  this.create = function (currentUser) {
     this.currentUser=currentUser;
  };
  this.destroy = function () {
    this.currentUser=null;
  };
})