angular.module("abeingWellness").factory('dataService',
    function($log, $http, $rootScope, $cookies, $state, Session, fhirConfig) {
        $log.log("dataService init")
        var AuthToken = null;



        return {
            doRequestFunc: function(sPath, dataObj, methodString, headerObj) {

                var url_string = fhirConfig.apiUrl + sPath;

                var sMethod = methodString || "POST";
                var headerObj = headerObj || {
                    "Content-Type": "application/json"
                };
                var dataObj = dataObj || {};


                if (sMethod == "POST" || sMethod == "PUT") {
                    return $http({
                        method: sMethod,
                        url: url_string,
                        headers: headerObj,
                        data: dataObj
                    });
                } else {
                    return $http({
                        method: sMethod,
                        url: url_string,
                        headers: headerObj,
                        params: dataObj
                    });
                }
            },
            doRequestFuncFree: function(sUrl, dataObj, methodString, headerObj) {

                var sMethod = methodString || "GET";
                var headerObj = headerObj || {};
                var dataObj = dataObj || {};
                if (typeof(Session.id) !== "undefined") {
                    headerObj.AuthToken = Session.id;
                }
                if (sMethod == "POST" || sMethod == "PUT") {
                    return $http({
                        method: sMethod,
                        url: sUrl,
                        headers: headerObj,
                        data: dataObj
                    });
                } else {
                    return $http({
                        method: sMethod,
                        url: sUrl,
                        headers: headerObj,
                        params: dataObj
                    });
                }
            },
            parseDate: function(timestamp) {
                var tmpDate = new Date(timestamp);
                var monthString = ((tmpDate.getMonth() + 1) >= 10) ? (tmpDate.getMonth() + 1) : "0" + (tmpDate.getMonth() + 1);
                var dateString = ((tmpDate.getDate()) >= 10) ? (tmpDate.getDate()) : "0" + (tmpDate.getDate());
                var hourString = (tmpDate.getHours() >= 10) ? tmpDate.getHours() : "0" + tmpDate.getHours();
                var minString = (tmpDate.getMinutes() >= 10) ? tmpDate.getMinutes() : "0" + tmpDate.getMinutes();
                var secString = (tmpDate.getSeconds() >= 10) ? tmpDate.getSeconds() : "0" + tmpDate.getSeconds();
                return tmpDate.getFullYear() + "-" + monthString + "-" + dateString + " " + hourString + ":" + minString + ":" + secString;

            },
            br2n: function(str, reverge) {
                if (reverge == undefined)
                    reverge = true;
                var regex = /<br\s*[\/]?>/gi;
                if (!reverge) {
                    regex = /\n/g;
                    return str.replace(regex, "<br>");
                }

                return str.replace(regex, "\n");
            },
            transFileSize: function(size) {
                var KB = 1024;
                var MB = KB * 1024;
                var GB = MB * 1024;
                var TB = GB * 1024;

                var result = "";
                switch (true) {
                    case TB <= size:
                        result = (size / TB).toFixed(1) + " TB";
                        break;
                    case GB <= size:
                        result = (size / GB).toFixed(1) + " GB";
                        break;
                    case MB <= size:
                        result = (size / MB).toFixed(1) + " MB";
                        break;
                    case KB <= size:
                        result = (size / KB).toFixed(1) + " KB";
                        break;
                    case 1 == size:
                        result = size + " Byte";
                        break;
                    default:
                        result = size + " Bytes";
                }
                return result;
            },


            printElement: function(id) {

                var divToPrint = document.getElementById(id);
                var newWin = window.open('', 'Print-Agreement');

                newWin.document.open();
                newWin.document.write(
                    '<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>'
                );
                newWin.document.close();
                setTimeout(function() {
                    newWin.close();
                }, 10);


            }
        };

    });