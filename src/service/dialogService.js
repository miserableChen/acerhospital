angular.module("abeingWellness").service("dialogService", function($log, $mdDialog, $filter) {

    this.alertBox = function(options) {
        var title = options.title || "Alert";
        var message = options.message || " ";
        var okBtnTxt = options.okText || "Ok";
        $mdDialog.show({
            controller: function($scope, $mdDialog) {
                $scope.close = function() {
                    $mdDialog.hide();
                }
            },
            template: '<md-dialog class="md-transition-in" aria-label="Custom Alert" role="dialog" tabindex="-1" aria-describedby="dialogContent_0">' +
                '<md-toolbar class="bg-white">' +
                '<div class="md-toolbar-tools">' +
                '<h2> ' + title + '</h2>' +
                '</div>' +
                '</md-toolbar>' +
                '  <md-dialog-content class="commonDialog confirmDialog">' +
                '<form>' +
                '<div class="row">' +
                '<div class="col-md-12">' +
                message +
                '</div>' +
                '</div>' +
                '</form>' +
                '  </md-dialog-content>' +
                '  <md-dialog-actions layout="row" class="layout-row" class="col-xs-12">' +
                '<div class="col-xs-12 alert_ok">' +
                '    <button  class="btn-white md-button md-ink-ripple confirmCancelBtn" ng-click="close()">' +
                okBtnTxt +
                '    </button>' +
                '</div>' +
                '  </md-dialog-actions>' +
                '</md-dialog>',
        }).finally(function() {
            if (options.funcOnClose) {
                options.funcOnClose();
            }
        });
    }

    this.confirmBox = function(options) {
        var title = options.title || $filter("translate")("S_00691");
        var message = options.message || " ";
        $log.log(options)
        var cancelBtnTxt = options.cancelLabel || $filter("translate")("S_00111");
        var confirmBtnTxt = options.confirmLabel || $filter("translate")("S_00691");
        $mdDialog.show({
            controller: function($scope, $mdDialog) {
                $scope.close = function() {
                    $mdDialog.cancel();
                }
                $scope.confirm = function() {
                    $mdDialog.hide();
                }
            },
            template: '<md-dialog class="md-transition-in" aria-label="Custom Confirm" role="dialog" tabindex="-1" aria-describedby="dialogContent_0">' +
                '<md-toolbar  class="bg-white">' +
                '<div class="md-toolbar-tools">' +
                '<h2> ' + title + '</h2>' +
                '</div>' +
                '</md-toolbar>' +
                '  <md-dialog-content class="commonDialog confirmDialog" id="dialogContent_0">' +
                '<form>' +
                '<div class="row">' +
                '<div class="col-md-12">' +
                message +
                '</div>' +
                '</div>' +
                '</form>' +
                '  </md-dialog-content>' +
                '  <md-dialog-actions layout="row" class="layout-row" class="col-xs-12">' +
                '<div class="col-xs-1"></div>' +
                '<div class="col-xs-5">' +
                '<button class="btn-green md-button md-ink-ripple" ng-click="confirm()">' +
                confirmBtnTxt +
                '    </button>' +
                '</div>' +
                '<div class="col-xs-5">' +
                '<button class="btn-white md-button md-ink-ripple confirmCancelBtn" 	ng-click="close()">' +
                cancelBtnTxt +
                '    </button>' +

                '</div>' +
                '<div class="col-xs-1"></div>' +
                '  </md-dialog-actions>' +
                '</md-dialog>',
        }).then(function() {
            if (options.funcOnConfirm) {
                options.funcOnConfirm();
            }
        }, function() {
            if (options.funcOnCancel) {
                options.funcOnCancel();
            }
        });
    }

});