angular.module("abeingWellness").factory('eChartService',
    function($log, $filter, DATA_TYPE) {
        $log.log("eChartService init")
        var translate = $filter('translate');
        $log.log(translate("S_SAVE_AS_IMG"));

        var lineConfig = {
            theme: 'default',
            dataLoaded: false
        }
        var chart_color = '#83d4f4';
        var text_color = '#979797';
        var dataConfigArr = {
            "bp": {
                title: 'S_03048',
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03049', keyword: 'systolic' }, { name: 'S_03050', keyword: 'diastolic' }, { name: 'Unit', keyword: 'bp_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03049', keyword: 'systolic', color: chart_color }, { name: 'S_03050', keyword: 'diastolic', color: "#fdbf62" }],
                unitName: "bp_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "hr": {
                title: "S_03046",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03046', keyword: 'hr' }, { name: 'Unit', keyword: 'hr_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03046', keyword: 'hr', color: chart_color }],
                unitName: "hr_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "rhr": {
                title: "S_03047",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03047', keyword: 'rhr' }, { name: 'Unit', keyword: 'rhr_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03047', keyword: 'rhr', color: chart_color }],
                unitName: "rhr_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "sleep": {
                title: "S_03174",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03174', keyword: 'sleep' }, { name: 'Unit', keyword: 'sleep_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03174', keyword: 'sleep', color: chart_color }],
                unitName: "sleep_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "bg": {
                title: "S_03278",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03278', keyword: 'bg' }, { name: 'Unit', keyword: 'bg_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03278', keyword: 'bg', color: chart_color }],
                unitName: "bg_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "bmi": {
                title: "S_03267",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03267', keyword: 'bmi' }, { name: 'Unit', keyword: 'bmi_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03267', keyword: 'bmi', color: chart_color }],
                unitName: "bmi_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "bmr": {
                title: "S_03176",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03176', keyword: 'bmr' }, { name: 'Unit', keyword: 'bmr_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03176', keyword: 'bmr', color: chart_color }],
                unitName: "bmr_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "weight": {
                title: "S_03056",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03056', keyword: 'wgt' }, { name: 'Unit', keyword: 'wgt_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03056', keyword: 'wgt', color: chart_color }],
                unitName: "wgt_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "bfp": {
                title: "S_03175",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03175', keyword: 'bfp' }, { name: 'Unit', keyword: 'bfp_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03175', keyword: 'bfp', color: chart_color }],
                unitName: "bfp_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "step": {
                title: "S_03257",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03257', keyword: 'steps' }, { name: 'Unit', keyword: 'steps_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03257', keyword: 'steps', color: chart_color }],
                unitName: "steps_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "co2": {
                title: "S_03137",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03137', keyword: 'co2' }, { name: 'Unit', keyword: 'co2_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03137', keyword: 'co2', color: chart_color }],
                unitName: "co2_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "pm2_5": {
                title: "S_03141",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03141', keyword: 'pm2_5' }, { name: 'Unit', keyword: 'pm2_5_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03141', keyword: 'pm2_5', color: chart_color }],
                unitName: "pm2_5_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            },
            "tvocs": {
                title: "S_03139",
                chartType: "line",
                dataKeywordSet: [{ name: 'S_03139', keyword: 'tvocs' }, { name: 'Unit', keyword: 'tvocs_unit' }, { name: 'Time', keyword: 'timestamp' }],
                seriesKeywordSet: [{ name: 'S_03139', keyword: 'tvocs', color: chart_color }],
                unitName: "tvocs_unit",
                timeName: "timestamp",
                dateFormat: "MMM DD YYYY",
                timeFormat: "HH:mm"
            }

        }
        var chartOption = {
            lineOption: {
                title: {
                    text: '趨勢圖',
                    left: 'left'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    show: true,
                    bottom: 0,
                    right: '5%',
                    itemGap: 50,
                    itemWidth: 28,
                    itemHeight: 4,

                },
                xAxis: {
                    type: 'category',
                    boundaryGap: true,
                    axisTick: {
                        alignWithLabel: true,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: text_color
                        }
                    },
                },
                yAxis: {
                    boundaryGay: true,
                    axisLine: {
                        show: false
                    },
                    nameLocation: 'end',
                    nameGap: 61,
                    nameTextStyle: {
                        color: text_color,
                        fontSize: 16,
                    },
                    splitLine: {
                        show: true
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            fontSize: 16,
                            color: text_color
                        }
                    }
                },
                grid: {
                    left: '5%',
                    right: '5%',
                    bottom: '10%',
                    top: '150px',
                    containLabel: true
                },
                calculable: true,
                toolbox: {
                    left: 'right',
                    itemSize: 20,
                    feature: {
                        restore: {
                            title: translate("S_03193")
                        },
                        saveAsImage: {
                            title: translate("S_03195")
                        }
                    }
                },
                dataZoom: [{
                    type: 'inside'
                }],
                series: []
            },
            pieOption: {
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    right: 5,
                    bottom: 5,
                    data:[]
                },
                color:["#d84e4e","#82b841","#efd400"],
                series : [
                    {
                        type: 'pie',
                        radius : '45%',
                        center: ['23%', '50%'],
                        silent:true,
                        data:[],
                        label: {
                            normal: {
                                show: false
                            },
                            emphasis: {
                                show: false
                            }
                        }
                    }
                ]
            },
            boxplotOption: {
                title: {
                    text: '各時段平均圖',
                    left: 'left'
                },
                tooltip: {
                    trigger: 'item',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '5%',
                    right: '5%',
                    bottom: '10%',
                    top: '150px',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    nameGap: 20,
                    splitArea: {
                        show: true
                    }
                },
                yAxis: [],
                series: []
            }
        };
        return {
            getDefaultOption: function(type) {
                var option = {},
                    typeOption = chartOption[type+"Option"];
                angular.copy(typeOption, option);
                return option;

            },
            getDefaultConfig: function() {
                var config = {};
                angular.copy(lineConfig, config);
                return config;

            },
            getChartTitle: function(type_str) {
                return translate(dataConfigArr[type_str].title);

            },
            /*type reference eChart document
                {type: 'line', ...},
                {type: 'bar', ...},
                {type: 'pie', ...},
                {type: 'scatter', ...},
                {type: 'effectScatter', ...},
                {type: 'radar', ...},
                {type: 'treemap', ...},
                {type: 'boxplot', ...},
                {type: 'candlestick', ...},
                {type: 'heatmap', ...},
                {type: 'map', ...},
                {type: 'parallel', ...},
                {type: 'lines', ...},
                {type: 'graph', ...},
                {type: 'sankey', ...},
                {type: 'funnel', ...},
                {type: 'gauge', ...},
            */
            getSeries: function(type_str, data) {
                var translate = $filter('translate');

                var series_arr = [];

                $log.log(type_str)
                if (type_str == "bp") {
                    var sys_series = {
                        name: translate(dataConfigArr[type_str].seriesKeywordSet[0].name),
                        type: dataConfigArr[type_str].chartType,
                        data: [],
                        showSymbol: false,
                        itemStyle: {
                            normal: {
                                color: dataConfigArr[type_str].seriesKeywordSet[0].color
                            }
                        }
                    };

                    var dia_series = {
                        name: translate(dataConfigArr[type_str].seriesKeywordSet[1].name),
                        type: dataConfigArr[type_str].chartType,
                        data: [],
                        showSymbol: false,
                        itemStyle: {
                            normal: {
                                color: dataConfigArr[type_str].seriesKeywordSet[1].color
                            }
                        }
                    };

                    for (var idx in data) {
                        sys_series.data.push(data[idx].dataValue.split(";")[0])
                        dia_series.data.push(data[idx].dataValue.split(";")[1])
                    }
                    series_arr.push(sys_series);
                    series_arr.push(dia_series);
                } else {
                    var series = {
                        name: translate(dataConfigArr[type_str].seriesKeywordSet[0].name),
                        type: dataConfigArr[type_str].chartType,
                        data: [],
                        showSymbol: false,
                        itemStyle: {
                            normal: {
                                color: dataConfigArr[type_str].seriesKeywordSet[0].color
                            }
                        }
                    };

                    for (var idx in data) {
                        series.data.push(data[idx].dataValue)
                    }
                    series_arr.push(series);
                }

                $log.log(series_arr);

                return series_arr;

            },
            /*format reference Moment formatter
                YYYY	2014	4 or 2 digit year
                YY	14	2 digit year
                Y	-25	Year with any number of digits and sign
                Q	1..4	Quarter of year. Sets month to first month in quarter.
                M MM	1..12	Month number
                MMM MMMM	Jan..December	Month name in locale set by moment.locale()
                D DD	1..31	Day of month
                Do	1st..31st	Day of month with ordinal
                DDD DDDD	1..365	Day of year
                X	1410715640.579	Unix timestamp
                x	1410715640579	Unix ms timestamp
                moment(parseInt(item["timestamp"]) * 1000).format(dataConfigArr[type_str].timeFormat),
            */
            parseDatasetToTime: function(type_str, data) {
                return data.map(function(item) {
                    return {
                        value: moment(parseInt(item["timestamp"]) * 1000).format(dataConfigArr[type_str].dateFormat) + "\r\n" + moment(parseInt(item["timestamp"]) * 1000).format(dataConfigArr[type_str].timeFormat),
                        textStyle: {
                            color: text_color,
                            align: 'left',
                            baseline: "top",
                            fontSize: 10
                        }
                    }
                });
            }

        }

    });
