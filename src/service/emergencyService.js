angular.module('abeingWellness.patients').service("emergencyService", function ($log, $q, $rootScope, $http, $filter, $cookies, dataService) {
    var self = this;
    var sUrl = "models/emergency.php";

    self.getEmergencyPatientList = function (branchId) {
        var deferred = $q.defer();
        var dataObj = {
            type: "R_EMG_DATA",
            branchId: branchId
        };

        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data.result);
        }, function (failed) {
            if (failed.status == -1) {
                $rootScope.alertBox("danger", $filter("translate")("S_00202"));
            } else {
                $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
            }

        });

        return deferred.promise;

    }


    self.readAlertData = function (patientId,createTime) {


        var dataObj = {
            "type": "SET_EMG_READED"
        };
        if (typeof (patientId) !== "undefined"){
            dataObj.patientId = patientId;
            dataObj.createTime = createTime;
        }    
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

    }
});