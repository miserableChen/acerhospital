angular.module('abeingWellness').service("groupService",
    function($log, $http, $rootScope, $window, $cookies, $filter, $state, dataService, currentUserService, AuthService, $q) {
        var self = this;
        var sUrl = "models/group.php";
        self.scope = '';
        self.groupName = {};
        self.patientGroup = {};
        self.groupIdList = {};
        self.updateList = function() {
            var defer = $q.defer();

            var dataObj = {
                "type": 'RAL',
                "branchId": currentUserService.currentUser.branchId
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(
                function(response) {
                    var groupList = response.data.result;
                    defer.resolve(groupList);
                },
                function(responseError) {
                    $log.log(responseError)
                    if (responseError.status == 403) {
                        AuthService.logout();
                    }
                    defer.reject(responseError);
                });

            return defer.promise;
        }

        self.addGroup = function(patientObj, groupName, branchId) {
            var defer = $q.defer();
            var patientObj = Object.keys(patientObj);
            var dataObj = {
                "type": 'C',
                "name": groupName,
                "branchId": branchId
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(response) {
                    var groupId = response.resultMsg;
                    dataObj = {
                        "type": 'JOIN',
                        "groupId": groupId,
                        "patientObj": patientObj
                    }
                    dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                        .success(function(response) {
                            defer.resolve(response.resultMsg);
                        })
                        .error(function(responseError) {
                            defer.reject(responseError);
                        })
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                });
            return defer.promise;
        }

        self.joinGroup = function(patientObj, groupId) {
            var defer = $q.defer();
            var patientObj = Object.keys(patientObj);
            var dataObj = {
                "type": 'JOIN',
                "groupId": groupId,
                "patientObj": patientObj
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(response) {
                    defer.resolve(response.resultMsg);
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                })
            return defer.promise;
        }
        self.renameGroup = function(groupName, groupId) {
            var defer = $q.defer();
            var dataObj = {
                "type": 'U',
                "id": groupId,
                "name": groupName
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(response) {
                    defer.resolve(response.resultMsg);
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                })
            return defer.promise;
        }
        self.delGroup = function(groupId) {
            var defer = $q.defer();
            var dataObj = {
                "type": 'D',
                "id": groupId
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(groupId) {
                    defer.resolve('Delete Group Success');
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                });
            return defer.promise;
        }

        self.leaveGroup = function(patientObj, groupId) {
            var defer = $q.defer();
            var patientObj = Object.keys(patientObj);
            var dataObj = {
                "type": 'LEAVE',
                "id": groupId,
                "patientObj": patientObj
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(groupId) {
                    defer.resolve('Leave Group Success');
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                });
            return defer.promise;
        }

        self.getPatientGroup = function(patientId) {
            var defer = $q.defer();
            self.updateList().then(
                function(response) {
                    angular.forEach(response, function(value) {
                        self.groupIdList[value.name] = value.id;
                        self.groupName[value.id] = value.name;
                        self.patientGroup[value.name] = false;
                    });
                    var dataObj = {
                        "type": 'R_PATIENT',
                        "patientId": patientId
                    }
                    dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                        .success(function(response) {
                            angular.forEach(response.result, function(value) {
                                self.patientGroup[value.name] = true;
                            })
                            var responseData = [];
                            responseData['patientGroup'] = self.patientGroup;
                            responseData['groupIdList'] = self.groupIdList;
                            responseData['groupName'] = self.groupName;

                            defer.resolve(responseData);
                        })
                        .error(function(responseError) {
                            defer.reject(responseError);
                        });
                },
                function(responseError) {
                    defer.reject(responseError);
                }
            )
            return defer.promise;
        }
        self.setPatientGroup = function(patientId, groupObjId) {
            var defer = $q.defer();
            var dataObj = {
                "type": 'U_PATIENT',
                "patientId": patientId,
                "groupObjId": groupObjId
            }
            dataService.doRequestFuncFree(sUrl, dataObj, "POST")
                .success(function(response) {
                    defer.resolve('Update patient Groups Success!');
                })
                .error(function(responseError) {
                    defer.reject(responseError);
                });
            return defer.promise;
        }
    });