
angular.module("abeingWellness").service("homeService",
	function ($log, $http, $window, $cookies, $state, fhirConfig, dataService) {
		var self = this;
		var syncInterval = null;
		self.syncData = function () {
			var user = $cookies.get("username");
			var pwd = $cookies.get("password");			
			if (user == undefined && pwd == undefined) {
				if (syncInterval != null)
					syncInterval = window.clearInterval(syncInterval);
				syncInterval = null;
				return;
			}
			var sUrl = fhirConfig.apiUrl + "/byoc/v1/session";
			headerObj = {
				"Content-Type": "application/json"
			}

			dataObj = {
				"username": user,
				"password": pwd
			};

			if (syncInterval == null) {
				syncInterval = setInterval(function () {
					dataService.doRequestFunc(sUrl, dataObj, "POST", headerObj).then(function () {
						$log.log("interval call session success");

					}, function () {
						$log.log("interval call session fail");
					});

				}, 600000);
			}
		};

		self.stopSyncData = function () {
			syncInterval = window.clearInterval(syncInterval);

		}

	});
