angular.module("abeingWellness").service('logService', function ($q, $log,$rootScope,$filter, $cookies, dataService, Session) {

    var self = this;
    var sUrl = "models/log.php"
    self.log = function (message) {
        var dataObj = {
            "type": "C",
            "member_id": Session.userId,
            "message": message
        };
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    self.get_log = function (message) {
        var deferred = $q.defer();
        var dataObj = {};
        dataObj.type = "RAL";
        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data);
        }, function (failed) {
            $rootScope.alertBox("danger",$filter("translate")( failed.data.errorCode));
        });

        return deferred.promise;
    }

})