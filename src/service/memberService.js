angular.module('abeingWellness.admin').service("memberService", function ($log, $q,$rootScope, $http,$filter, $cookies, dataService) {
    var memService = this;
    var sUrl = "models/member.php";
    memService.create = function (dataObj) {
        dataObj.type = "C";
        dataObj.password = hex_md5(dataObj.password);
        $log.log(dataObj);
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    memService.list = function () {
        var deferred = $q.defer();
        var dataObj = {};
        dataObj.type = "RAL";
        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data);
        }, function (failed) {
            $rootScope.alertBox("danger",$filter("translate")( failed.data.errorCode));
        });

        return deferred.promise;

    };

    memService.getMemberData = function (id) {
        var deferred = $q.defer();
        var dataObj = {
            "type": "R",
            "id": id
        };

        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response);
        }, function (failed) {
            $rootScope.alertBox("danger",$filter("translate")( failed.data.errorCode));
        });

        return deferred.promise;
    };

    memService.update = function (dataObj) {
        dataObj.type = "U";
        if (dataObj.oldpassword) {
            dataObj.oldpassword = hex_md5(dataObj.oldpassword);
        }
        if (dataObj.password) {
            dataObj.password = hex_md5(dataObj.password);
        }
        $log.log(dataObj);
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    memService.del = function (dataObj) {
        dataObj.type = "D";

        $log.log(dataObj);
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

});