angular.module("abeingWellness").factory('NoteEditService', function($log, $rootScope, dataService, NoteService) {
    var service = {};

    var classItems = [ //unuse 
        { label: "來電處理", value: "來電處理" },
        { label: "去電訪edit", value: "去電訪" }
    ]

    var selectItems = []
    var selectTemplate = []
        //category
    service.subCategoryItems = {};
    service.categoryItems = [];

    service.init = function(noteData) {
        $log.log("edit service init");
        service.categoryItems = NoteService.categoryItems;
        service.subCategoryItems = NoteService.getCategoryItems();
        noteData.note_content = eval(noteData.note_content);

        for (var i in noteData.note_content) {
            service.subCategoryItems[noteData.note_content[i].label_id].template = noteData.note_content[i].description
            service.subCategoryItems[noteData.note_content[i].label_id].comment = noteData.note_content[i].comment
            service.subCategoryItems[noteData.note_content[i].label_id].checked = true;
            service.addSelectItems(noteData.note_content[i].label_id)
        }

    }

    service.getCategoryItems = function() {
        return angular.copy(service.subCategoryItems);
    }

    service.addSelectItems = function(id) {
        $log.log("addSelectItems" + id);
        var item = {
            "category": service.subCategoryItems[id].label,
            "value": id
        }
        selectItems.push(item)

        var tmp = service.subCategoryItems[id];
        tmp.id = id;
        $log.log("tmp");
        $log.log(tmp);
        selectTemplate.push(tmp)
        service.subCategoryItems[id].checked = true;

    }

    service.removeSelectItem = function(id) {
        $log.log("removeSelectItem" + id);
        for (var i = 0; i < selectItems.length; i++) {
            if (selectItems[i].value === id) {
                selectItems.splice(i, 1);
                break;
            }
        }
        for (var i = 0; i < selectTemplate.length; i++) {
            if (selectTemplate[i].id === id) {
                selectTemplate.splice(i, 1);
                break;
            }
        }

        service.subCategoryItems[id].checked = false;


    }


    service.cleanSelect = function() {
        for (var i in service.subCategoryItems) { service.subCategoryItems[i].checked = false; }
        selectItems.length = 0;
        selectTemplate.length = 0;
    }



    service.selectItems = selectItems;
    service.selectTemplate = selectTemplate;

    return service;
})