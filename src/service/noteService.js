angular.module("abeingWellness").factory('NoteService', function($log, $rootScope, dataService) {
    var service = {};

    var classItems = [ //unuse 
            { label: "來電處理", value: "來電處理" },
            { label: "去電訪", value: "去電訪" }
        ]
        //category
    service.subCategoryItems = {};
    service.categoryItems = [];

    var categoryItems = [{
                "subCategory": [
                    2
                ],
                "Category": "例行性關懷"
            },
            {
                "subCategory": [
                    3
                ],
                "Category": "門診協助"
            },
            {
                "subCategory": [
                    1,
                    4
                ],
                "Category": "量測值異常"
            },
            {
                "subCategory": [],
                "Category": "設備或系統障礙"
            },
            {
                "subCategory": [],
                "Category": "住院或急診訪視"
            },
            {
                "subCategory": [],
                "Category": "收案或結案"
            },
            {
                "subCategory": [],
                "Category": "疾病照護諮詢"
            },
            {
                "subCategory": [],
                "Category": "生活資源事宜"
            },
            {
                "subCategory": [],
                "Category": "其他"
            }
        ]
        //     //subcategory
        //     //  subCategoryItems={}
        //     //  subCategoryItems[categoryItems[i].subCategory[j].id]=categoryItems[i].subCategory[j]
    var subCategoryItems = {
        "1": {
            "template": "(小於70)&nbsp;&nbsp;餐前/後血糖是 &nbsp;  mg/dl，去電關心身體症狀_____，準備要吃_____。請會員先補充含糖食物(3顆方糖/ 一湯匙砂糖/一湯匙蜂蜜/120cc果汁)，15分鐘後再測量一次血糖，先不要服用降血糖藥物/注射胰島素，我端之後再去電關心追蹤。",
            "checked": false,
            "comment": "",
            "label": "低血糖"
        },
        "2": {
            "template": "去電關心會員，(因氣溫遽降，故提醒須注意保暖、多補充水分___cc/天)；降血壓/血糖藥物規則服用；三餐飲食定時定量，避免油膩；每天規律運動30分鐘。",
            "checked": false,
            "comment": "",
            "label": "例行性電訪或家訪"
        },
        "3": {
            "template": "去電提醒會員明天早上/下午/晚上___醫師門診___診___號。提醒要先禁食抽血/要看上次檢驗報告/要請醫師開___檢查/要至___拿報告。告知插卡報到後再CALL TSC。",
            "checked": false,
            "comment": "",
            "label": "看門診事項"
        },
        "4": {
            "template": "(小於70)____餐前/後血糖是___mg/dl，去電關心身體症狀_____，準備要吃_____。請會員先補充含糖食物(3顆方糖/ 一湯匙砂糖/一湯匙蜂蜜/120cc果汁)，15分鐘後再測量一次血糖，先不要服用降血糖藥物/注射胰島素，我端之後再去電關心追蹤。",
            "checked": false,
            "comment": "",
            "label": "高血糖"
        }
    }

    service.init = function() {
        $log.log("note service init");
        service.categoryItems = categoryItems
        service.subCategoryItems = subCategoryItems;


        // dataService.doRequestFunc("/noteSystem/v1/template", {}, "GET").then(function(response) {
        //     service.categoryItems = response.data;
        // })

        // dataService.doRequestFunc("/noteSystem/v1/template", { "content": "all" }, "GET").then(function(response) {
        //     service.subCategoryItems = response.data;
        // })
    }

    service.getCategoryItems = function() {
        return angular.copy(service.subCategoryItems);
    }

    return service;
})