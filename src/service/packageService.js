angular.module('abeingWellness.admin').service("packageService", function ($q,$filter,$log,$rootScope, $http, $cookies, dataService,AuthService) {
    var packageService = this;
    var sUrl = "models/package.php";
    packageService.create = function (dataObj) {
        dataObj.type = "C";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    packageService.list = function () {
        var deferred = $q.defer();
        var dataObj = {};
        dataObj.type = "RAL";
        dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data);
        },
        function(failed){            

            $rootScope.alertBox("danger",$filter("translate")( failed.data.errorCode));
            AuthService.logout();
        });
        return deferred.promise;
    };

    packageService.update = function (dataObj) {
        dataObj.type = "U";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

    packageService.del = function (dataObj) {
        dataObj.type = "D";
        return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
    };

});