angular.module('abeingWellness.patients').service("patientService",
    function($log, $http, $window, $rootScope, $cookies, $state, $filter, dataService, $q, DATA_UNIT, currentUserService) {
        var self = this;
        var sUrl = "models/patient.php";
        self.create = function (dataObj) {
            var api = "/byoc/v1/user";
            $log.log(dataObj)
            return dataService.doRequestFunc(api, dataObj);

        };

        self.editBg = function (id, dataObj) {
            dataObj.type = "UPDATE_BG_RULE";
            dataObj.id = +id;
            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
        };

        self.editBp = function (id, dataObj) {
            dataObj.type = "UPDATE_BP_RULE";
            dataObj.id = +id;
            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");
        };

        self.setNote = function(id, data) {
            var deferred = $q.defer();

            dataService.doRequestFunc("/noteSystem/v1?pid=" + id, data, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                // $log.log(failed)
                $rootScope.alertBox("danger", "set note fail");
            });

            return deferred.promise;
        }

        self.editNote = function(id, data) {
            var deferred = $q.defer();

            dataService.doRequestFunc("/noteSystem/v1/note/" + id, data, "PUT").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                $log.log(failed)
                    // $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
            });

            return deferred.promise;
        }


        self.getNote = function(id) {
            var deferred = $q.defer();

            // dataService.doRequestFunc("/noteSystem/v1", { "pid": id }, "GET").then(function(response) {
            dataService.doRequestFuncFree("assets/resources/fake.json", { "pid": id }, "GET").then(function(response) {
                // $log.log("response")
                // $log.log(response.data.result)
                deferred.resolve(response.data.result);
            }, function(failed) {
                // $log.log(failed)
                $rootScope.alertBox("danger", "get note api fail");
            });

            return deferred.promise;
        }

        self.update = function(dataObj) {
            var api = "/byoc/v1/user";

            return dataService.doRequestFunc(api, dataObj, "PUT");

        }
        self.pushNotification = function(dataObj) {
            var api = "/byoc/v1/notification";

            return dataService.doRequestFunc(api, dataObj);

        }

        self.unlinkDevice = function(dataObj) {
            var api = "/byoc/v1/device";

            return dataService.doRequestFunc(api, dataObj, "PUT");

        }

        self.deleteData = function(patientId) {


            var dataObj = {
                "type": "D"
            };
            dataObj.id = patientId;
            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

        }

        self.setDeivicePurchaseDate = function(dataObj) {

            dataObj.type = "U_DEV_INFO";

            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

        }

        self.setThreshold = function(dataObj) {

            dataObj.type = "U_THRESHOLD";

            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

        }

        self.setEmergencyContact = function(patientId, emgName, emgPhone) {


            var dataObj = {
                "type": "C_EMG_DATA",
                "id": patientId,
                "emgName": emgName,
                "emgPhone": emgPhone

            };

            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

        }

        self.updateEmergencyContact = function(patientId, emgName, emgPhone) {

            var dataObj = {
                "type": "U_EMG_DATA",
                "id": patientId,
                "emgName": emgName,
                "emgPhone": emgPhone

            };

            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");

        }

        self.getPatientListWithGroup = function(branchId, groupId) {
            var deferred = $q.defer();
            var dataObj = {
                type: "R_GROUP",
                id: groupId,
                branchId: branchId
            };

            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                if (failed.status == -1) {
                    $rootScope.alertBox("danger", $filter("translate")("S_00202"));
                } else {
                    $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
                }

            });

            return deferred.promise;

        }

        self.getPatientList = function(branchId) {
            var deferred = $q.defer();
            var dataObj = {
                type: "RAL",
                branchId: branchId
            };

            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                if (failed.status == -1) {
                    $rootScope.alertBox("danger", $filter("translate")("S_00202"));
                } else {
                    $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
                }

            });

            return deferred.promise;
        }



        self.getPatientData = function(type, patientId, start, end) {
            var deferred = $q.defer();

            var dataObj = {
                "type": type
            };


            if (typeof(patientId) !== "undefined") {
                dataObj.id = patientId;
            }

            dataObj.start = start;
            dataObj.end = end;

            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                if (failed.status == -1) {
                    $rootScope.alertBox("danger", $filter("translate")("S_00202"));
                } else {
                    $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
                }

            });

            return deferred.promise;

        }

        self.getPatientChartData = function(type, patientId, start, end) {
            var deferred = $q.defer();

            var dataObj = {
                "type": type==="bg" ? "BG_CHARTDATA":"R_CHARTDATA",
                "data_type": type,
                "start_date": moment(start).format("YYYY/MM/DD 00:00"),
                "end_date": moment(end).format("YYYY/MM/DD 23:59")
            };


            if (typeof(patientId) !== "undefined")
                dataObj.id = patientId;

            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                if (failed.status == -1) {
                    $rootScope.alertBox("danger", $filter("translate")("S_00202"));
                } else {
                    $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
                }

            });

            return deferred.promise;

        }

        self.getPatientBPData = function(patientId, start, end) {
            var deferred = $q.defer();

            var dataObj = {
                "type": "BP_DATA",
                "start_date": moment(start).format("YYYY/MM/DD 00:00"),
                "end_date": moment(end).format("YYYY/MM/DD 23:59")
            };


            if (typeof(patientId) !== "undefined")
                dataObj.id = patientId;

            dataService.doRequestFuncFree(sUrl, dataObj, "POST").then(function(response) {
                deferred.resolve(response.data.result);
            }, function(failed) {
                if (failed.status == -1) {
                    $rootScope.alertBox("danger", $filter("translate")("S_00202"));
                } else {
                    $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
                }

            });

            return deferred.promise;

        };

        self.setPatientInfo = function(profileInfo) {

            var patientInfo = {};


            for (var key in profileInfo) {

                switch (key) {
                    case "photo":
                        patientInfo[key] = (profileInfo[key]) ? profileInfo[key] : "assets/resources/images/profile_pic.png";
                        break;
                    case "birthday":
                        patientInfo["age"] = (profileInfo[key]) ? (new Date().getFullYear() - parseInt(profileInfo[key].split("/")[0])) : "--";
                        patientInfo[key] = (profileInfo[key]) ? moment(profileInfo[key]).format("MM/DD/YYYY") : "--";
                        break;
                    case "telecom":
                        patientInfo["phone"] = (profileInfo[key]) ? profileInfo[key] : "--";
                        break;
                    case "created":
                        patientInfo["since"] = (profileInfo[key]) ? dataService.parseDate(parseInt(profileInfo[key]) * 1000) : "--";
                        break;
                    case "gender":
                        if (typeof(profileInfo[key]) === "undefined")
                            patientInfo["gender_display"] = "--";
                        else if (profileInfo[key].toLowerCase() == "male")
                            patientInfo["gender_display"] = $filter("translate")("S_03202");
                        else
                            patientInfo["gender_display"] = $filter("translate")("S_03203");

                        patientInfo[key] = (profileInfo[key]) ? profileInfo[key].toLowerCase() : "--";
                        break;
                    case "package":
                        $log.log(profileInfo[key]);
                        if (profileInfo[key] == null) {
                            patientInfo.active_status = $filter("translate")("S_03192");
                        } else {
                            patientInfo.active_status = (moment(profileInfo[key]) <= moment()) ? $filter("translate")("S_03194") : "VIP";
                        }

                        patientInfo[key] = (profileInfo[key]) ? profileInfo[key] : "--";
                        break;
                    case "systolic":
                        patientInfo["bp_unit"] = DATA_UNIT["bp"];
                        //no need break;
                    default:
                        if (typeof(DATA_UNIT[key]) !== "undefined") patientInfo[key + "_unit"] = DATA_UNIT[key];
                        patientInfo[key] = (profileInfo[key]) ? profileInfo[key] : "--";
                }

            }
            patientInfo.emgContact = ((patientInfo.emgName) ? patientInfo.emgName : "--") + "(" + ((patientInfo.emgPhone) ? patientInfo.emgPhone : "--") + ")";

            return patientInfo;
        }

        self.setFlag = function(patientId, flag) {


            var dataObj = {
                "type": "SET_FLAG"
            };

            if (typeof(patientId) !== "undefined")
                dataObj.id = patientId;
            if (typeof(flag) === "boolean") {
                if (flag)
                    dataObj.flag = 1;
                else
                    dataObj.flag = 0;
            } else {
                dataObj.flag = Math.abs(1 - parseInt(flag));
            }


            return dataService.doRequestFuncFree(sUrl, dataObj, "POST");


        }

        self.setTimer = function(timer) {
            return self.timer = timer;
        };

        self.createExportData = function(org_data, response_field) {
            var result = [];
            for (var i in org_data) {

                var record_obj = {};
                for (var key in response_field) {

                    if (response_field[key] == "timestamp") {
                        record_obj[key] = moment(org_data[i][response_field[key]] * 1000).format("YYYY-MM-DD HH:mm:ss");
                    } else {
                        record_obj[key] = (org_data[i][response_field[key]]) ? org_data[i][response_field[key]] : '--';
                    }
                }
                result.push(record_obj);
            }
            return result;

        }
    });