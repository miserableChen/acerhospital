angular.module('abeingWellness.patients').service("purchaseService", function ($q,$filter, $log,$rootScope, $http, $cookies, dataService) {
    var purchaseService = this;
    purchaseService.sUrl = "models/purchase.php";
    purchaseService.create = function (dataObj) {
        dataObj.type = "C";
        $log.log(dataObj);
        return dataService.doRequestFuncFree(purchaseService.sUrl, dataObj, "POST");
    };

    purchaseService.list = function (patientId) {
        var deferred = $q.defer();

        dataObj = { "patientId": patientId };
        dataObj.type = "RAL";
        dataService.doRequestFuncFree(purchaseService.sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data.result);
         },function(failed){
            $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
        });

        return deferred.promise;

    };

    purchaseService.refresh = function () {
        var deferred = $q.defer();
        dataObj = { "type": "CHK_DUE_DATE" };
        dataService.doRequestFuncFree(purchaseService.sUrl, dataObj, "POST").then(function (response) {
            deferred.resolve(response.data.result);
         },function(failed){
           $rootScope.alertBox("danger", $filter("translate")(failed.data.errorCode));
           
        });

        return deferred.promise;

    };

    purchaseService.update = function (dataObj) {
        dataObj.type = "U";
        return dataService.doRequestFuncFree(purchaseService.sUrl, dataObj, "POST");
    };

    purchaseService.del = function (dataObj) {
        dataObj.type = "D";
        return dataService.doRequestFuncFree(purchaseService.sUrl, dataObj, "POST");
    };

});